**Actualizar dependencias**

```
composer install
```

**Levantar servidor**

```
php artisan serve
```

**Requerimientos del backend** 

Asegurarse de tener estas versiones.

```
Laravel 5.8.X-dev
composer 1.8.5
npm 6.4.1
MYSQL version 14.14
      distrib 5.7.29
PHP 7.2.17 
```

**Migraciones del backend** 

```
php artisan migrate --path=/database/migrations/2020_03_16_195213_create_usuarios_table.php
```

```
php artisan migrate --path=/database/migrations/2020_03_16_210052_create_roles_table.php

```

```
php artisan migrate --path=/database/migrations/2020_03_16_210234_create_roles_usuarios_table.php
```
```
php artisan migrate --path=/database/migrations/2020_03_18_205936_create_negocios_table.php
```

**En caso de un error General error: 1215 Cannot add foreign key constraint")**

```
asegurarse que la foreign key sea del mismo tipo en las dos tablas
https://www.percona.com/blog/2017/04/06/dealing-mysql-error-code-1215-cannot-add-foreign-key-constraint/
```

**configuracion de envio de correo con sendgrid**
```
https://sendgrid.com/docs/for-developers/sending-email/laravel/
```
