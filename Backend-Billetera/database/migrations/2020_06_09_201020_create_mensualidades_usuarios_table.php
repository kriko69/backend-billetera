<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensualidadesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensualidades_usuarios', function (Blueprint $table) {
            $table->increments('mensualidad_usuario_id');
            $table->integer('usuario_id')->unsigned();
            $table->integer('mensualidad_id')->unsigned();
            $table->double('multa_pagada');
            $table->foreign( 'usuario_id')->references('usuario_id')->on('usuarios');
            $table->foreign('mensualidad_id')->references('mensualidad_id')->on('mensualidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensualidades_usuarios');
    }
}
