<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensualidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensualidades', function (Blueprint $table) {
            $table->increments('mensualidad_id');
            $table->string('nombre');
            $table->string('fecha_pago');
            $table->double('monto');
            $table->integer('numero_pago');
            $table->boolean('estado')->default(false);
            $table->integer('gestion_id')->unsigned();
            $table->foreign( 'gestion_id')->references('gestion_id')->on('gestiones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensualidades');
    }
}
