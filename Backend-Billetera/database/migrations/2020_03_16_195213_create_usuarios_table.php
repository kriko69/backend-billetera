<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('usuario_id');
            $table->string('nombre');
            $table->string('apellidos');
            $table->integer('carnet');
            $table->string('correo');
            //$table->string('carrera')->default("sin carrera");
            $table->integer('carrera_id')->unsigned();
            $table->bigInteger('telefono');
            $table->date('fecha_nacimiento');
            $table->string('pregunta_secreta')->default("S/N");
            $table->string('codigo_reset_password')->default("S/N");
            $table->string('password');
            $table->string('imagen')->default('defaultuser.png');
            $table->boolean('estado')->default(false);
            $table->boolean('password_default')->default(true);
            $table->string('onesignal_id')->default('s/n');
            $table->foreign( 'carrera_id')->references('carrera_id')->on('carreras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
