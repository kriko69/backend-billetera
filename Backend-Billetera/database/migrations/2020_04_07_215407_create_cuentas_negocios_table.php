<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuentasNegociosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_negocios', function (Blueprint $table) {
            $table->increments('cuentas_negocios_id');
            $table->integer('cuenta_id')->unsigned();
            $table->integer('negocio_id')->unsigned();

            $table->foreign( 'cuenta_id')->references('cuenta_id')->on('cuentas');
            $table->foreign('negocio_id')->references('negocio_id')->on('negocios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas_negocios');
    }
}
