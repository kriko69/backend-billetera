<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNegociosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('negocios', function (Blueprint $table) {
            $table->increments('negocio_id');
            $table->string('nombre');
            $table->string('descripcion');
            $table->string('hora_inicio');
            $table->string('hora_fin');
            $table->boolean('afiliacion')->default(false);  //false=no afiliado ; true=afiliado
            $table->boolean('estado')->default(false); //false=no borrado ; true=borrado
            $table->string('estado_afiliacion')->default('Pendiante');
            $table->string('public_key')->default('s/n');
            $table->string('nit')->default('0');
            $table->integer('dueno_id')->unsigned();
            $table->foreign( 'dueno_id')->references('usuario_id')->on('usuarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('negocios');
    }
}
