<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transacciones', function (Blueprint $table) {
            $table->increments('transaccion_id');
            $table->double('monto');
            $table->integer('tipo_transaccion_id')->unsigned();
            $table->integer('cuenta_de')->unsigned();
            $table->integer('cuenta_para')->unsigned();
            $table->boolean('estado')->default(false);
            $table->foreign( 'tipo_transaccion_id')->references('tipo_transaccion_id')->on('tipo_transacciones');
            $table->foreign( 'cuenta_de')->references('cuenta_id')->on('cuentas');
            $table->foreign( 'cuenta_para')->references('cuenta_id')->on('cuentas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transacciones');
    }
}
