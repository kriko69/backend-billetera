<?php

use Illuminate\Http\Request;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-type,Authorization, Origin');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Credentials: true');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/rsa','UsuarioController@rsa');

Route::post('/registrar-estudiante','UsuarioController@registrar');
Route::post('/registrar-vendedor','UsuarioController@registrarVendedor');

Route::post('/login','UsuarioController@login');
Route::get('/perfil',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@perfil']);
Route::put('/perfil',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@actualizarPerfil']);
Route::put('/perfil/password',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@actualizarPassword']);
Route::get('/usuarios',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@listarUsuarios']);


Route::post('/negocio',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@registrarNegocio']);

Route::put('/afiliar/negocio',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@afiliarNegocio']);

Route::put('/negocio',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@actualizarNegocio']);

Route::delete('/negocio',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@eliminarNegocio']);
Route::post('/negocio/estudiante',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@verPerfilNegocioParaEstudiante']);
Route::get('/negocios/afiliados',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@listarNegociosAfiliadosYNoEliminados']);
Route::get('/negocios/admin',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@listarTodosLosNegocios']);
//carreras
Route::post('/carrera',[
    'middleware'=>'JWT',
    'uses'=>'CarrerasController@registrar']);
Route::put('/carrera',[
    'middleware'=>'JWT',
    'uses'=>'CarrerasController@actualizar']);
Route::delete('/carrera',[
    'middleware'=>'JWT',
    'uses'=>'CarrerasController@eliminar']);
Route::get('/carreras',[
    'middleware'=>'JWT',
    'uses'=>'CarrerasController@listar']);

//alumnos universidad
Route::post('/alumno-universidad',[
    'middleware'=>'JWT',
    'uses'=>'AlumnoUniversidadController@registrar']);
Route::put('/alumno-universidad',[
    'middleware'=>'JWT',
    'uses'=>'AlumnoUniversidadController@actualizar']);
Route::delete('/alumno-universidad',[
    'middleware'=>'JWT',
    'uses'=>'AlumnoUniversidadController@eliminar']);
Route::get('/alumnos-universidad',[
    'middleware'=>'JWT',
    'uses'=>'AlumnoUniversidadController@listar']);

//cuentas
Route::post('/cuenta/estudiante',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@registrarEstudiante']);
Route::post('/cuenta/proveedor',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@registrarProveedor']);
Route::put('/cuenta',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@actualizar']);

Route::get('/cuentas',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@listar']);

//tipos de transaccion
Route::post('/tipo',[
    'middleware'=>'JWT',
    'uses'=>'TipoTransaccionController@registrar']);
/*Route::put('/cuenta',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@actualizar']);
Route::delete('/cuenta',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@eliminar']);*/
Route::get('/tipos',[
    'middleware'=>'JWT',
    'uses'=>'TipoTransaccionController@listar']);

//deposito
Route::post('/deposito/info/usuario',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@obtenerInformacionUsuarioDeposito']);
Route::put('/depositar',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@depositar']);

//retiro
Route::post('/retiro/info/usuario',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@obtenerInformacionUsuarioRetiro']);
Route::put('/retirar',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@retirar']);

//traspaso
Route::put('/traspaso',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@traspasoCuenta']);

//reset password correo
Route::post('/reset/password',[
    'uses'=>'UsuarioController@resetPassword']);
Route::post('/reset/password/verificar',[
    'uses'=>'UsuarioController@verificarResetPassword']);
Route::post('/reset/password/cambiar',[
    'uses'=>'UsuarioController@cambiarPassword']);

//obtener saldo
Route::get('/usuario/saldo',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@obtenerSaldo']);

//obtener nombres de negocio para pagos
Route::get('/negocios/nombres',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@obtenerNombresNegociosPago']);

//pagar
Route::post('/usuario/pagar',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@pagar']);

//trasferencia
Route::post('/usuario/transferir/datos',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@obtenerDatosTransferir']);
Route::post('/usuario/transferir',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@transferir']);
Route::get('/usuario/transferir/codigo',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@generarCodigoTransferencia']);

//movimientos
Route::post('/usuario/movimientos/de/mi',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@misMovimientosMios']);
Route::post('/usuario/movimientos/para/mi',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@misMovimientosParaMi']);

//mi cuenta
Route::get('/usuario/mi/cuenta',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@obtenerNumeroCuenta']);

//data para qr
Route::post('/negocio/info/qr',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@obtenerDatosParaQR']);

//mis negocios proveedor
Route::get('/negocio/proveedor',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@misNegocios']);

//activar usuario
Route::put('/usuario/activar',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@activarUsuario']);

//editar usuario admin
Route::post('/usuario/editar',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@editarUsuarioAdministrador']);
//eliminar usuario admin
Route::put('/usuario/eliminar',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@eliminarUsuario']);

//eliminar cuenta admin
Route::put('/cuenta/eliminar',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@eliminar']);
//activar cuenta admin
Route::put('/cuenta/activar',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@activar']);
//eliminar negocio admin
Route::put('/negocio/eliminar',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@eliminarNegocio']);
//activar negocio admin
Route::put('/negocio/activar',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@activar']);
//aceptar afiliacion negocio admin
Route::put('/negocio/afiliacion/aceptar',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@aceptarAfiliacion']);
//rechazar afiliacion negocio admin
Route::put('/negocio/afiliacion/rechazar',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@rechazarAfiliacion']);
//usuarios administradores
Route::get('/usuarios/admins',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@listarUsuariosAdministradores']);

//activar usuario administrador
Route::put('/usuario/admin/activar',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@activarUsuarioAdministrador']);
//eliminar usuario administrador
Route::put('/usuario/admin/eliminar',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@eliminarUsuarioAdministrador']);
//registro administrador
Route::post('/registrar-administrador',[
    'middleware'=>'JWT',
    'uses'=>'UsuarioController@registrarAdministrador'
    ]);



//cambio password default
Route::post('/usuario/password/default',[
    'uses'=>'UsuarioController@cambiarPasswordDefault']);

//reintertar afiliacion negocio rechazado
Route::put('/usuario/reintentar/afiliacion',[
    'uses'=>'NegocioController@reintentarAfiliacion']);
//retiro de dinero de cuenta de estudiante fin carrera
Route::put('/usuario/retiro/fin/carrera',[
    'uses'=>'UsuarioController@retirarDineroEstudianteFinCarrera']);

//actualizar foto de perfil
Route::put('/usuario/foto',[
    'uses'=>'UsuarioController@actualizarFotoPerfil']);
//eliminar foto de perfil
Route::put('/usuario/foto/eliminar',[
    'uses'=>'UsuarioController@eliminarFotoPerfil']);

//onesignal id
Route::post('/usuario/onesignal',[
    'uses'=>'UsuarioController@guardarOneSignalId']);

//super administrador genesis
Route::post('/usuario/super/administrador/genesis',[
    'uses'=>'UsuarioController@crearSuperAdministradorGenesis']);

//creacion de universidad
Route::post('/universidad',[
    'middleware'=>'JWT',
    'uses'=>'NegocioController@creacionUniversidad']);

//crear cuenta universidad
Route::post('/negocio/universidad/cuenta',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@cuentaUniversidad']);

//listar gestiones
Route::get('/gestiones',[
    'middleware'=>'JWT',
    'uses'=>'GestionController@listarGestiones']);
//registrar gestion
Route::post('/gestion',[
    'middleware'=>'JWT',
    'uses'=>'GestionController@registrarGestion']);
//eliminar gestion
Route::put('/gestion/eliminar',[
    'middleware'=>'JWT',
    'uses'=>'GestionController@eliminarGestion']);
//activar gestion
Route::put('/gestion/activar',[
    'middleware'=>'JWT',
    'uses'=>'GestionController@activarGestion']);

//registrar mensualidad
Route::post('/mensualidad',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@registrar']);
//listar mensualidades
Route::post('/mensualidades',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@listar']);

//editar mensualidad
Route::put('/mensualidad',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@actualizar']);

//listar mensualidades para estudiante no pagadas
Route::post('/mensualidades/estudiante/nopagadas',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@listarMensualidadesNoPagadas']);
//listar mensualidades para estudiante  pagadas
Route::post('/mensualidades/estudiante/pagadas',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@listarMensualidadesPagadas']);

//listar gestiones estudiante
Route::get('/gestiones/estudiante',[
    'middleware'=>'JWT',
    'uses'=>'GestionController@listarGestionesEstudiante']);
//pagar mensualidad
Route::post('/mensualidades/estudiante/pagar',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@pagarMensualidad']);
//saldo de la universidad
Route::get('/universidad/saldo',[
    'middleware'=>'JWT',
    'uses'=>'CuentasController@obtenerTotalUniversidad']);

//lista de pagos de mensualidades
Route::post('/mensualidades/pagos',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@pagosMensualidades']);

//lista de pagos por gestion
Route::post('/gestiones/pagos',[
    'middleware'=>'JWT',
    'uses'=>'MensualidadController@pagosMensualidadesPorGestion']);

//reset onesignal id
Route::put('/usuario/onesignal/reset',[
    'uses'=>'UsuarioController@resetOneSignalId']);

//obtener onesignal id por usuario_id
Route::post('/usuario/obtener/onesignal',[
    'uses'=>'UsuarioController@obtenerOneSignalId']);












//pruebas encriptacion
Route::post('/encriptar',[
    'uses'=>'UsuarioController@encriptarBase64']);

Route::post('/desencriptar',[
    'uses'=>'UsuarioController@desencriptarBase64']);


