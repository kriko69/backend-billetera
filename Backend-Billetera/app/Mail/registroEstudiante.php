<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 01-04-20
 * Time: 05:14 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class registroEstudiante extends Mailable
{
    use Queueable, SerializesModels;

    public $password,$numero_cuenta,$nombre;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Registro exitosa en la billetera movil.';
    public function __construct($nombre,$password,$numero_cuenta)
    {
        $this->nombre=$nombre;
        $this->password=$password;
        $this->numero_cuenta=$numero_cuenta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'billeteramovil@gmail.com';
        $subject = 'Registro exitosa en la billetera movil..';
        $name = 'Billetera movil';

        return $this->view('correos.registro-estudiante')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with('password',$this->password)->with('numero_cuenta',$this->numero_cuenta)->with('nombre',$this->nombre);
    }

}
