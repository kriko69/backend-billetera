<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 01-04-20
 * Time: 05:14 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class registroAdministrador extends Mailable
{
    use Queueable, SerializesModels;

    public $password,$apellido,$nombre;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Registro exitosa en la billetera movil.';
    public function __construct($nombre,$apellido,$password)
    {
        $this->nombre=$nombre;
        $this->apellido=$apellido;
        $this->password=$password;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'billeteramovil@gmail.com';
        $subject = 'Registro exitoso en la billetera movil..';
        $name = 'Billetera movil';

        return $this->view('correos.registro-administrador')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with('password',$this->password)->with('apellido',$this->apellido)->with('nombre',$this->nombre);
    }

}
