<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 01-04-20
 * Time: 05:14 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $usuario,$codigo;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Recuperacion de Contraseña.';
    public function __construct($usuario,$codigo)
    {
        $this->codigo=$codigo;
        $this->usuario=$usuario;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'billeteramovil@gmail.com';
        $subject = 'Recuperacion de Contraseña.';
        $name = 'Billetera movil';

        return $this->view('correos.reset-password')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with('usuario',$this->usuario)->with('codigo',$this->codigo);
    }

}
