<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 01-04-20
 * Time: 05:14 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class rechazarAfiliacion extends Mailable
{
    use Queueable, SerializesModels;

    public $nombreDueno,$nombreNegocio;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject = 'Afiliacion exitosa en la billetera movil.';
    public function __construct($nombreDueno,$nombreNegocio)
    {
        $this->nombreDueno=$nombreDueno;
        $this->nombreNegocio=$nombreNegocio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'billeteramovil@gmail.com';
        $subject = 'Afiliacion rechazada en la billetera movil..';
        $name = 'Billetera movil';

        return $this->view('correos.rechazar-afiliacion')
            ->from($address, $name)
            ->cc($address, $name)
            ->bcc($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with('nombre_dueno',$this->nombreDueno)->with('nombre_negocio',$this->nombreNegocio);
    }

}
