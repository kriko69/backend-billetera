<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-07-20
 * Time: 06:10 PM
 */

namespace App\Helpers;


class Cifrado_Http
{

    public function encriptarBase64($data)
    {

        //convertir a json
        $data=json_encode($data); //ya es json

        //codifico base64
        $base64Data=base64_encode($data);

        //contamos iguales
        $iguales=substr_count($base64Data,"=");

        //quitamos iguales
        if($iguales!=0)
        {
            //quitamos iguales
            $base64Data=substr($base64Data,0,strlen($base64Data)-$iguales);
        }

        //cada 3 caracteres reemplazaremos un caracter

        $longitudCadena=strlen($base64Data);

        $cociente=$longitudCadena/3;

        $cociente=floor($cociente); //redondeamos siempre hacia abajo
        $resto=$longitudCadena%3;
        if($resto==0)
        {
            //sumo cociente y resto
            $cantidadCaracteresGenerar=$cociente+$resto;
            $cadenaAleatoria=$this->generarCadenaAleatoria($cantidadCaracteresGenerar);
        }else{
            if($resto==1)
            {
                //sumo cociente y resto
                $cantidadCaracteresGenerar=$cociente+$resto;
                $cadenaAleatoria=$this->generarCadenaAleatoria($cantidadCaracteresGenerar);
            }else{
                //resto es igual a 2
                //sumo uno al cociente
                $cantidadCaracteresGenerar=$cociente+1;
                $cadenaAleatoria=$this->generarCadenaAleatoria($cantidadCaracteresGenerar);
            }
        }

        if($resto==0)
        {
            return $this->encriptarModulo0($data,$iguales,$base64Data,$cadenaAleatoria);
        }else{
            if($resto==1)
            {
                return $this->encriptarModulo1($data,$iguales,$base64Data,$cadenaAleatoria);
            }else{
                if($resto==2)
                {
                    return $this->encriptarModulo2($data,$iguales,$base64Data,$cadenaAleatoria);
                }else{
                    $respuesta=array(
                        'mensaje'=>'Modulo no esta en el rango permitido',
                        'estado'=>'error'
                    );
                    return $respuesta;
                }

            }
        }



    }

    public function generarCadenaAleatoria($length) {

        $letras="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $letras_length = strlen($letras);
        $random_string = '';
        for($i = 0; $i < $length; $i++) {
            $random_character = $letras[mt_rand(0, $letras_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }

    public function encriptarModulo0($data,$iguales,$base64Data,$cadenaAleatoria)
    {
        $caracteresReemplazados="";
        $nuevaBaseJsonData="";
        $longitud=strlen($base64Data); //longitud de la cadena
        for ( $i = 0; $i <$longitud ; $i++) {

            $indice=$i+1; //para que no de error 0/3
            if($indice%3==0) //es multiplo de 3
            {
                $caracteresReemplazados=$caracteresReemplazados.$base64Data[$i]; //concatenamos lo caracteres reemplados
            }else{
                $nuevaBaseJsonData=$nuevaBaseJsonData.$base64Data[$i]; //concatenamos los caracteres no multiplos de 3
            }

        }

        $cadenaCambiada="";
        $aux1=0;
        $longitud2=strlen($cadenaAleatoria); // longitud de los caracteres aleatorios
        //creamos la cadena cambiada
        for ($i=0;$i<$longitud2;$i++)
        {
            $slice=substr($nuevaBaseJsonData,$aux1,2); //obtengo substring de 0-2, 2-4
            $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
            $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
            $aux1=$aux1+2; //aumento el indice para crear una nueva subcadena
        }

        $inverso="";
        $primeraMitad="";
        $segundaMitad="";
        $cociente=strlen($cadenaCambiada)/2;
        $cociente=floor($cociente); //redondeamos siempre hacia abajo

        if(strlen($cadenaCambiada)%2==0)
        {
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,$cociente);
            $inverso=$segundaMitad.$primeraMitad;
        }else{
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,($cociente+1));
            $inverso=$segundaMitad.$primeraMitad;
        }

        $reverso=strrev($inverso);

        $dataVer = array(
            /*'data original'=>$data,
            'iguales'=>$iguales,
            '$base64Data'=>$base64Data,
            '$cadenaAleatoria' => $cadenaAleatoria,
            '$caracteresReemplazados'=>$caracteresReemplazados,
            '$nuevaBaseJsonData sin caracteres'=>$nuevaBaseJsonData,
            '$cadenaCambiada con caracteres'=>$cadenaCambiada,
            '$cadenaCambiada inversa'=>$inverso,
            'primera mitad'=>$primeraMitad,
            'segunda mitad'=>$segundaMitad,
            'longitud primera mitad'=>strlen($primeraMitad),
            'longitud segunda mitad'=>strlen($segundaMitad),
            '$cadenaCambiada invertida'=>$reverso,
            'longitud base64Data'=>$longitud,
            'longitud cadenaCambiada'=>strlen($cadenaCambiada),
            'longitud caracteres generados'=>$longitud2,
            'modulo'=>0*/
            'reverso'=>$reverso,
            'caracteres_reemplazados'=>$caracteresReemplazados,
            'cadena_aleatoria' => $cadenaAleatoria,
            'modulo'=>0,
            'iguales'=>$iguales
        );
        return $dataVer;

    }

    public function encriptarModulo1($data,$iguales,$base64Data,$cadenaAleatoria)
    {
        $caracteresReemplazados="";
        $nuevaBaseJsonData="";
        $longitud=strlen($base64Data); //longitud de la cadena
        for ( $i = 0; $i <$longitud ; $i++) {

            $indice=$i+1; //para que no de error 0/3
            if($indice%3==0) //es multiplo de 3
            {
                $caracteresReemplazados=$caracteresReemplazados.$base64Data[$i]; //concatenamos lo caracteres reemplados
            }else{
                $nuevaBaseJsonData=$nuevaBaseJsonData.$base64Data[$i]; //concatenamos los caracteres no multiplos de 3
            }

        }

        $cadenaCambiada="";
        $aux1=0;
        $parar=0;
        $longitud2=strlen($cadenaAleatoria); // longitud de los caracteres aleatorios
        //creamos la cadena cambiada
        for ($i=0;$i<$longitud2;$i++)
        {
            if($parar==($longitud2-1))
            {
                $slice=substr($nuevaBaseJsonData,$aux1,1); //obtengo la ultima letra porque aux esta penultimo
                $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
                $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
                break;
            }else{
                $slice=substr($nuevaBaseJsonData,$aux1,2); //obtengo substring de 0-2, 2-4
                $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
                $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
                $aux1=$aux1+2; //aumento el indice para crear una nueva subcadena
                $parar=$parar+1;
            }
        }

        $inverso="";
        $primeraMitad="";
        $segundaMitad="";
        $cociente=strlen($cadenaCambiada)/2;
        $cociente=floor($cociente); //redondeamos siempre hacia abajo
        if(strlen($cadenaCambiada)%2==0)
        {
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,$cociente);
            $inverso=$segundaMitad.$primeraMitad;
        }else{
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,($cociente+1));
            $inverso=$segundaMitad.$primeraMitad;
        }

        $reverso=strrev($inverso);

        $dataVer = array(
            'reverso'=>$reverso,
            'caracteres_reemplazados'=>$caracteresReemplazados,
            'cadena_aleatoria' => $cadenaAleatoria,
            'modulo'=>1,
            'iguales'=>$iguales
        );
        return $dataVer;

    }

    public function encriptarModulo2($data,$iguales,$base64Data,$cadenaAleatoria)
    {

        $caracteresReemplazados="";
        $nuevaBaseJsonData="";

        $longitud=strlen($base64Data); //longitud de la cadena
        for ( $i = 0; $i <$longitud ; $i++) {

            $indice=$i+1; //para que no de error 0/3
            if($indice%3==0) //es multiplo de 3
            {
                $caracteresReemplazados=$caracteresReemplazados.$base64Data[$i]; //concatenamos lo caracteres reemplados
            }else{
                $nuevaBaseJsonData=$nuevaBaseJsonData.$base64Data[$i]; //concatenamos los caracteres no multiplos de 3
            }

        }

        $cadenaCambiada="";
        $aux1=0;
        $parar=0;
        $longitud2=strlen($cadenaAleatoria); // longitud de los caracteres aleatorios
        //creamos la cadena cambiada
        for ($i=0;$i<$longitud2;$i++)
        {
            $slice=substr($nuevaBaseJsonData,$aux1,2); //obtengo substring de 0-2, 2-4
            $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
            $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
            $aux1=$aux1+2; //aumento el indice para crear una nueva subcadena
        }

        $inverso="";
        $primeraMitad="";
        $segundaMitad="";
        $cociente=strlen($cadenaCambiada)/2;
        $cociente=floor($cociente); //redondeamos siempre hacia abajo

        if(strlen($cadenaCambiada)%2==0)
        {
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,$cociente);
            $inverso=$segundaMitad.$primeraMitad;
        }else{
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,($cociente+1));
            $inverso=$segundaMitad.$primeraMitad;
        }

        $reverso=strrev($inverso);

        $dataVer = array(
            'reverso'=>$reverso,
            'caracteres_reemplazados'=>$caracteresReemplazados,
            'cadena_aleatoria' => $cadenaAleatoria,
            'modulo'=>2,
            'iguales'=>$iguales
        );
        return $dataVer;

    }


    public function desencriptarBase64($data)
    {
        //$data=json_decode($data,true);
        $reverso = $data["reverso"];
        $caracteresReemplazados = $data["caracteres_reemplazados"];
        $cadenaAleatoria = $data["cadena_aleatoria"];
        $modulo = $data["modulo"];
        $iguales = $data["iguales"];
        $inverso = strrev($reverso);
        $primeraMitad = "";
        $segundaMitad = "";
        if ($modulo == 0) {
            $cociente = strlen($inverso) / 2;
            $cociente = floor($cociente); //redondeamos siempre hacia abajo
            if (strlen($inverso) % 2 == 0) {
                $primeraMitad = substr($inverso, 0, $cociente);
                $segundaMitad = substr($inverso, $cociente, $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            } else {
                $primeraMitad = substr($inverso, 0, ($cociente + 1));
                $segundaMitad = substr($inverso, ($cociente + 1), $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            }

            $caracteresReemplazadosDC = "";
            $nuevaBaseJsonDataDC = "";

            $longitud = strlen($inverso); //longitud de la cadena
            for ($i = 0; $i < $longitud; $i++) {

                $indice = $i + 1; //para que no de error 0/3
                if ($indice % 3 == 0) //es multiplo de 3
                {
                    $caracteresReemplazadosDC = $caracteresReemplazadosDC . $inverso[$i]; //concatenamos lo caracteres reemplados
                } else {
                    $nuevaBaseJsonDataDC = $nuevaBaseJsonDataDC . $inverso[$i]; //concatenamos los caracteres no multiplos de 3
                }

            }
            if ($caracteresReemplazadosDC == $cadenaAleatoria) //comprobacion
            {

                $cadenaCambiada = "";
                $aux1 = 0;
                $longitud2 = strlen($caracteresReemplazados); // longitud de los caracteres aleatorios
                //creamos la cadena cambiada
                for ($i = 0; $i < $longitud2; $i++) {
                    $slice = substr($nuevaBaseJsonDataDC, $aux1, 2); //obtengo substring de 0-2, 2-4
                    $add = $slice . $caracteresReemplazados[$i]; //le concateno el caracter aleatorio
                    $cadenaCambiada = $cadenaCambiada . $add; //creo la cadena
                    $aux1 = $aux1 + 2; //aumento el indice para crear una nueva subcadena
                }

                for ($i = 0; $i < $iguales; $i++) {
                    $cadenaCambiada = $cadenaCambiada . '=';
                }

                $var = json_decode(base64_decode($cadenaCambiada), true);
                return $var;
                //fin modulo 0
            } else {
                $data = array(
                    'mensaje' => 'no se puede decodificar',
                    'estado'=>'error'
                );
                $data=json_decode($data,true);
                return $data;
            }
        } else {
            //modulo 1 o 2
            $cociente = strlen($inverso) / 2;
            $cociente = floor($cociente); //redondeamos siempre hacia abajo
            if (strlen($inverso) % 2 == 0) {
                $primeraMitad = substr($inverso, 0, $cociente);
                $segundaMitad = substr($inverso, $cociente, $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            } else {
                $primeraMitad = substr($inverso, 0, ($cociente + 1));
                $segundaMitad = substr($inverso, ($cociente + 1), $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            }

            if ($modulo == 1) {
                $caracteresReemplazadosDC = "";
                $nuevaBaseJsonDataDC = "";

                $longitud = strlen($inverso); //longitud de la cadena
                for ($i = 0; $i < $longitud; $i++) {

                    $indice = $i + 1; //para que no de error 0/3
                    if ($indice % 3 == 0) //es multiplo de 3
                    {
                        $caracteresReemplazadosDC = $caracteresReemplazadosDC . $inverso[$i]; //concatenamos lo caracteres reemplados
                    } else {
                        $nuevaBaseJsonDataDC = $nuevaBaseJsonDataDC . $inverso[$i]; //concatenamos los caracteres no multiplos de 3
                    }

                }
                //$caracteresReemplazadosDC tiene los ultimos caracteres menos el ultimo
                //concatenamos el ultimo caracter para la verificacion
                $ultimo_caracter = substr($inverso, (strlen($inverso) - 1), 1);
                $caracteresReemplazadosDC = $caracteresReemplazadosDC . $ultimo_caracter;
                if ($caracteresReemplazadosDC == $cadenaAleatoria) {
                    $nuevaBaseJsonDataDC = substr($nuevaBaseJsonDataDC, 0, (strlen($nuevaBaseJsonDataDC) - 1)); //elimino el ultimo caracter

                    $ultima_letra = substr($nuevaBaseJsonDataDC, (strlen($nuevaBaseJsonDataDC) - 1), 1);

                    $cadenaCambiada = "";
                    $aux1 = 0;
                    $parar = 0;
                    $longitud2 = strlen($caracteresReemplazados); // longitud de los caracteres aleatorios
                    //creamos la cadena cambiada
                    for ($i = 0; $i < $longitud2; $i++) {
                        $slice = substr($nuevaBaseJsonDataDC, $aux1, 2); //obtengo substring de 0-2, 2-4
                        $add = $slice . $caracteresReemplazados[$i]; //le concateno el caracter aleatorio
                        $cadenaCambiada = $cadenaCambiada . $add; //creo la cadena
                        $aux1 = $aux1 + 2; //aumento el indice para crear una nueva subcadena
                    }
                    $cadenaCambiada = $cadenaCambiada . $ultima_letra;

                    for ($i = 0; $i < $iguales; $i++) {
                        $cadenaCambiada = $cadenaCambiada . '=';
                    }

                    $var = json_decode(base64_decode($cadenaCambiada), true);
                    return $var;
                }else{
                    $data = array(
                        'mensaje' => 'no se puede decodificar',
                        'estado'=>'error'
                    );
                    $data=json_decode($data,true);
                    return $data;
                }


            } else {
                //modulo 2

                $caracteresReemplazadosDC = "";
                $nuevaBaseJsonDataDC = "";

                $longitud = strlen($inverso); //longitud de la cadena
                for ($i = 0; $i < $longitud; $i++) {

                    $indice = $i + 1; //para que no de error 0/3
                    if ($indice % 3 == 0) //es multiplo de 3
                    {
                        $caracteresReemplazadosDC = $caracteresReemplazadosDC . $inverso[$i]; //concatenamos lo caracteres reemplados
                    } else {
                        $nuevaBaseJsonDataDC = $nuevaBaseJsonDataDC . $inverso[$i]; //concatenamos los caracteres no multiplos de 3
                    }

                }
                //$caracteresReemplazadosDC tiene los ultimos caracteres y como su longitud es par
                // ya no concatenamos el ultimo caracter para la verificacion

                if ($caracteresReemplazadosDC == $cadenaAleatoria) {

                    //nuevaCadenaDC esta bien

                    $ultimas_2_letra = substr($nuevaBaseJsonDataDC, (strlen($nuevaBaseJsonDataDC) - 2), 2);

                    $cadenaCambiada = "";
                    $aux1 = 0;
                    $parar = 0;
                    $longitud2 = strlen($caracteresReemplazados); // longitud de los caracteres aleatorios
                    //creamos la cadena cambiada
                    for ($i = 0; $i < $longitud2; $i++) {
                        $slice = substr($nuevaBaseJsonDataDC, $aux1, 2); //obtengo substring de 0-2, 2-4
                        $add = $slice . $caracteresReemplazados[$i]; //le concateno el caracter aleatorio
                        $cadenaCambiada = $cadenaCambiada . $add; //creo la cadena
                        $aux1 = $aux1 + 2; //aumento el indice para crear una nueva subcadena
                    }
                    $cadenaCambiada = $cadenaCambiada . $ultimas_2_letra;

                    for ($i = 0; $i < $iguales; $i++) {
                        $cadenaCambiada = $cadenaCambiada . '=';
                    }

                    $var = json_decode(base64_decode($cadenaCambiada), true);
                    return $var;
                }else{
                    $data = array(
                        'mensaje' => 'no se puede decodificar',
                        'estado'=>'error'
                    );
                    $data=json_decode($data,true);
                    return $data;
                }
            }

        }
    }
}
