<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 10-03-20
 * Time: 04:40 PM
 */

namespace App\Helpers;
use App\Http\Controllers\Dao\UsuarioDao;
use Firebase\JWT\JWT;
use function PHPSTORM_META\type;

Class JwtAuth{

    public $key;

    public function __construct()
    {
        $this->key='billetera movil';
    }

    //login
    public function signUp($carnet,$password,$getToken=null)
    {
        //comprobarr si el usuario exite

        $usuarioDao=new UsuarioDao();

        $user = $usuarioDao->obtenerUsuarioParaToken($carnet,$password);
        $signUp=false;
        if (!is_null($user))
        {
            $signUp=true;
        }

        if($signUp)
        {
            $roles=array();
            foreach ($user->roles as $rol)
            {
                array_push ($roles,$rol->pivot->rol_id);
            }
            //generar token
            $tokenVerificacion=array(
                'sub' => $user->usuario_id,
                'carnet' => $user->carnet,
                'roles' => $roles,
                'type'=> 'verificacion',
                'iat' => time(),
                'exp' => time() + (60*60), //1 hora
            );

            /*$tokenRefresh=array(
                'sub' => $user->usuario_id,
                'usuario' => $user->usuario,
                'roles' => $roles,
                'type'=> 'refresh',
                'iat' => time(),
                'exp' => time() + (2*60*60), //2 horas
            );*/

            $jwt1 = JWT::encode($tokenVerificacion,$this->key,'HS256');
            //$jwt2 = JWT::encode($tokenRefresh,$this->key,'HS256');
            $decoded = JWT::decode($jwt1,$this->key,array('HS256'));

            if (is_null($getToken))
            {
                $roles_usuario=array();
                foreach ($user->roles as $rol)
                {
                    array_push ($roles_usuario,$rol->nombre);
                }
                $data=array(
                    'Token-Verificacion'=>$jwt1,
                    //'Token-Refresh'=>$jwt2,
                    'roles'=>implode(", ",$roles_usuario),
                    'estado'=>'Tokens creados con exito'
                );
                //$cifrado= new Cifrado_Http();
                //$data=json_encode($data,true);
                //$info=$cifrado->encriptarBase64($data);
                //$data=$info;
            }else{
                $data=$decoded;

            }

        }else{
            //devolver error
            $data=array(
                'mensaje'=>'El usuario no existe o el password es incorrecto.',
                'estado'=>'error'
            );
            //$cifrado= new Cifrado_Http();
            //$data=json_encode($data,true);
            //$info=$cifrado->encriptarBase64($data);
            //$data=$info;
        }
        return $data;
    }

    public function getTokenUser($getToken=null)
    {
        //comprobarr si el usuario exite

        $usuario = "pepe";
        $password = "master69"; //cambiar para que funcione
        $password=$password.'turismo';
        $password=hash('sha256',$password);
        $usuarioDao=new UsuarioDao();

        $user = $usuarioDao->obtenerUsuarioParaToken($usuario,$password);
        $signUp=false;
        if (!is_null($user))
        {
            $signUp=true;
        }

        if($signUp)
        {
            //generar token
            $tokenVerificacion=array(
                'sub' => $user->usuario_id,
                'usuario' => $user->usuario,
                'type'=> 'verificacion',
                'iat' => time(),
                'exp' => time() + (60*60), //1 hora
            );
            $jwt1 = JWT::encode($tokenVerificacion,$this->key,'HS256');
            $decoded = JWT::decode($jwt1,$this->key,array('HS256'));

            if (is_null($getToken))
            {
                $data=$jwt1;
            }else{
                $data=$decoded;

            }

        }else{
            //devolver error
            $data=array(
                'mensaje'=>'El usuario no existe.',
                'estado'=>'error'
            );
        }
        return $data;
    }

    /*
    public function signUpEmpresa($correo,$password,$getToken=null)
    {
        //comprobarr si el usuario exite

        $empresaDao=new EmpresaDao();

        $user = $empresaDao->obtenerEmpresaParaToken($correo,$password);
        $signUp=false;

        if (is_object($user))
        {
            $signUp=true;
        }

        if($signUp)
        {
            //generar token
            $tokenVerificacion=array(
                'sub' => $user->empresa_id,
                'correo' => $user->correo,
                'type'=> 'verificacion',
                'iat' => time(),
                'exp' => time() + (60*60), //1 hora
            );

            $tokenRefresh=array(
                'sub' => $user->empresa_id,
                'correo' => $user->correo,
                'type'=> 'refresh',
                'iat' => time(),
                'exp' => time() + (2*60*60), //2 horas
            );

            $jwt1 = JWT::encode($tokenVerificacion,$this->key,'HS256');
            $jwt2 = JWT::encode($tokenRefresh,$this->key,'HS256');
            $decoded = JWT::decode($jwt1,$this->key,array('HS256'));

            if (is_null($getToken))
            {
                $data=array(
                    'Token-Verificacion'=>$jwt1,
                    'Token-Refresh'=>$jwt2,
                    'estado'=>'Tokens creados con exito'
                );
            }else{
                $data=$decoded;

            }

        }else{
            //devolver error
            $data=array(
                'mensaje'=>'La empresa no existe.',
                'estado'=>'error'
            );
        }
        return $data;
    }

    public function getTokenEmpresa($getToken=null)
    {
        //comprobarr si el usuario exite

        $correo = "kevin.bonilla.kbs17@gmail.com";
        $password = "123abc";
        $password=$password.'software-1';
        $password=hash('sha256',$password);
        $empresaDao=new EmpresaDao();

        $user = $empresaDao->obtenerEmpresaParaToken($correo,$password);
        $signUp=false;

        if (is_object($user))
        {
            $signUp=true;
        }

        if($signUp)
        {
            //generar token
            $tokenVerificacion=array(
                'sub' => $user->empresa_id,
                'correo' => $user->correo,
                'type'=> 'verificacion',
                'iat' => time(),
                'exp' => time() + (60*60), //1 hora
            );

            $jwt1 = JWT::encode($tokenVerificacion,$this->key,'HS256');
            $decoded = JWT::decode($jwt1,$this->key,array('HS256'));

            if (is_null($getToken))
            {
                $data=$jwt1;
            }else{
                $data=$decoded;

            }

        }else{
            //devolver error
            $data=array(
                'mensaje'=>'La empresa no existe.',
                'estado'=>'error',
                'correo'=>$correo.$password
            );
        }
        return $data;
    }*/


    public function checkToken($jwt,$getIdentity=false)
    {
        $auth=false;
        $decoded=null;
        try{
            $decoded = JWT::decode($jwt,$this->key,array('HS256'));
        }catch (\UnexpectedValueException $e){
            $auth=false;
        }catch (\DomainException $e){
            $auth=false;
        }


        if (is_object($decoded) && isset($decoded))
        {
            $auth=true;
        }else{
            $auth=false;
        }

        if ($getIdentity)
        {
            return $decoded;
        }


        return $auth;
    }

    public function verificarToken($jwt)
    {
        $decoded=null;
        try{
            $decoded = JWT::decode($jwt,$this->key,array('HS256'));
        }catch (\UnexpectedValueException $e){
            $decoded=false;
        }catch (\DomainException $e){
            $decoded=false;
        }
        return $decoded;
    }

}
