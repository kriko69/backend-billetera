<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $table = 'cuentas';
    protected $primaryKey='cuenta_id';
    protected $fillable = [
        'cuenta_id','numero_cuenta','saldo','estado','usuario_id'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }
    public function negocios(){
        return $this->belongsToMany(Negocio::class,'cuentas_negocios','cuenta_id','negocio_id');
    }
}
