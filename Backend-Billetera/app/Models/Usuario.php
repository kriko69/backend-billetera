<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey='usuario_id';
    protected $fillable = [
        'usuario_id','nombre','apellidos','carnet','correo','telefono','carrera_id','fecha_nacimiento','password','imagen','estado','password_default','onesignal_id'
    ];

    protected $hidden = [
        'password',
    ];


    public function roles(){
        return $this->belongsToMany(Rol::class,'roles_usuarios','usuario_id','rol_id');
    }
    public function mensualidades(){
        return $this->belongsToMany(Mensualidad::class,'mensualidades_usuarios','usuario_id','mensualidad_id')->withTimestamps();
    }
    public function negocios(){
        return $this->hasMany(Negocio::class,'usuario_id');
    }

    public function cuentas(){
        return $this->hasMany(Cuenta::class,'usuario_id');
    }
    public function carrera()
    {
        return $this->belongsTo(Carrera::class,'carrera_id');
    }
}
