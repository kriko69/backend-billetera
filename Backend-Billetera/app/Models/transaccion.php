<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class transaccion extends Model
{
    protected $table = 'transacciones';
    protected $primaryKey='transaccion_id';
    protected $fillable = [
        'transaccion_id','monto','cuenta_de','cuenta_para','estado','tipo_transaccion_id'
    ];

    public function tipo_transaccion()
    {
        return $this->belongsTo(tipo_transaccion::class,'tipo_transaccion_id');
    }
}
