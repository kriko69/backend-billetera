<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CuentaNegocio extends Model
{
    protected $table = 'cuentas_negocios';
    protected $primaryKey='cuentas_negocios_id';
    protected $fillable = [
        'cuentas_negocios_id','cuenta_id','negocio_id'
    ];
}
