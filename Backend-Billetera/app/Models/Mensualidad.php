<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-06-20
 * Time: 04:16 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mensualidad extends Model
{
    protected $table = 'mensualidades';
    protected $primaryKey='mensualidad_id';
    protected $fillable = [
        'mensualidad_id','nombre','fecha_pago','monto','numero_pago','estado','gestion_id'
    ];

    public function gestion()
    {
        return $this->belongsTo(Gestion::class, 'usuario_id');
    }

    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class,'mensualidades_usuarios','mensualidad_id','usuario_id')->withTimestamps();
    }
}
