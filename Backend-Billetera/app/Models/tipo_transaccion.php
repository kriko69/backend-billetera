<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tipo_transaccion extends Model
{
    protected $table = 'tipo_transacciones';
    protected $primaryKey='tipo_transaccion_id';
    protected $fillable = [
        'tipo_transaccion_id','nombre','descripcion','estado'
    ];

    public function transacciones()
    {
        return $this->hasMany(transaccion::class,'tipo_transaccion_id');
    }
}
