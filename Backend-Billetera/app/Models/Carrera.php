<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    protected $table = 'carreras';
    protected $primaryKey='carrera_id';
    protected $fillable = [
        'carrera_id','nombre','estado'
    ];

    public function usuarios(){
        return $this->hasMany(Usuario::class,'carrera_id');
    }


}
