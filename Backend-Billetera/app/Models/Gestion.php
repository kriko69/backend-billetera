<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-06-20
 * Time: 04:18 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gestion extends Model
{
    protected $table = 'gestiones';
    protected $primaryKey='gestion_id';
    protected $fillable = [
        'gestion_id','semestre','ano','estado'
    ];

    public function mensualidades()
    {
        return $this->hasMany(Mensualidad::class, 'gestion_id');
    }
}
