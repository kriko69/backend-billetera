<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alumno_Universidad extends Model
{
    protected $table = 'alumnos_universidad';
    protected $primaryKey='alumno_universidad_id';
    protected $fillable = [
        'alumno_universidad_id','carnet','ciudad','carrera'
    ];
}
