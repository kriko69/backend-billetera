<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Negocio extends Model
{
    protected $table = 'negocios';
    protected $primaryKey='negocio_id';
    protected $fillable = [
        'negocio_id','nombre','descripcion','hora_inicio','hora_fin','afiliacion',/*'imagen',*/'estado','dueno_id','estado_afiliacion'
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }

    public function cuentas(){
        return $this->belongsToMany(Cuenta::class,'cuentas_negocios','negocio_id','cuenta_id');
    }

}
