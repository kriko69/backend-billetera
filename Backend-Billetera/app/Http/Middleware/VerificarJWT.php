<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\JwtAuth;


class VerificarJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token=$request->header('Authorization',null);

        //$id=$request->route('id');

        if (!isset($token) || is_null($token))
        {
            $data=array(
                'mensaje'=>'el token es null o no existe',
                "estado"=>'error'
            );
            return response()->json($data);
        }else{
            //existe
            $jwt= new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                //si es invalido
                $data=array(
                    'mensaje'=>'el token ha expirado.',
                    "estado"=>'error'
                );
                return response()->json($data);
            }else{
                //token valido
                if ($payload->type=='verificacion')
                {
                    //verificacion
                    if($payload->exp >= time())
                    {
                        return $next($request);
                    }else{
                        $data=array(
                            'mensaje'=>'el token de verificacion ha expirado.',
                            "estado"=>'error'
                        );
                        return response()->json($data);
                    }

                }elseif ($payload->type=='refresh'){
                    //refresh
                    if($payload->exp >= time())
                    {
                        $usuario=Usuario::find($payload->sub);

                        $nuevosTokens=$jwt->signUp($usuario->usuario,$usuario->password);
                        return response()->json($nuevosTokens);
                    }else{
                        $data=array(
                            'mensaje'=>'el token de rerfresh ha expirado necesita loguearse de nuevo'
                        );
                        return response()->json($data);
                    }

                }else{
                    $data=array(
                        'mensaje'=>'el token es incorrecto',
                        "estado"=>'error'
                    );
                    return response()->json($data);
                }
            }
        }
    }
}
