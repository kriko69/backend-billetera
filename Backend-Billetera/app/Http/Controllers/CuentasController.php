<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\CuentasBl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CuentasController extends Controller
{
    public function registrarEstudiante(Request $request)
    {

        /*$rules = [
            'numero_cuenta' => 'required|numeric',
            'saldo' => 'required|numeric',
            'usuario_id'=>'required'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $saldo=$request->json("saldo");
        $pass=$request->json('password-default');
        $usuario_id=$request->json("usuario_id");
        $token=$request->header('Authorization',null);
        if(!is_null($saldo) && !is_null($usuario_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->registrarEstudiante($saldo,$usuario_id,$pass);
            }
        }else{
            $data=array(

                'descripcion'=>'algun parametro en null',
                'estado'=>'error',
            );
            return response()->json($data);
        }



    }

    public function registrarProveedor(Request $request)
    {

        $rules = [
            'numero_cuenta' => 'required|numeric',
            'saldo' => 'required|numeric',
            'usuario_id'=>'required'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }

        $numero_cuenta=$request->json("numero_cuenta");
        $saldo=$request->json("saldo");
        $usuario_id=$request->json("usuario_id");
        $token=$request->header('Authorization',null);
        if(!is_null($numero_cuenta) && !is_null($saldo) && !is_null($usuario_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->registrarProveedor($payload->sub,$payload->roles,$numero_cuenta, $saldo,$usuario_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }



    }

    public function actualizar(Request $request)
    {
        $rules = [
            'cuenta_id'=>'required',
            'saldo' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $cuenta_id=$request->json("cuenta_id");
        $saldo=$request->json("saldo");
        $token=$request->header('Authorization',null);
        if(!is_null($cuenta_id) && !is_null($saldo)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->actualizar($payload->sub,$payload->roles,$cuenta_id,$saldo);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }
    }

    public function eliminar(Request $request)
    {
        /*$rules = [
            'cuenta_id' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $cuenta_id=$request->json("cuenta_id");
        $token=$request->header('Authorization',null);
        if(!is_null($cuenta_id)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->eliminar($payload->sub,$payload->roles,$cuenta_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'
            );
            return response()->json($data);
        }
    }

    public function activar(Request $request)
    {

        $cuenta_id=$request->json("cuenta_id");
        $token=$request->header('Authorization',null);
        if(!is_null($cuenta_id)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->activar($payload->sub,$payload->roles,$cuenta_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'
            );
            return response()->json($data);
        }
    }

    public function listar(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new CuentasBl();
            return $bl->listar($payload->sub,$payload->roles);
        }
    }

    public function depositar(Request $request)
    {

        $rules = [
            'numero_cuenta' => 'required|numeric',
            'monto' => 'required|numeric',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }

        $numero_cuenta=$request->json("numero_cuenta");
        $monto=$request->json("monto");
        $token=$request->header('Authorization',null);
        if(!is_null($numero_cuenta) && !is_null($monto)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->depositar($payload->sub,$payload->roles,$numero_cuenta, $monto);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'
            );
            return response()->json($data);
        }



    }

    public function retirar(Request $request)
    {

        $rules = [
            'numero_cuenta' => 'required|numeric',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }

        $numero_cuenta=$request->json("numero_cuenta");
        $token=$request->header('Authorization',null);
        if(!is_null($numero_cuenta)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->retirar($payload->sub,$payload->roles,$numero_cuenta);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }
    }


    public function misMovimientosMios(Request $request)
    {

        /*$rules = [
            'numero_cuenta' => 'required|numeric',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $numero_cuenta=$request->json("numero_cuenta");
        $token=$request->header('Authorization',null);
        if(!is_null($numero_cuenta)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->misMovimientosMios($payload->sub,$payload->roles,$numero_cuenta);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function misMovimientosParaMi(Request $request)
    {

        /*$rules = [
            'numero_cuenta' => 'required|numeric',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $numero_cuenta=$request->json("numero_cuenta");
        $token=$request->header('Authorization',null);
        if(!is_null($numero_cuenta)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CuentasBl();
                return $bl->misMovimientosParaMi($payload->sub,$payload->roles,$numero_cuenta);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function cuentaUniversidad(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $bl=new CuentasBl();
            return $bl->cuentaUniversidad($payload->sub,$payload->roles);
        }
    }

    public function obtenerTotalUniversidad(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $bl=new CuentasBl();
            return $bl->obtenerTotalUniversidad($payload->sub,$payload->roles);
        }
    }



}
