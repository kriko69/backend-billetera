<?php

namespace App\Http\Controllers;

use App\Helpers\Cifrado_Http;
use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\NegocioBl;
use App\Http\Controllers\Bl\UsuarioBl;
use App\Models\Cuenta;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

use phpseclib\Crypt\RSA;

class UsuarioController extends Controller
{
    public function registrar(Request $request)
    {

        /*$rules = [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellidos' => 'required|regex:/^[\pL\s\-]+$/u', //solo letras
            'carnet' => 'required|numeric|digits_between:1,10',
            'correo' => 'required|regex:/(.+)@(.+)\.(.+)/i', //para correos
            'fecha_nacimiento' => 'required|date_format:Y-m-d',
            'carrera' => 'required|regex:/^[\pL\s\-]+$/u', //solo letras
            'telefono' => 'required|numeric|digits_between:1,10',
            'password' => 'required',
            //'imagen' => 'required',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
            'date_format' => 'El atributo :attribute debe tomar el formato Y-m-d.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $carnet=$request->json("carnet");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $carrera=$request->json("carrera");
        $telefono=$request->json("telefono");
        //$imagen=$request->json("imagen");



        //$nombreimagen = $imagen->getClientOriginalName();

        //return $nombre.'-'.$apellidos.'-'.$password;

        $bl = new UsuarioBl();
        $data=$bl->registro($nombre, $apellidos, $carnet,$correo, $nacimiento,$carrera, $telefono); //falta imagen

        return response()->json($data,200);

        /*$usuario=Usuario::find(2);
        return $usuario->persona->nombre;*/

    }

    public function registrarVendedor(Request $request)
    {

        /*$rules = [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellidos' => 'required|regex:/^[\pL\s\-]+$/u', //solo letras
            'carnet' => 'required|numeric|digits_between:1,10',
            'correo' => 'required|regex:/(.+)@(.+)\.(.+)/i', //para correos
            'fecha_nacimiento' => 'required|date_format:Y-m-d',
            'telefono' => 'required|numeric|digits_between:1,10',
            'password' => 'required',
            //'imagen' => 'required',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
            'date_format' => 'El atributo :attribute debe tomar el formato Y-m-d.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $carnet=$request->json("carnet");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        $password=$request->json("password");
        //$imagen=$request->json("imagen");



        //$nombreimagen = $imagen->getClientOriginalName();

        //return $nombre.'-'.$apellidos.'-'.$password;

        $bl = new UsuarioBl();
        $data=$bl->registroVendedor($nombre, $apellidos, $carnet,$correo, $nacimiento, $telefono,$password); //falta imagen

        return response()->json($data,200);

        /*$usuario=Usuario::find(2);
        return $usuario->persona->nombre;*/

    }

    public function registrarAdministrador(Request $request)
    {

        /*$rules = [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellidos' => 'required|regex:/^[\pL\s\-]+$/u', //solo letras
            'carnet' => 'required|numeric|digits_between:1,10',
            'correo' => 'required|regex:/(.+)@(.+)\.(.+)/i', //para correos
            'fecha_nacimiento' => 'required|date_format:Y-m-d',
            'telefono' => 'required|numeric|digits_between:1,10',
            'password' => 'required',
            //'imagen' => 'required',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
            'date_format' => 'El atributo :attribute debe tomar el formato Y-m-d.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/

        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $carnet=$request->json("carnet");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        //$imagen=$request->json("imagen");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            $data=$bl->registroAdministrador($payload->roles,$nombre, $apellidos, $carnet,$correo, $nacimiento, $telefono); //falta imagen

            return response()->json($data,200);
        }


    }

    //chris
    public function login(Request $request)
    {
        //CON CIFRADO
        //$jjson=json_decode($request->getContent(),true);
        //$cifrado=new Cifrado_Http();
        //$info=$cifrado->desencriptarBase64($jjson); //ya es json
        //$info=json_decode($info,true);
        /*$data=array(
            'data'=>$info,
            "estado"=>'exito'
        );
        return response()->json($data);*/
        //CON CIFRADO
        //$carnet=(int)$info["carnet"];
        //$password=$info["password"];
        $carnet=$request->json("carnet");
        $password=$request->json("password");
        $jwtAuth = new JwtAuth();



        if (!is_null($carnet) && !is_null($password) && isset($carnet) && isset($password))
        {
            $password=$password.'billetera';
            $password=hash('sha256',$password);
            $signUp=$jwtAuth->signUp($carnet,$password);
            return response()->json($signUp,200);
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null',
                'estado'=>'error'
            );

            return response()->json($data);
        }

    }

    public function decodificarToken(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            return response()->json($payload);
        }
    }

    public function pruebaValidacion(Request $request)
    {

        $comentarios=DB::table('comentarios')
            ->join('usuarios','comentarios.usuario_id','=','usuarios.usuario_id')
            ->join('calificaciones','calificaciones.usuario_id','=','usuarios.usuario_id')
            ->select('comentarios.comentario_id','usuarios.nombre','usuarios.usuario_id','usuarios.apellidos','comentarios.comentario','comentarios.created_at as fecha','usuarios.imagen','calificaciones.calificacion')
            ->where('comentarios.estado','=',false)
            ->where('comentarios.actividad_id','=',1)
            ->where('calificaciones.actividad_id','=',1)
            ->orderBy('comentarios.created_at','desc')
            ->get();
        return $comentarios;



        $rules = [
            'nombre' => 'required|min:5',
            'apellidos' => 'required',

        ];
        $customMessages = [
            'required' => 'El atributo :attribute  es requerido.',
            'min'=> 'El atributo :attribute debe tener al menos 5 caracteres'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {

            $data=array(
                'errores'=>$validatorUsuario->errors(),
                "estado"=>'error'
            );
            return response()->json($data);
            //return response()->json($validatorUsuario->errors(),400);
        }else{
            return $request->json('nombre').' '.$request->json('apellidos');
        }
    }
    public function perfil(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->mostrarUsuario($payload->sub);
        }

    }

    public function actualizarPerfil(Request $request)
    {
        /*$rules = [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'apellidos' => 'required|regex:/^[\pL\s\-]+$/u', //solo letras
            'correo' => 'required|regex:/(.+)@(.+)\.(.+)/i', //para correos
            'fecha_nacimiento' => 'required|date_format:Y-m-d',
            'telefono' => 'required|numeric|digits_between:1,10',
            'carrera' => 'required|regex:/^[\pL\s\-]+$/u', //solo letras
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
            'date_format' => 'El atributo :attribute debe tomar el formato Y-m-d.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        //$carrera=$request->json("carrera");
        //$imagen = $request->json("imagen");
        $token=$request->header('Authorization',null);
        if(!is_null($nombre) && !is_null($apellidos) && !is_null($correo) && !is_null($nacimiento)
            && !is_null($telefono)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->actualizarPerfil($payload->sub,$nombre, $apellidos,$correo, $nacimiento, $telefono);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro es nulo',
                'estado'=>'error'
            );
            return response()->json($data);
        }


    }


    public function actualizarPassword(Request $request)
    {
        $rules = [
            'actual_password' => 'required',
            'nueva_password' => 'required', //solo letras
        ];
        $customMessages = [
            'required' => ':attribute es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $pass1=$request->json("actual_password");
        $pass2=$request->json("nueva_password");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->actualizarPassword($payload->sub,$pass1,$pass2);
        }

    }

    public function listarUsuarios(Request $request)
    {
        /*$rules = [
            'tipo' => 'required|regex:/^[\pL\s\-]+$/u'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{
            $bl = new UsuarioBl();
            return $bl->listarUsuarios($payload->sub,$payload->roles);
        }

    }

    public function listarUsuariosAdministradores(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{
            $bl = new UsuarioBl();
            return $bl->listarUsuariosAdministradores($payload->sub,$payload->roles);
        }

    }

    public function eliminarUsuario(Request $request)
    {

        $token=$request->header('Authorization',null);
        $usuario_id=$request->json("usuario_id");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->eliminarUsuario($payload->roles,$usuario_id);
        }

    }
    public function eliminarUsuarioAdministrador(Request $request)
    {

        $token=$request->header('Authorization',null);
        $usuario_id=$request->json("usuario_id");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->eliminarUsuarioAdministrador($payload->roles,$usuario_id);
        }

    }



    public function obtenerInformacionUsuarioDeposito(Request $request)
    {
        $rules = [
            'carnet' => 'required|numeric|digits_between:1,10'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $carnet=$request->json("carnet");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->obtenerInformacionUsuarioDeposito($payload->sub,$payload->roles,$carnet);
        }

    }

    public function obtenerInformacionUsuarioRetiro(Request $request)
    {
        $rules = [
            'carnet' => 'required|numeric|digits_between:1,10'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $carnet=$request->json("carnet");
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->obtenerInformacionUsuarioRetiro($payload->sub,$payload->roles,$carnet);
        }

    }

    public function traspasarCuenta(Request $request)
    {
        $rules = [
            'carnet' => 'required|numeric|digits_between:1,10',
            "nueva_carrera"=>'required|regex:/^[\pL\s\-]+$/u'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $carnet=$request->json("carnet");
        $nueva_carrera=$request->json("nueva_carrera");
        if (!is_null($carnet) && !is_null($nueva_carrera))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Token incorrecto',
                    "estado"=>'error'
                );
                return response()->json($data);

            }else{

                $bl = new UsuarioBl();
                return $bl->traspasarCuenta($payload->sub,$payload->roles,$carnet,$nueva_carrera);
            }
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro es nulo.',
                "estado"=>'error'
            );
            return response()->json($data);
        }


    }

    public function resetPassword(Request $request)
    {
        /*$rules = [
            'carnet' => 'required|numeric|digits_between:1,10',

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $carnet=$request->json("carnet");
        if (!is_null($carnet))
        {
            $bl = new UsuarioBl();
            return $bl->resetPassword($carnet);
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro es nulo.',
                "estado"=>'error'
            );
            return response()->json($data);
        }


    }

    public function verificarResetPassword(Request $request)
    {
        /*$rules = [
            'carnet' => 'required|numeric|digits_between:1,10',
            "codigo_reset_password"=>'required'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $carnet=$request->json("carnet");
        $codigo_reset_password=$request->json("codigo_reset_password");
        if (!is_null($carnet) && !is_null($codigo_reset_password))
        {
            $bl = new UsuarioBl();
            return $bl->verificarResetPassword($carnet,$codigo_reset_password);
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro es nulo.',
                "estado"=>'error'
            );
            return response()->json($data);
        }


    }

    public function cambiarPassword(Request $request)
    {
        /*$rules = [
            'password' => 'required',
            "repetir_password"=>'required',
            'carnet' => 'required|numeric'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $password=$request->json("password");
        $repetir_password=$request->json("repetir_password");
        $carnet=$request->json("carnet");
        if (!is_null($password) && !is_null($repetir_password) && !is_null($carnet))
        {
            $bl = new UsuarioBl();
            /*$rsa = new RSA();
            $key= $rsa->createKey(1024);
            return response()->json(array('public_key' => $key['publickey'], 'private_key' => $key['privatekey']));*/
            return $bl->cambiarPassword($password,$repetir_password,$carnet);
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro es nulo.',
                "estado"=>'error'
            );
            return response()->json($data);
        }


    }

    public function cambiarPasswordDefault(Request $request)
    {

        $password=$request->json("password");
        $token=$request->header('Authorization',null);
        if (!is_null($password))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Token incorrecto',
                    "estado"=>'error'
                );
                return response()->json($data);

            }else{

                $bl = new UsuarioBl();
                return $bl->cambiarPasswordDefault($payload->sub,$password);
            }
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro es nulo.',
                "estado"=>'error'
            );
            return response()->json($data);
        }


    }

    public function obtenerSaldo(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->obtenerSaldo($payload->sub);
        }

    }

    public function obtenerNombresNegociosPago(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->obtenerNombresNegociosPago($payload->sub,$payload->roles);
        }

    }


    public function pagar(Request $request)
    {
        /*$rules = [
            'negocio_id' => 'required',
            'proveedor_id' => 'required',
            'carnet_dueno' => 'required',
            'nombre_negocio' => 'required',
            'numero_cuenta' => 'required',
            'monto' => 'required',

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $negocio_id=$request->json("negocio_id");
        $dueno_id=$request->json("proveedor_id");
        $carnet_dueno=$request->json("carnet_dueno");
        $nombre_negocio=$request->json("nombre_negocio");
        $numero_cuenta=$request->json("numero_cuenta");
        $monto=$request->json("monto");
        //return $negocio_id." ".$dueno_id." ".$carnet_dueno." ".$nombre_negocio." ".$numero_cuenta." ".$monto;
        $token=$request->header('Authorization',null);
        if (!is_null($negocio_id) && !is_null($dueno_id) && !is_null($carnet_dueno)
            && !is_null($nombre_negocio) && !is_null($numero_cuenta) && !is_null($monto)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->pagar($payload->sub,$payload->roles,$negocio_id,$dueno_id,$carnet_dueno,$nombre_negocio,$numero_cuenta,$monto);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'
            );
            return response()->json($data);
        }


    }

    public function obtenerDatosTransferir(Request $request)
    {
        $rules = [
            'numero_cuenta' => 'required|numeric|digits_between:1,10'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $numero_cuenta=$request->json("numero_cuenta");
        if (!is_null($numero_cuenta))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Token incorrecto',
                    "estado"=>'error'
                );
                return response()->json($data);

            }else{

                $bl = new UsuarioBl();
                return $bl->obtenerDatosTransferir($payload->sub,$payload->roles,$numero_cuenta);
            }
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro con null.',
                "estado"=>'error'
            );
            return response()->json($data);
        }

    }

    public function transferir(Request $request)
    {
        /*$rules = [
            'numero_cuenta' => 'required|numeric',
            'monto' => 'required|numeric'

        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $token=$request->header('Authorization',null);
        $numero_cuenta=$request->json("numero_cuenta");
        $monto=$request->json("monto");
        if (!is_null($numero_cuenta))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Token incorrecto',
                    "estado"=>'error'
                );
                return response()->json($data);

            }else{

                $bl = new UsuarioBl();
                return $bl->transferir($payload->sub,$payload->roles,$numero_cuenta,$monto);
            }
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Algun parametro con null.',
                "estado"=>'error'
            );
            return response()->json($data);
        }

    }

    public function generarCodigoTransferencia(Request $request)
    {

        $token=$request->header('Authorization',null);

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->generarCodigoTransferencia($payload->sub,$payload->roles);
        }
    }

    public function obtenerNumeroCuenta(Request $request)
    {

        $token=$request->header('Authorization',null);

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->obtenerNumeroCuenta($payload->sub);
        }
    }

    public function activarUsuario(Request $request)
    {

        $token=$request->header('Authorization',null);
        $usuario_id = $request->json('usuario_id');
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->activarUsuario($payload->roles,$usuario_id);
        }
    }
    public function activarUsuarioAdministrador(Request $request)
    {

        $token=$request->header('Authorization',null);
        $usuario_id = $request->json('usuario_id');
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->activarUsuarioAdministrador($payload->roles,$usuario_id);
        }
    }


    public function retirarDineroEstudianteFinCarrera(Request $request)
    {

        $token=$request->header('Authorization',null);
        $usuario_id = $request->json('usuario_id');
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->retirarDineroEstudianteFinCarrera($payload->roles,$usuario_id);
        }
    }

    public function actualizarFotoPerfil(Request $request)
    {

        $token=$request->header('Authorization',null);
        $imagen = $request->json('imagen');
        $rol = $request->json('rol');
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->actualizarFotoPerfil($payload->sub,$imagen,$rol);
        }
    }

    public function eliminarFotoPerfil(Request $request)
    {

        $token=$request->header('Authorization',null);
        $imagen = $request->json('imagen');
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                "estado"=>'error'
            );
            return response()->json($data);

        }else{

            $bl = new UsuarioBl();
            return $bl->eliminarFotoPerfil($payload->sub);
        }
    }

    public function editarUsuarioAdministrador(Request $request)
    {

        $usuario_id=$request->json('usuario_id');
        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        //$imagen = $request->json("imagen");
        $token=$request->header('Authorization',null);
        if(!is_null($nombre) && !is_null($apellidos) && !is_null($correo) && !is_null($nacimiento)
            && !is_null($telefono)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->editarUsuarioAdministrador($payload->roles,$usuario_id,$nombre, $apellidos,$correo, $nacimiento, $telefono);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro es nulo',
                'estado'=>'error'
            );
            return response()->json($data);
        }


    }

    public function guardarOneSignalId(Request $request)
    {
        $onesignal_id=$request->json('onesignal_id');
        $token=$request->header('Authorization',null);
        if(!is_null($onesignal_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->guardarOneSignalId($payload->sub,$onesignal_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro es nulo',
                'estado'=>'error'
            );
            return response()->json($data);
        }
    }

    public function resetOneSignalId(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $bl=new UsuarioBl();
            return $bl->resetOneSignalId($payload->sub);
        }
    }

    public function obtenerOneSignalId(Request $request)
    {
        $usuario2_id=$request->json('usuario_id');
        $token=$request->header('Authorization',null);
        if(!is_null($usuario2_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new UsuarioBl();
                return $bl->obtenerOneSignalId($payload->sub,$payload->roles,$usuario2_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro es nulo',
                'estado'=>'error'
            );
            return response()->json($data);
        }
    }


    public function rsa()
    {
        $rsa = new RSA();
        $rsa->loadKey('-----BEGIN PUBLIC KEY-----
                    MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvV7GuBdjeYc07oeXLMXn
                    BBbNaphS9q0p6Dm0hCdnSNklChn03kNr8287G2LUama4CHjGj2tJVcochEI6XmPp
                    UFRUdL/PO6HxWC+aOQrdELA0fL+DcyZaM1d8D/uvX5Wo091bsvbGHK4DcegLSx0q
                    KIRAN88p1H1iciGVH4rjwQlOpn1Lr7/gyBmSIwO+l+c54nIGFgw58SylnkHnsKRl
                    qr1SbJxtKsUXrspUpj15F3GRbgutyyYLGczoYlEvbQ+P3OFcDhU+FQ7BTEo5hs+k
                    Ibw4t2YvrSk0qMgga+4dAwf/E033Bk7QgnlpfHPq2QM5St0ZkYLzKaWPFt6qsOB8
                    IQIDAQAB
                            -----END PUBLIC KEY-----');
        $rsa->setPublicKey();

        $publickey = $rsa->getPublicKey();
        $plaintext = 'Hello World!';

        $rsa->setEncryptionMode(RSA::ENCRYPTION_OAEP);
        $ciphertext = $rsa->encrypt($plaintext);

        $rsa->loadKey('-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAvV7GuBdjeYc07oeXLMXnBBbNaphS9q0p6Dm0hCdnSNklChn0
3kNr8287G2LUama4CHjGj2tJVcochEI6XmPpUFRUdL/PO6HxWC+aOQrdELA0fL+D
cyZaM1d8D/uvX5Wo091bsvbGHK4DcegLSx0qKIRAN88p1H1iciGVH4rjwQlOpn1L
r7/gyBmSIwO+l+c54nIGFgw58SylnkHnsKRlqr1SbJxtKsUXrspUpj15F3GRbgut
yyYLGczoYlEvbQ+P3OFcDhU+FQ7BTEo5hs+kIbw4t2YvrSk0qMgga+4dAwf/E033
Bk7QgnlpfHPq2QM5St0ZkYLzKaWPFt6qsOB8IQIDAQABAoIBAAX3JNpqvjL3W+cB
tXTvHQpnb3wipUZpCQ9sNfANz7afRIfvbmPMCuwFvn5r3BYn+zclGtgswIgn0EMo
VO9hRVnG9kwhOLV/sfsdsMWIPpOGgXjGja0EccX5RQq52nnv3HwkFn3qQS5pGumt
LiHlHs1c8Srw5zfu5TmELk7qkKclhn3TZ0nYjo/UPNn/oujKyt6NJqPirg4oTfvn
7bpaMfwN0jMPfITV1kaF9hRUrFok1Fs1WAMeoiU7mVdqRdOu4vc8mXoxePQqWZsU
5OBMKzWjoKmPA9YQ8o0ya0/r+TyFMYt8BTSBmg4mMRJLPgc2XV9ENJvc1whwUVPX
M+pWdosCgYEA7y52TZdCZEnClhrsTtNMdHQKFxnw9yjtvisYFAhqfHpSKbfKkKC/
vpeDBO4USduw8KNyG60maj4JkjMeMgRfqipsFQSzU76noj7x4ZVIe5+OFkqH1nsY
dEhI7YNtqoHEH8N/lW8a37UEzFlaxF4m+CB2J2J93JDOFbYHeB8lABMCgYEAyq+n
nyRZ+LHTNgjmKFovzOQEtYF96TYwoF8GKO9ZrOTNVhr5ONFmvK5YKa1HisTQINFV
R+g54D1STPHN9glNy0UE5/QfXuGeRKIFs74P3lFYEkg0S0wh6TekZeRvsAKbWM+E
4xs50rOCOgAHcLM7ajGVpdpFhQwefmF09P9tIXsCgYAKVeem0rJ7wvQ1qFQt09Op
9Jm7BdPz2kDrxtDzjKmNV3vVcau/NDMU+VcgD2mxB11OCMIVkBXo4WVoe+0CHE5F
/C9PeTSGmeaEjFPHe7v2G574N6BHFGqqG7WLDfLfO1xOQxRwUKzInfnnGrVhZvAo
KL7yVL87mMZPCd6owi/HIQKBgQCxhuzwTOypEUgH2f+NdIDNmyIswEDCVX592P0S
OaJ39ueK9ZE4nOC5fpMl3en9t6j8NZ3Wn6ahq9Q8JMbbKwcCdW94orAfcMopHyiT
aHOJrAHR8YAsO4CwPtY1kMHqjj32qbyhFqWs0SHNH11k7vepacjDPc0KXbUPyNii
Bl9VpQKBgQCtPBYUMnhzRd9VD2gkfUyEF6bmOf2hcSIHyc75YhUJw0mBGEQPfUf4
EvP8TVSsmJZsDObErVdHrMKmfz2S6FZtjCQL345L77Kj3/96xFq/zm/KwzRGyaNM
JmmGCjAk0+zici4lvkVYLNJzXFyVrqxUgX0GW5Gr5JljbNVU8HJnZw==
-----END RSA PRIVATE KEY-----');

        //return base64_encode($ciphertext);
        //$ciphertext='hiYI5hiGEUZ+VvDW9sVKX3uYtLo6ameYfCr3FQEaMrRW2IrM/iZduyaowHDyOQqYezkIn1RLtctESCNdBi7OZkS9mVyehodEtEmfB6CzmXFANF1jzdUoiL1D5VWxNCSXWzKJqllf2VtU7Qrmo0si/zf9S8UvOwBGjgAo+c+JKHYvAEK+f44P9DI0ECFlrViVEDjblisr0PNge5VWt39m+3H9Q0SsUr2WvrQXRZkNPvM2kurwFESaXJH7FsVL82HpuX7VMBel/fQ+8gd4ioDxkBIaw3uoWKVAfWUXk5g1l+8xnfQTPpN/N7oPS8uKzWW6D8FxGfeWJmg0xEIuRe1qpQ==';
        //$ciphertext=base64_decode($ciphertext);
        //return $ciphertext;
        return $rsa->decrypt($ciphertext);
    }


    public function crearSuperAdministradorGenesis(Request $request)
    {


        $nombre=$request->json("nombre");
        $apellidos=$request->json("apellidos");
        $carnet=$request->json("carnet");
        $correo=$request->json("correo");
        $nacimiento=$request->json("fecha_nacimiento");
        $telefono=$request->json("telefono");
        $password=$request->json("password");
        //$imagen=$request->json("imagen");



        $bl = new UsuarioBl();
        $data=$bl->crearSuperAdministradorGenesis($nombre, $apellidos, $carnet,$correo, $nacimiento, $telefono,$password); //falta imagen

        return response()->json($data,200);

        /*$usuario=Usuario::find(2);
        return $usuario->persona->nombre;*/

    }

    public function encriptarBase64(Request $request)
    {
        //obtengo jsoon
        $data=$request->all();

        //convertir a json
        $data=json_encode($data); //ya es json

        //codifico base64
        $base64Data=base64_encode($data);

        //contamos iguales
        $iguales=substr_count($base64Data,"=");

        //quitamos iguales
        if($iguales!=0)
        {
            //quitamos iguales
            $base64Data=substr($base64Data,0,strlen($base64Data)-$iguales);
        }

        //cada 3 caracteres reemplazaremos un caracter

        $longitudCadena=strlen($base64Data);

        $cociente=$longitudCadena/3;

        $cociente=floor($cociente); //redondeamos siempre hacia abajo
        $resto=$longitudCadena%3;
        if($resto==0)
        {
            //sumo cociente y resto
            $cantidadCaracteresGenerar=$cociente+$resto;
            $cadenaAleatoria=$this->generarCadenaAleatoria($cantidadCaracteresGenerar);
        }else{
            if($resto==1)
            {
                //sumo cociente y resto
                $cantidadCaracteresGenerar=$cociente+$resto;
                $cadenaAleatoria=$this->generarCadenaAleatoria($cantidadCaracteresGenerar);
            }else{
                //resto es igual a 2
                //sumo uno al cociente
                $cantidadCaracteresGenerar=$cociente+1;
                $cadenaAleatoria=$this->generarCadenaAleatoria($cantidadCaracteresGenerar);
            }
        }

        if($resto==0)
        {
            return $this->encriptarModulo0($data,$iguales,$base64Data,$cadenaAleatoria);
        }else{
            if($resto==1)
            {
                return $this->encriptarModulo1($data,$iguales,$base64Data,$cadenaAleatoria);
            }else{
                if($resto==2)
                {
                    return $this->encriptarModulo2($data,$iguales,$base64Data,$cadenaAleatoria);
                }else{
                    $respuesta=array(
                        'mensaje'=>'Modulo no esta en el rango permitido',
                        'estado'=>'error'
                    );
                    return $respuesta;
                }

            }
        }



    }

    public function generarCadenaAleatoria($length) {

        $letras="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $letras_length = strlen($letras);
        $random_string = '';
        for($i = 0; $i < $length; $i++) {
            $random_character = $letras[mt_rand(0, $letras_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }

    public function encriptarModulo0($data,$iguales,$base64Data,$cadenaAleatoria)
    {
        $caracteresReemplazados="";
        $nuevaBaseJsonData="";
        $longitud=strlen($base64Data); //longitud de la cadena
        for ( $i = 0; $i <$longitud ; $i++) {

            $indice=$i+1; //para que no de error 0/3
            if($indice%3==0) //es multiplo de 3
            {
                $caracteresReemplazados=$caracteresReemplazados.$base64Data[$i]; //concatenamos lo caracteres reemplados
            }else{
                $nuevaBaseJsonData=$nuevaBaseJsonData.$base64Data[$i]; //concatenamos los caracteres no multiplos de 3
            }

        }

        $cadenaCambiada="";
        $aux1=0;
        $longitud2=strlen($cadenaAleatoria); // longitud de los caracteres aleatorios
        //creamos la cadena cambiada
        for ($i=0;$i<$longitud2;$i++)
        {
            $slice=substr($nuevaBaseJsonData,$aux1,2); //obtengo substring de 0-2, 2-4
            $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
            $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
            $aux1=$aux1+2; //aumento el indice para crear una nueva subcadena
        }

        $inverso="";
        $primeraMitad="";
        $segundaMitad="";
        $cociente=strlen($cadenaCambiada)/2;
        $cociente=floor($cociente); //redondeamos siempre hacia abajo

        if(strlen($cadenaCambiada)%2==0)
        {
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,$cociente);
            $inverso=$segundaMitad.$primeraMitad;
        }else{
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,($cociente+1));
            $inverso=$segundaMitad.$primeraMitad;
        }

        $reverso=strrev($inverso);

        $dataVer = array(
            'data original'=>$data,
            'iguales'=>$iguales,
            '$base64Data'=>$base64Data,
            '$cadenaAleatoria' => $cadenaAleatoria,
            '$caracteresReemplazados'=>$caracteresReemplazados,
            '$nuevaBaseJsonData sin caracteres'=>$nuevaBaseJsonData,
            '$cadenaCambiada con caracteres'=>$cadenaCambiada,
            '$cadenaCambiada inversa'=>$inverso,
            'primera mitad'=>$primeraMitad,
            'segunda mitad'=>$segundaMitad,
            'longitud primera mitad'=>strlen($primeraMitad),
            'longitud segunda mitad'=>strlen($segundaMitad),
            '$cadenaCambiada invertida'=>$reverso,
            'longitud base64Data'=>$longitud,
            'longitud cadenaCambiada'=>strlen($cadenaCambiada),
            'longitud caracteres generados'=>$longitud2,
            'modulo'=>0
        );
        return $dataVer;

    }

    public function encriptarModulo1($data,$iguales,$base64Data,$cadenaAleatoria)
    {
        $caracteresReemplazados="";
        $nuevaBaseJsonData="";
        $longitud=strlen($base64Data); //longitud de la cadena
        for ( $i = 0; $i <$longitud ; $i++) {

            $indice=$i+1; //para que no de error 0/3
            if($indice%3==0) //es multiplo de 3
            {
                $caracteresReemplazados=$caracteresReemplazados.$base64Data[$i]; //concatenamos lo caracteres reemplados
            }else{
                $nuevaBaseJsonData=$nuevaBaseJsonData.$base64Data[$i]; //concatenamos los caracteres no multiplos de 3
            }

        }

        $cadenaCambiada="";
        $aux1=0;
        $parar=0;
        $longitud2=strlen($cadenaAleatoria); // longitud de los caracteres aleatorios
        //creamos la cadena cambiada
        for ($i=0;$i<$longitud2;$i++)
        {
            if($parar==($longitud2-1))
            {
                $slice=substr($nuevaBaseJsonData,$aux1,1); //obtengo la ultima letra porque aux esta penultimo
                $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
                $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
                break;
            }else{
                $slice=substr($nuevaBaseJsonData,$aux1,2); //obtengo substring de 0-2, 2-4
                $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
                $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
                $aux1=$aux1+2; //aumento el indice para crear una nueva subcadena
                $parar=$parar+1;
            }
        }

        $inverso="";
        $primeraMitad="";
        $segundaMitad="";
        $cociente=strlen($cadenaCambiada)/2;
        $cociente=floor($cociente); //redondeamos siempre hacia abajo
        if(strlen($cadenaCambiada)%2==0)
        {
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,$cociente);
            $inverso=$segundaMitad.$primeraMitad;
        }else{
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,($cociente+1));
            $inverso=$segundaMitad.$primeraMitad;
        }

        $reverso=strrev($inverso);

        $dataVer = array(
            'data original'=>$data,
            'iguales'=>$iguales,
            '$base64Data'=>$base64Data,
            '$cadenaAleatoria' => $cadenaAleatoria,
            '$caracteresReemplazados'=>$caracteresReemplazados,
            '$nuevaBaseJsonData sin caracteres'=>$nuevaBaseJsonData,
            '$cadenaCambiada con caracteres'=>$cadenaCambiada,
            '$cadenaCambiada inversa'=>$inverso,
            'primera mitad'=>$primeraMitad,
            'segunda mitad'=>$segundaMitad,
            'longitud primera mitad'=>strlen($primeraMitad),
            'longitud segunda mitad'=>strlen($segundaMitad),
            '$cadenaCambiada invertida'=>$reverso,
            'longitud base64Data'=>$longitud,
            'longitud cadenaCambiada'=>strlen($cadenaCambiada),
            'longitud caracteres generados'=>$longitud2,
            'modulo'=>1
        );
        return $dataVer;

    }

    public function encriptarModulo2($data,$iguales,$base64Data,$cadenaAleatoria)
    {

        $caracteresReemplazados="";
        $nuevaBaseJsonData="";

        $longitud=strlen($base64Data); //longitud de la cadena
        for ( $i = 0; $i <$longitud ; $i++) {

            $indice=$i+1; //para que no de error 0/3
            if($indice%3==0) //es multiplo de 3
            {
                $caracteresReemplazados=$caracteresReemplazados.$base64Data[$i]; //concatenamos lo caracteres reemplados
            }else{
                $nuevaBaseJsonData=$nuevaBaseJsonData.$base64Data[$i]; //concatenamos los caracteres no multiplos de 3
            }

        }

        $cadenaCambiada="";
        $aux1=0;
        $parar=0;
        $longitud2=strlen($cadenaAleatoria); // longitud de los caracteres aleatorios
        //creamos la cadena cambiada
        for ($i=0;$i<$longitud2;$i++)
        {
            $slice=substr($nuevaBaseJsonData,$aux1,2); //obtengo substring de 0-2, 2-4
            $add = $slice.$cadenaAleatoria[$i]; //le concateno el caracter aleatorio
            $cadenaCambiada=$cadenaCambiada.$add; //creo la cadena
            $aux1=$aux1+2; //aumento el indice para crear una nueva subcadena
        }

        $inverso="";
        $primeraMitad="";
        $segundaMitad="";
        $cociente=strlen($cadenaCambiada)/2;
        $cociente=floor($cociente); //redondeamos siempre hacia abajo

        if(strlen($cadenaCambiada)%2==0)
        {
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,$cociente);
            $inverso=$segundaMitad.$primeraMitad;
        }else{
            $primeraMitad=substr($cadenaCambiada,0,$cociente);
            $segundaMitad=substr($cadenaCambiada,$cociente,($cociente+1));
            $inverso=$segundaMitad.$primeraMitad;
        }

        $reverso=strrev($inverso);

        $dataVer = array(
            'data original'=>$data,
            'iguales'=>$iguales,
            '$base64Data'=>$base64Data,
            '$cadenaAleatoria' => $cadenaAleatoria,
            '$caracteresReemplazados'=>$caracteresReemplazados,
            '$nuevaBaseJsonData sin caracteres'=>$nuevaBaseJsonData,
            '$cadenaCambiada con caracteres'=>$cadenaCambiada,
            '$cadenaCambiada inversa'=>$inverso,
            'primera mitad'=>$primeraMitad,
            'segunda mitad'=>$segundaMitad,
            'longitud primera mitad'=>strlen($primeraMitad),
            'longitud segunda mitad'=>strlen($segundaMitad),
            '$cadenaCambiada invertida'=>$reverso,
            'longitud base64Data'=>$longitud,
            'longitud cadenaCambiada'=>strlen($cadenaCambiada),
            'longitud caracteres generados'=>$longitud2,
            'modulo'=>2
        );
        return $dataVer;

    }


    public function desencriptarBase64(Request $request)
    {
        $reverso = $request->json("reverso");
        $caracteresReemplazados = $request->json("caracteresReemplazados");
        $cadenaAleatoria = $request->json("cadenaAleatoria");
        $modulo = $request->json("modulo");
        $iguales = $request->json("iguales");
        $inverso = strrev($reverso);
        $primeraMitad = "";
        $segundaMitad = "";
        if ($modulo == 0) {
            $cociente = strlen($inverso) / 2;
            $cociente = floor($cociente); //redondeamos siempre hacia abajo
            if (strlen($inverso) % 2 == 0) {
                $primeraMitad = substr($inverso, 0, $cociente);
                $segundaMitad = substr($inverso, $cociente, $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            } else {
                $primeraMitad = substr($inverso, 0, ($cociente + 1));
                $segundaMitad = substr($inverso, ($cociente + 1), $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            }

            $caracteresReemplazadosDC = "";
            $nuevaBaseJsonDataDC = "";

            $longitud = strlen($inverso); //longitud de la cadena
            for ($i = 0; $i < $longitud; $i++) {

                $indice = $i + 1; //para que no de error 0/3
                if ($indice % 3 == 0) //es multiplo de 3
                {
                    $caracteresReemplazadosDC = $caracteresReemplazadosDC . $inverso[$i]; //concatenamos lo caracteres reemplados
                } else {
                    $nuevaBaseJsonDataDC = $nuevaBaseJsonDataDC . $inverso[$i]; //concatenamos los caracteres no multiplos de 3
                }

            }
            if ($caracteresReemplazadosDC == $cadenaAleatoria) //comprobacion
            {

                $cadenaCambiada = "";
                $aux1 = 0;
                $longitud2 = strlen($caracteresReemplazados); // longitud de los caracteres aleatorios
                //creamos la cadena cambiada
                for ($i = 0; $i < $longitud2; $i++) {
                    $slice = substr($nuevaBaseJsonDataDC, $aux1, 2); //obtengo substring de 0-2, 2-4
                    $add = $slice . $caracteresReemplazados[$i]; //le concateno el caracter aleatorio
                    $cadenaCambiada = $cadenaCambiada . $add; //creo la cadena
                    $aux1 = $aux1 + 2; //aumento el indice para crear una nueva subcadena
                }

                for ($i = 0; $i < $iguales; $i++) {
                    $cadenaCambiada = $cadenaCambiada . '=';
                }

                $var = json_decode(base64_decode($cadenaCambiada), true);
                return response()->json($var);
                //fin modulo 0
            } else {
                $data = array(
                    'mensaje' => 'no se puede decodificar',
                    'estado'=>'error'
                );
                return $data;
            }
        } else {
            //modulo 1 o 2
            $cociente = strlen($inverso) / 2;
            $cociente = floor($cociente); //redondeamos siempre hacia abajo
            if (strlen($inverso) % 2 == 0) {
                $primeraMitad = substr($inverso, 0, $cociente);
                $segundaMitad = substr($inverso, $cociente, $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            } else {
                $primeraMitad = substr($inverso, 0, ($cociente + 1));
                $segundaMitad = substr($inverso, ($cociente + 1), $cociente);
                $inverso = $segundaMitad . $primeraMitad;
            }

            if ($modulo == 1) {
                $caracteresReemplazadosDC = "";
                $nuevaBaseJsonDataDC = "";

                $longitud = strlen($inverso); //longitud de la cadena
                for ($i = 0; $i < $longitud; $i++) {

                    $indice = $i + 1; //para que no de error 0/3
                    if ($indice % 3 == 0) //es multiplo de 3
                    {
                        $caracteresReemplazadosDC = $caracteresReemplazadosDC . $inverso[$i]; //concatenamos lo caracteres reemplados
                    } else {
                        $nuevaBaseJsonDataDC = $nuevaBaseJsonDataDC . $inverso[$i]; //concatenamos los caracteres no multiplos de 3
                    }

                }
                //$caracteresReemplazadosDC tiene los ultimos caracteres menos el ultimo
                //concatenamos el ultimo caracter para la verificacion
                $ultimo_caracter = substr($inverso, (strlen($inverso) - 1), 1);
                $caracteresReemplazadosDC = $caracteresReemplazadosDC . $ultimo_caracter;
                if ($caracteresReemplazadosDC == $cadenaAleatoria) {
                    $nuevaBaseJsonDataDC = substr($nuevaBaseJsonDataDC, 0, (strlen($nuevaBaseJsonDataDC) - 1)); //elimino el ultimo caracter

                    $ultima_letra = substr($nuevaBaseJsonDataDC, (strlen($nuevaBaseJsonDataDC) - 1), 1);

                    $cadenaCambiada = "";
                    $aux1 = 0;
                    $parar = 0;
                    $longitud2 = strlen($caracteresReemplazados); // longitud de los caracteres aleatorios
                    //creamos la cadena cambiada
                    for ($i = 0; $i < $longitud2; $i++) {
                        $slice = substr($nuevaBaseJsonDataDC, $aux1, 2); //obtengo substring de 0-2, 2-4
                        $add = $slice . $caracteresReemplazados[$i]; //le concateno el caracter aleatorio
                        $cadenaCambiada = $cadenaCambiada . $add; //creo la cadena
                        $aux1 = $aux1 + 2; //aumento el indice para crear una nueva subcadena
                    }
                    $cadenaCambiada = $cadenaCambiada . $ultima_letra;

                    for ($i = 0; $i < $iguales; $i++) {
                        $cadenaCambiada = $cadenaCambiada . '=';
                    }

                    $var = json_decode(base64_decode($cadenaCambiada), true);
                    return response()->json($var);
                }else{
                    $data = array(
                        'mensaje' => 'no se puede decodificar',
                        'estado'=>'error'
                    );
                    return $data;
                }


            } else {
                //modulo 2

                $caracteresReemplazadosDC = "";
                $nuevaBaseJsonDataDC = "";

                $longitud = strlen($inverso); //longitud de la cadena
                for ($i = 0; $i < $longitud; $i++) {

                    $indice = $i + 1; //para que no de error 0/3
                    if ($indice % 3 == 0) //es multiplo de 3
                    {
                        $caracteresReemplazadosDC = $caracteresReemplazadosDC . $inverso[$i]; //concatenamos lo caracteres reemplados
                    } else {
                        $nuevaBaseJsonDataDC = $nuevaBaseJsonDataDC . $inverso[$i]; //concatenamos los caracteres no multiplos de 3
                    }

                }
                //$caracteresReemplazadosDC tiene los ultimos caracteres y como su longitud es par
                // ya no concatenamos el ultimo caracter para la verificacion

                if ($caracteresReemplazadosDC == $cadenaAleatoria) {

                    //nuevaCadenaDC esta bien

                    $ultimas_2_letra = substr($nuevaBaseJsonDataDC, (strlen($nuevaBaseJsonDataDC) - 2), 2);

                    $cadenaCambiada = "";
                    $aux1 = 0;
                    $parar = 0;
                    $longitud2 = strlen($caracteresReemplazados); // longitud de los caracteres aleatorios
                    //creamos la cadena cambiada
                    for ($i = 0; $i < $longitud2; $i++) {
                        $slice = substr($nuevaBaseJsonDataDC, $aux1, 2); //obtengo substring de 0-2, 2-4
                        $add = $slice . $caracteresReemplazados[$i]; //le concateno el caracter aleatorio
                        $cadenaCambiada = $cadenaCambiada . $add; //creo la cadena
                        $aux1 = $aux1 + 2; //aumento el indice para crear una nueva subcadena
                    }
                    $cadenaCambiada = $cadenaCambiada . $ultimas_2_letra;

                    for ($i = 0; $i < $iguales; $i++) {
                        $cadenaCambiada = $cadenaCambiada . '=';
                    }

                    $var = json_decode(base64_decode($cadenaCambiada), true);
                    return response()->json($var);
                }else{
                    $data = array(
                        'mensaje' => 'no se puede decodificar',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }

        }
    }


}
