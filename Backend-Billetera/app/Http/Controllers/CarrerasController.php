<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\CarreraBl;
use App\Http\Controllers\Bl\UsuarioBl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CarrerasController extends Controller
{
    public function registrar(Request $request)
    {

        $rules = [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'prefijo_cuenta' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }

        $nombre=$request->json("nombre");
        $prefijo=$request->json("prefijo_cuenta");
        $token=$request->header('Authorization',null);
        if(!is_null($nombre) && !is_null($prefijo)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CarreraBl();
                return $bl->registrar($payload->sub,$payload->roles,$nombre, $prefijo);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }



    }


    public function actualizar(Request $request)
    {
        $rules = [
            'carrera_id'=>'required',
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u',
            'prefijo_cuenta' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $carrera_id=$request->json("carrera_id");
        $nombre=$request->json("nombre");
        $prefijo=$request->json("prefijo_cuenta");
        $token=$request->header('Authorization',null);
        if(!is_null($carrera_id) && !is_null($nombre) && !is_null($prefijo)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CarreraBl();
                return $bl->actualizar($payload->sub,$payload->roles,$carrera_id,$nombre, $prefijo);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }
    }

    public function eliminar(Request $request)
    {
        $rules = [
            'carrera_id' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $carrera_id=$request->json("carrera_id");
        $token=$request->header('Authorization',null);
        if(!is_null($carrera_id)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new CarreraBl();
                return $bl->eliminar($payload->sub,$payload->roles,$carrera_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }
    }

    public function listar(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new CarreraBl();
            return $bl->listar($payload->sub,$payload->roles);
        }
    }
}
