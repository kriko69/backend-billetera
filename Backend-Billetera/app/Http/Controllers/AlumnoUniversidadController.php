<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\AlumnoUniversidadBl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AlumnoUniversidadController extends Controller
{
    public function registrar(Request $request)
    {

        $rules = [
            'carnet' => 'required|numeric',
            'ciudad' => 'required|regex:/^[\pL\s\-]+$/u',
            'carrera' => 'required|regex:/^[\pL\s\-]+$/u',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }

        $carnet=$request->json("carnet");
        $ciudad=$request->json("ciudad");
        $carrera=$request->json("carrera");
        $token=$request->header('Authorization',null);
        if(!is_null($carnet) && !is_null($ciudad) && !is_null($carrera)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new AlumnoUniversidadBl();
                return $bl->registrar($payload->sub,$payload->roles,$carnet, $ciudad,$carrera);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }



    }


    public function actualizar(Request $request)
    {
        $rules = [
            'alumno_universidad_id'=>'required',
            'ciudad' => 'required|regex:/^[\pL\s\-]+$/u',
            'carrera' => 'required|regex:/^[\pL\s\-]+$/u'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $alumno_universidad_id=$request->json("alumno_universidad_id");
        $ciudad=$request->json("ciudad");
        $carrera=$request->json("carrera");
        $token=$request->header('Authorization',null);
        if(!is_null($alumno_universidad_id) && !is_null($ciudad) && !is_null($carrera)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new AlumnoUniversidadBl();
                return $bl->actualizar($payload->sub,$payload->roles,$alumno_universidad_id, $ciudad,$carrera);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }
    }

    /*public function eliminar(Request $request)
    {
        $rules = [
            'alumno_universidad_id' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $alumno_universidad_id=$request->json("alumno_universidad_id");
        $token=$request->header('Authorization',null);
        if(!is_null($carrera_id)){
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new AlumnoUniversidadBl();
                return $bl->eliminar($payload->sub,$payload->roles,$alumno_universidad_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }
    }*/

    public function listar(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new AlumnoUniversidadBl();
            return $bl->listar($payload->sub,$payload->roles);
        }
    }
}
