<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\GestionBl;
use Illuminate\Http\Request;

class GestionController extends Controller
{
    public function listarGestiones(Request $request)
    {

        $token=$request->header('Authorization',null);

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $bl=new GestionBl();
            return $bl->listarGestiones($payload->sub,$payload->roles);
        }


    }

    public function registrarGestion(Request $request)
    {

        $token=$request->header('Authorization',null);
        $semestre=$request->json('semestre');
        $ano=$request->json('ano');
        if(!is_null($semestre) && !is_null($ano))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new GestionBl();
                return $bl->registrarGestion($payload->sub,$payload->roles,$semestre,$ano);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro nulo.',
                'estado'=>'error'
            );
            return response()->json($data);
        }


    }

    public function eliminarGestion(Request $request)
    {

        $token=$request->header('Authorization',null);
        $gestion_id=$request->json('gestion_id');
        if(!is_null($gestion_id))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new GestionBl();
                return $bl->eliminarGestion($payload->sub,$payload->roles,$gestion_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro nulo.',
                'estado'=>'error'
            );
            return response()->json($data);
        }


    }

    public function activarGestion(Request $request)
    {

        $token=$request->header('Authorization',null);
        $gestion_id=$request->json('gestion_id');
        if(!is_null($gestion_id))
        {
            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new GestionBl();
                return $bl->activarGestion($payload->sub,$payload->roles,$gestion_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro nulo.',
                'estado'=>'error'
            );
            return response()->json($data);
        }


    }

    public function listarGestionesEstudiante(Request $request)
    {

        $token=$request->header('Authorization',null);

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $bl=new GestionBl();
            return $bl->listarGestionesEstudiante($payload->sub,$payload->roles);
        }


    }
}
