<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\NegocioBl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NegocioController extends Controller
{
    public function registrarNegocio(Request $request)
    {

        /*$rules = [
            'nombre' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
            'descripcion' => 'required|regex:/^[a-zA-Z0-9\s]+$/', //solo letras alpha_dash no valida espacios
            'dueno_id' => 'required|numeric',
            'hora_inicio'=>'required',
            'hora_fin'=>'required'
            //'imagen' => 'required',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $token=$request->header('Authorization',null);
        $nombre=$request->json("nombre");
        $descripcion=$request->json("descripcion");
        $hora_inicio=$request->json("hora_inicio");
        $hora_fin=$request->json("hora_fin");
        //$imagen=$request->json("imagen");
        if(!is_null($nombre) && !is_null($descripcion) && !is_null($hora_inicio) && !is_null($hora_fin)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->registrarNegocio($payload->sub,$payload->roles,$nombre, $descripcion,$hora_inicio,$hora_fin);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'
            );
            return response()->json($data);
        }

    }

    public function afiliarNegocio(Request $request)
    {

        $rules = [
            'negocio_id' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }
        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if(!is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->afiliarNegocio($payload->sub,$payload->roles,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function actualizarNegocio(Request $request)
    {

        /*$rules = [
            'nombre' => 'required|regex:/^[a-zA-Z0-9\s]+$/',
            'descripcion' => 'required|regex:/^[a-zA-Z0-9\s]+$/', //solo letras alpha_dash no valida espacios
            'hora_inicio'=>'required',
            'hora_fin'=>'required',
            'negocio_id' => 'required|numeric'
            //'imagen' => 'required',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $token=$request->header('Authorization',null);
        $nombre=$request->json("nombre");
        $descripcion=$request->json("descripcion");
        $hora_inicio=$request->json("hora_inicio");
        $hora_fin=$request->json("hora_fin");
        $negocio_id=$request->json("negocio_id");
        //$imagen=$request->json("imagen");
        if(!is_null($nombre) && !is_null($descripcion) && !is_null($hora_inicio) && !is_null($hora_fin)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->actualizarNegocio($payload->sub,$payload->roles,$nombre, $descripcion,$hora_inicio,$hora_fin,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                "estado"=>'error'
            );
            return response()->json($data);
        }

    }

    public function eliminarNegocio(Request $request)
    {

        /*$rules = [
            'negocio_id' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if(!is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->eliminarNegocio($payload->sub,$payload->roles,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function activar(Request $request)
    {

        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if(!is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->activar($payload->sub,$payload->roles,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function aceptarAfiliacion(Request $request)
    {

        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if(!is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->aceptarAfiliacion($payload->sub,$payload->roles,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function rechazarAfiliacion(Request $request)
    {

        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        $usuario_id=$request->json("usuario_id");
        if(!is_null($negocio_id) && !is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto',
                    'estado'=>'error'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->rechazarAfiliacion($payload->sub,$payload->roles,$negocio_id,$usuario_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function verPerfilNegocioParaEstudiante(Request $request)
    {

        /*$rules = [
            'negocio_id' => 'required|numeric'
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.'
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }*/
        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if( !is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->verPerfilNegocioParaEstudiante($payload->sub,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function listarNegociosAfiliadosYNoEliminados(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new NegocioBl();
            return $bl->listarNegociosAfiliadosYNoEliminados($payload->sub);
        }

    }

    public function listarTodosLosNegocios(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $bl=new NegocioBl();
            return $bl->listarTodosLosNegocios($payload->sub,$payload->roles);
        }

    }

    public function obtenerDatosParaQR(Request $request)
    {
        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if( !is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->obtenerDatosParaQR($payload->sub,$payload->roles,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'

            );
            return response()->json($data);
        }

    }

    public function misNegocios(Request $request)
    {
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new NegocioBl();
            return $bl->misNegocios($payload->sub,$payload->roles);
        }

    }

    public function reintentarAfiliacion(Request $request)
    {
        $token=$request->header('Authorization',null);
        $negocio_id=$request->json("negocio_id");
        if( !is_null($negocio_id)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new NegocioBl();
                return $bl->reintentarAfiliacion($payload->sub,$payload->roles,$negocio_id);
            }
        }else{
            $data=array(
                'mensaje'=>'algun parametro en null',
                'estado'=>'error'

            );
            return response()->json($data);
        }

    }

    public function creacionUniversidad(Request $request)
    {

        $token=$request->header('Authorization',null);

        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new NegocioBl();
            return $bl->registrarUniversidad($payload->sub,$payload->roles);
        }


    }



}
