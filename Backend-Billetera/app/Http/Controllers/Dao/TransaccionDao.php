<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 06-04-20
 * Time: 04:00 PM
 */

namespace App\Http\Controllers\Dao;


use Illuminate\Support\Facades\DB;

class TransaccionDao
{
    public function registrar($transaccion)
    {
        DB::beginTransaction();
        try {
            $transaccion->save();
            $data=array(
                'mensaje'=>'Transaccion realizada con exito.',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }
}
