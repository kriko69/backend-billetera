<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 18-03-20
 * Time: 05:22 PM
 */

namespace App\Http\Controllers\Dao;


use App\Mail\aceptarAfiliacion;
use App\Mail\rechazarAfiliacion;
use App\Models\Negocio;
use App\Models\Usuario;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NegocioDao
{

    function verificarExistenciaDuenoPorId($dueno_id)
    {
        $isset_usuario = Usuario::where(
            array(
                'usuario_id' => $dueno_id
            )
        )->first();
        if (!is_object($isset_usuario))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }
    function verificarExistenciaNombreNegocio($nombre_negocio)
    {
        $negocio = Negocio::where(
            array(
                'nombre' => $nombre_negocio
            )
        )->first();
        if (!is_object($negocio))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }
    function obtenerNombreDuenoPorId($dueno_id)
    {
        $dueno=Usuario::find($dueno_id);
        return $dueno->nombre." ".$dueno->apellidos;
    }
    function registrarNegocio($negocio)
    {
        DB::beginTransaction();
        try {

            $negocio->save();

            //Storage::disk('ftp')->putFileAs('/imagenes/Empresa',$imagen,$nombreimagen);
            $data=array(
                'mensaje'=>'Negocio creado con exito',
                'Negocio_id'=>$negocio->negocio_id,
                'estado'=>'exito',
                'Nombre del dueño'=>$this->obtenerNombreDuenoPorId($negocio->dueno_id)
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function obtenerNegocioPorId($negocio_id)
    {
        $negocio = Negocio::find($negocio_id);
        return $negocio;
    }

    public function actualizarNegocio($negocioNuevo)
    {
        DB::beginTransaction();
        try {

            $negocioNuevo->save();
            $data=array(
                'data'=>null,
                'mensaje'=>'Negocio se actualizo con exito',
                'descripcion'=>'exito',
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function verificarExistenciaNegocioPorId($negocio_id)
    {
        $isset_negocio = Negocio::where(
            array(
                'negocio_id' => $negocio_id
            )
        )->first();
        if (!is_object($isset_negocio))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    public function verPerfilNegocioParaEstudiante($negocio_id)
    {
        DB::beginTransaction();
        try {
            $negocio=DB::table('usuarios')
                ->join('negocios','usuarios.usuario_id','=','negocios.dueno_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos','usuarios.carnet',
                    'usuarios.correo','usuarios.telefono','negocios.negocio_id','negocios.nombre as nombre_negocio',
                    'negocios.descripcion','negocios.hora_inicio','negocios.hora_fin')
                ->where('negocios.negocio_id','=',$negocio_id)
                ->get();
            $data=array(
                'data'=>$negocio,
                'mensaje'=>'Exito al encontrar el negocio.',
                'estado'=>'exito'
            );
            return response()->json($data);

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }

    }

    public function listarNegociosAfiliadosYNoEliminados($usuario_id)
    {
        DB::beginTransaction();
        try {
            $negocios=DB::table('usuarios')
                ->join('negocios','usuarios.usuario_id','=','negocios.dueno_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos',
                    'usuarios.correo','usuarios.telefono','negocios.negocio_id','negocios.nombre as nombre_negocio',
                    'negocios.descripcion','negocios.hora_inicio','negocios.hora_fin','negocios.afiliacion')
                ->where('negocios.afiliacion','=',true)
                ->where('negocios.estado','=',false)
                ->get();

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($negocios)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay negocios en la billetera movil.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$negocios,
                'mensaje'=>'Exito al encontrar los negocios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function listarTodosLosNegocios($admin_id)
    {
        DB::beginTransaction();
        try {
            $negocios=DB::table('usuarios')
                ->join('negocios','usuarios.usuario_id','=','negocios.dueno_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos','usuarios.carnet',
                    'usuarios.correo','usuarios.telefono','usuarios.fecha_nacimiento','usuarios.estado as usuario_estado',
                    'negocios.negocio_id', 'negocios.nombre as nombre_negocio','negocios.descripcion','negocios.hora_inicio',
                    'negocios.hora_fin','negocios.afiliacion','negocios.estado as negocio_estado','negocios.estado_afiliacion')
                ->get();

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($negocios)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay negocios en la billetera movil.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$negocios,
                'mensaje'=>'Exito al encontrar los negocios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function obtenerNombresNegociosPago()
    {
        DB::beginTransaction();
        try {
            $negocios=DB::table('cuentas_negocios')
                ->join('cuentas','cuentas_negocios.cuenta_id','=','cuentas.cuenta_id')
                ->join('negocios','cuentas_negocios.negocio_id','=','negocios.negocio_id')
                ->select('negocios.negocio_id','negocios.nombre as nombre_negocio','negocios.descripcion','negocios.hora_inicio','negocios.hora_fin','cuentas.numero_cuenta')
                ->where('negocios.estado','=',false)
                ->where('negocios.afiliacion','=',true)
                ->get();

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($negocios)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay negocios en la billetera movil.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$negocios,
                'mensaje'=>'Exito al encontrar los negocios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }



    public function obtenerDatosParaQR($negocio_id)
    {
        DB::beginTransaction();
        try {
            $data=DB::table('usuarios')
                ->join('negocios','usuarios.usuario_id','=','negocios.dueno_id')
                ->select('negocios.negocio_id','negocios.nombre as nombre_negocio','usuarios.usuario_id as dueno_id','usuarios.carnet')
                ->where('usuarios.estado','=',false)
                ->where('negocios.negocio_id','=',$negocio_id)
                ->get();

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($data)==0)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'La informacion no se encuentra disponible.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$data,
                'mensaje'=>'Exito al encontrar la informacion.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function misNegocios($usuario_id)
    {
        $negocios=Negocio::where(
            array(
                "dueno_id"=>$usuario_id
            )
        )->get();

        if (count($negocios)==0)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Usted aun no registro un negocio.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$negocios,
                'mensaje'=>'Exito al encontrar los negocios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }

    }



    function registrarCuentaNegocio($cuenta)
    {
        DB::beginTransaction();
        try {

            $cuenta->save();
            $data=$cuenta->cuenta_id;
            DB::commit();
        } catch (\Exception $e) {
            $data='Error al realizar la transaccion';
            DB::rollback();
        } catch (Throwable $e) {
            $data='Error al realizar la transaccion';
            DB::rollback();
        }
        return $data;
    }

    public function aceptarAfiliacion($negocio,$cuenta_id,$nombreDueno,$nombre_negocio,$nuevo_numero_cuenta,$correo)
    {
        DB::beginTransaction();
        try {

            $negocio->save();
            $negocio->cuentas()->attach($cuenta_id);

            try{
                Mail::to($correo)->send(new aceptarAfiliacion($nombreDueno,$nombre_negocio,$nuevo_numero_cuenta));
                $data=array(
                    'mensaje'=>'Negocio afiliado',
                    'cuenta_id'=>$cuenta_id,
                    'estado'=>'exito',
                );
                DB::commit();

            }catch(\Exception $e){
                $data=array(
                    'mensaje'=>'No se pudo registrar la cuenta',
                    'estado'=>'error',
                );
                //aqui se eliminaria la cuenta
                DB::rollBack();
            }

        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function rechazarAfiliacion($negocioNuevo,$nombre_negocio,$nombre_dueno,$correo_dueno)
    {
        DB::beginTransaction();
        try {

            $negocioNuevo->save();
            try{
                Mail::to($correo_dueno)->send(new rechazarAfiliacion($nombre_dueno,$nombre_negocio));
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Negocio se actualizo con exito',
                    'descripcion'=>'exito',
                    'estado'=>'exito',
                );
                DB::commit();

            }catch(\Exception $e){
                $data=array(
                    'mensaje'=>'No se pudo rechazar al negocio.',
                    'estado'=>'error',
                );
                DB::rollBack();
            }

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function negocioUniversidad($negocio,$cuenta_id)
    {
        DB::beginTransaction();
        try {

            $negocio->save();
            $negocio->cuentas()->attach($cuenta_id);
            $data=array(
                'mensaje'=>'Negocio creado con exito',
                'Negocio_id'=>$negocio->negocio_id,
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function obtenerNegocioUniversidad()
    {
        $negocio = Negocio::where(
            array(
                'nombre' => 'Universidad'
            )
        )->first();
        return $negocio;
    }
}
