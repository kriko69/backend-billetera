<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 30-03-20
 * Time: 04:59 PM
 */

namespace App\Http\Controllers\Dao;


use App\Models\Alumno_Universidad;
use Illuminate\Support\Facades\DB;

class AlumnoUniversidadDao
{
    public function registrar($alumno)
    {
        DB::beginTransaction();
        try {

            $alumno->save();
            $data=array(
                'mensaje'=>'alumno creado con exito',
                'estado'=>'exito',
                'usuario_id'=>$alumno->alumno_universidad_id
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function verificarExistenciaCarnet($carnet)
    {
        $isset_alumno = Alumno_Universidad::where(
            array(
                'carnet' => $carnet
            )
        )->first();
        if (!is_object($isset_alumno))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }



    public function obtenerAlumno($alumno_id)
    {
        return Alumno_Universidad::find($alumno_id);
    }

    public function actualizar($alumno)
    {
        DB::beginTransaction();
        try {

            $alumno->save();
            $data=array(
                'mensaje'=>'carrera actualizada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function eliminar($alumno)
    {
        DB::beginTransaction();
        try {

            $alumno->save();
            $data=array(
                'mensaje'=>'carrera eliminada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;

    }

    public function listar()
    {
        DB::beginTransaction();
        try {
            $alumnos=DB::table('alumnos_universidad')
                ->select('alumnos_universidad.alumno_universidad_id','alumnos_universidad.carnet','alumnos_universidad.ciudad',
                    'alumnos_universidad.carrera')
                //->where('usuarios.estado','=',false)
                //->orderByDesc('carreras.nombre')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($alumnos)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay alumnos.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$alumnos,
                'mensaje'=>'Exito al encontrar los alumnos.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }

    }

}
