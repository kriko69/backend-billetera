<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 30-03-20
 * Time: 04:23 PM
 */

namespace App\Http\Controllers\Dao;


use App\Models\Carrera;
use Illuminate\Support\Facades\DB;

class CarreraDao
{
    public function registrar($carrera)
    {
        DB::beginTransaction();
        try {

            $carrera->save();
            $data=array(
                'mensaje'=>'carrera creado con exito',
                'estado'=>'exito',
                'usuario_id'=>$carrera->carrera_id
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function verificarExistenciaCarrera($nombre)
    {
        $isset_carrera = Carrera::where(
            array(
                'nombre' => $nombre
            )
        )->first();
        if (!is_object($isset_carrera))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function verificarExistenciaPrefijo($prefijo)
    {
        $isset_carrera = Carrera::where(
            array(
                'prefijo_cuenta' => $prefijo
            )
        )->first();
        if (!is_object($isset_carrera))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    public function obtenerCarrera($carrera_id)
    {
        return Carrera::find($carrera_id);
    }

    public function actualizar($carrera)
    {
        DB::beginTransaction();
        try {

            $carrera->save();
            $data=array(
                'mensaje'=>'carrera actualizada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function eliminar($carrera)
    {
        DB::beginTransaction();
        try {

            $carrera->save();
            $data=array(
                'mensaje'=>'carrera eliminada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;

    }

    public function listar()
    {
        DB::beginTransaction();
        try {
            $carreras=DB::table('carreras')
                ->select('carreras.carrera_id','carreras.nombre',
                    'carreras.estado')
                //->where('usuarios.estado','=',false)
                ->orderBy('carreras.nombre','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($carreras)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay carreras.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$carreras,
                'mensaje'=>'Exito al encontrar los usuarios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }

    }

    function obtenerCarreraPorNombre($nombre)
    {
        $carrera = Carrera::where(
            array(
                'nombre' => $nombre
            )
        )->first();
        return $carrera;
    }

}
