<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-06-20
 * Time: 06:12 PM
 */

namespace App\Http\Controllers\Dao;


use App\Models\Gestion;
use Illuminate\Support\Facades\DB;

class GestionDao
{
    public function obtenerGestion($gestion_id)
    {
        $gestion=Gestion::find($gestion_id);
        return $gestion;
    }

    public function listarGestiones()
    {
        DB::beginTransaction();
        try {
            $gestiones=DB::table('gestiones')
                ->select('gestiones.gestion_id','gestiones.semestre','gestiones.ano',
                    'gestiones.estado')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (\Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($gestiones)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay carreras.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$gestiones,
                'mensaje'=>'Exito al encontrar las gestiones.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function registrarGestion($gestion)
    {
        DB::beginTransaction();
        try {

            $gestion->save();
            $data=array(
                'mensaje'=>'Gestion creada con exito',
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (\Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function actualizarGestion($gestion)
    {
        DB::beginTransaction();
        try {

            $gestion->save();
            $data=array(
                'mensaje'=>'Gestion actualizada con exito',
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (\Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function listarGestionesEstudiante()
    {
        DB::beginTransaction();
        try {
            $gestiones=DB::table('gestiones')
                ->select('gestiones.gestion_id','gestiones.semestre','gestiones.ano',
                    'gestiones.estado')
                ->where('gestiones.estado','=',false)
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (\Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($gestiones)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay gestiones.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$gestiones,
                'mensaje'=>'Exito al encontrar las gestiones.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }
}
