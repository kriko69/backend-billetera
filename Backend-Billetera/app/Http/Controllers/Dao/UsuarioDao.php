<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 16-03-20
 * Time: 03:48 PM
 */

namespace App\Http\Controllers\Dao;


use App\Http\Controllers\Bl\CuentasBl;
use App\Mail\registroAdministrador;
use App\Mail\registroEstudiante;
use App\Models\Cuenta;
use App\Models\Negocio;
use App\Models\Usuario;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UsuarioDao
{
    function obtenerUsuarioParaToken($carnet,$password)
    {
        $user = Usuario::where(
            array(
                'carnet' => $carnet,
                'password' => $password
            )
        )->where('estado','=',false)->first(); //eliminado
        return $user;
    }
    function verificarExistenciaUsuario($carnet)
    {
        $isset_usuario = Usuario::where(
            array(
                'carnet' => $carnet
            )
        )->first();
        if (!is_object($isset_usuario))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    public function obtenerCorreo($usuario_id)
    {
        return Usuario::where(
            array(
                'usuario_id'=>$usuario_id
            )
        )->get(['nombre','apellidos','correo']);
    }

    function registrar($usuario,$password_default)
    {
        DB::beginTransaction();
        try {

            $usuario->save();
            $usuario->roles()->attach(2);
            $data=array(
                'usuario_id'=>$usuario->usuario_id,
                'password_default'=>$password_default,
                'mensaje'=>'Estudiante registrado exitosamente.',
                'estado'=>'exito'
            );
            DB::commit();

            //Storage::disk('ftp')->putFileAs('/imagenes/Empresa',$imagen,$nombreimagen);


        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function registrarVendedor($usuario)
    {
        DB::beginTransaction();
        try {

            $usuario->save();
            $usuario->roles()->attach(3);

            //Storage::disk('ftp')->putFileAs('/imagenes/Empresa',$imagen,$nombreimagen);
            $data=array(
                'mensaje'=>'vendedor creado con exito',
                'descripcion'=>'exito',
                'estado'=>'exito',
                'usuario_id'=>$usuario->usuario_id
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function registrarAdministrador($usuario,$pass,$nombre,$apellido,$correo)
    {
        DB::beginTransaction();
        try {

            $usuario->save();
            $usuario->roles()->attach(1);
            try{
                Mail::to($correo)->send(new registroAdministrador($nombre,$apellido,$pass));
                $data=array(
                    'mensaje'=>'administrador creado con exito',
                    'estado'=>'exito',
                    'usuario_id'=>$usuario->usuario_id
                );
                DB::commit();

            }catch(\Exception $e){
                $data=array(
                    'mensaje'=>'No se pudo registrar al administrador',
                    'estado'=>'error',
                );
                DB::rollBack();
            }

        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion no hay roles',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function mostrarPerfil($id)
    {
        return Usuario::find($id);
    }

    public function verificarCantidadDeCuentas($dueno_id)
    {
        DB::beginTransaction();
        try {
            $cuentas=DB::table('cuentas')
                ->select('cuentas.cuenta_id')
                ->where('cuentas.estado','=',false)
                ->where('cuentas.usuario_id','=',$dueno_id)
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        return $cuentas;
    }

    public function actualizarPerfil($usuarioNuevo)
    {
        DB::beginTransaction();
        try {

            $usuarioNuevo->save();
            $data=array(
                'data'=>null,
                'mensaje'=>'usuario se actualizo con exito',
                'descripcion'=>'exito',
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }



    public function actualizarPassword($usuario_id,$actual_password_encriptada,$nueva_password_encriptada)
    {
        $usuario=$this->mostrarPerfil($usuario_id);
        if(strcmp($usuario->password,$actual_password_encriptada)==0)
        {
            //pasword iguales
            $usuario->password=$nueva_password_encriptada;
            $mensaje=$this->actualizarPerfil($usuario);
            return response()->json($mensaje);
        }else{
            //password distintas
            $data=array(
                'data'=>null,
                'mensaje'=>'Error su actual password no es la correcta.',
                'estado'=>'error'
            );
            return response()->json($data);
        }
    }

    public function listarUsuarios($usuario_id)
    {
        DB::beginTransaction();
        try {
            $usuarios=DB::table('roles_usuarios')
                ->join('usuarios','roles_usuarios.usuario_id','=','usuarios.usuario_id')
                ->join('roles','roles_usuarios.rol_id','=','roles.rol_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos',
                    'usuarios.carnet','usuarios.correo',
                    'usuarios.telefono','usuarios.fecha_nacimiento','usuarios.estado','usuarios.carrera_id','roles.nombre as rol')
                ->where('usuarios.usuario_id','!=',$usuario_id)
                ->whereIn('roles_usuarios.rol_id',[2,3])
                ->orderByDesc('usuarios.apellidos')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($usuarios)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay usuarios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$usuarios,
                'mensaje'=>'Exito al encontrar los usuarios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }


    }

    public function listarUsuariosAdministradores()
    {
        DB::beginTransaction();
        try {
            $usuarios=DB::table('roles_usuarios')
                ->join('usuarios','roles_usuarios.usuario_id','=','usuarios.usuario_id')
                ->join('roles','roles_usuarios.rol_id','=','roles.rol_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos',
                    'usuarios.carnet','usuarios.correo',
                    'usuarios.telefono','usuarios.fecha_nacimiento','usuarios.estado')
                ->where('roles_usuarios.rol_id','=',1)
                ->orderByDesc('usuarios.apellidos')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($usuarios)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay usuarios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$usuarios,
                'mensaje'=>'Exito al encontrar los usuarios.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }


    }

    public function obtenerUsuarioConCarnet($carnet)
    {
        $user = Usuario::where(
            array(
                'carnet' => $carnet
            )
        )->first();
        return $user;
    }
    public function eliminarUsuario($usuario)
    {
        DB::beginTransaction();
        try {

            $usuario->save();
            $data=array(
                'data'=>null,
                'mensaje'=>'usuario se elimino con exito',
                'descripcion'=>'exito',
                'estado'=>'exito',
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function obtenerInformacionUsuarioDeposito($carnet)
    {
        $user = Usuario::where(
            array(
                'carnet' => $carnet
            )
        )->first();
        if(is_object($user))
        {
            $cuentas=$this->obtenerCuentasPorUsuario($user->usuario_id);
            $actx=json_encode($user);

            $act2x=json_encode($cuentas->getData());
            $fusion=array_merge((array)json_decode($actx),(array)json_decode($act2x));
            $fusion["cuentas"]=$fusion["data"];
            unset($fusion["data"]);
            $data=array(
                'data'=>$fusion,
                'descripcion'=>'Exito al encontrar la informacion del usuario.',
                'estado'=>'exito'
            );
            return $data;
        }else{
            $data=array(
                'descripcion'=>'Error el usuario con carnet '.$carnet.' no existe.',
                'estado'=>'error'
            );
            return $data;
        }

    }
    public function obtenerCuentasPorUsuario($usuario_id)
    {
        DB::beginTransaction();
        try {
            $cuentas=DB::table('usuarios')
                ->join('cuentas','usuarios.usuario_id','=','cuentas.usuario_id')
                ->select('cuentas.numero_cuenta','cuentas.saldo')
                ->where('cuentas.estado','=',false)
                ->where('cuentas.usuario_id','=',$usuario_id)
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($cuentas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay cuentas para este usuario.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$cuentas,
                'mensaje'=>'Exito al encontrar las cuentas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function obtenerCuentasPorUsuario2($usuario_id)
    {
        $cuenta = Cuenta::where(
            array(
                'usuario_id' => $usuario_id
            )
        )->where('estado','=',false)->first(); //eliminado
        return $cuenta;
    }

    function verificarResetPassword($carnet,$codigo_reset_password)
    {
        $isset_usuario = Usuario::where(
            array(
                'carnet' => $carnet,
                'codigo_reset_password'=>$codigo_reset_password
            )
        )->first();
        if (!is_object($isset_usuario))
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error el codigo "'.$codigo_reset_password.'" no coincide.',
                'estado'=>'error'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Exito el codigo reset password coincide.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function obtenerSaldo($usuario_id)
    {
        DB::beginTransaction();
        try {
            $saldo=DB::table('cuentas')
                ->where('cuentas.usuario_id', '=', $usuario_id)
                ->where('cuentas.estado','=',false)
                ->sum('cuentas.saldo');
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        $data=array(
            'saldo'=>number_format((float)$saldo, 2, '.', ''),
            'mensaje'=>'Exito al encontrar el saldo.',
            'estado'=>'exito'
        );
        return response()->json($data);
    }

    //pago
    function verificarDueno($dueno_id,$carnet_dueno)
    {
        $isset_usuario = Usuario::where(
            array(
                'usuario_id'=>$dueno_id,
                'carnet' => $carnet_dueno
            )
        )->first();
        if (!is_object($isset_usuario))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function verificarNegocio($negocio_id,$nombre_negocio)
    {
        $isset_negocio = Negocio::where(
            array(
                'negocio_id' => $negocio_id,
                'nombre'=>$nombre_negocio
            )
        )->first();
        if (!is_object($isset_negocio))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function verificarCuenta($dueno_id,$numero_cuenta)
    {
        $isset_cuenta = Cuenta::where(
            array(
                'usuario_id' => $dueno_id,
                'numero_cuenta'=>$numero_cuenta
            )
        )->first();
        if (!is_object($isset_cuenta))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    public function obtenerSaldoPago($usuario_id)
    {

        $cuenta=Cuenta::where(
            array(
                'usuario_id'=>$usuario_id
            )
        )->first();
        return $cuenta;
    }

    function obtenerCuentaEstudiantePago($usuario_id)
    {
        $cuenta = Cuenta::where(
            array(
                'usuario_id' => $usuario_id
            )
        )->first();
        return $cuenta;
    }

    function obtenerCuentaProveedorPago($dueno_id,$numero_cuenta)
    {
        $cuenta = Cuenta::where(
            array(
                'usuario_id' => $dueno_id,
                'numero_cuenta'=>$numero_cuenta
            )
        )->first();
        return $cuenta;
    }

    public function obtenerDatosTransferir($numero_cuenta)
    {
        DB::beginTransaction();
        try {
            $info=DB::table('usuarios')
                ->join('cuentas','cuentas.usuario_id','=','usuarios.usuario_id')
                ->join('carreras','carreras.carrera_id','=','usuarios.carrera_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos','usuarios.carnet','usuarios.correo','usuarios.onesignal_id',
                    'usuarios.carrera_id','usuarios.telefono','cuentas.numero_cuenta','carreras.nombre as nombre_carrera')
                ->where('cuentas.numero_cuenta', '=', $numero_cuenta)
                ->where('cuentas.estado','=',false)
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        $data=array(
            'data'=>$info,
            'mensaje'=>'Exito al encontrar la informacion de la cuenta.',
            'estado'=>'exito'
        );
        return response()->json($data);
    }

    public function obtenerNumeroCuenta($usuario_id)
    {
        $cuentas=Cuenta::where(
            array(
                'usuario_id'=>$usuario_id
            )
        )->get(['numero_cuenta','saldo']);

        if (count($cuentas)==0)
        {
            $data=array(
                'data'=>null,
                'mensaje'=>'Este usuario no tiene cuentas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$cuentas,
                'mensaje'=>'Exito al encontrar las cuentas.',
                'estado'=>'exito'
        );
            return response()->json($data);
        }

    }

    public function crearSuperAdministradorGenesis($usuario)
    {
        DB::beginTransaction();
        try {

            $usuario->save();
            $usuario->roles()->attach(4);
            $data=array(
                'usuario_id'=>$usuario->usuario_id,
                'mensaje'=>'Estudiante registrado exitosamente.',
                'estado'=>'exito'
            );
            DB::commit();

            //Storage::disk('ftp')->putFileAs('/imagenes/Empresa',$imagen,$nombreimagen);


        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }
    public function obtenerCuentaPorUsuario($usuario_id)
    {
        $cuenta=Cuenta::where(
            array(
                'usuario_id'=>$usuario_id
            )
        )->get();
        return $cuenta;
    }
}
