<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-06-20
 * Time: 06:13 PM
 */

namespace App\Http\Controllers\Dao;


use App\Models\Mensualidad;
use Illuminate\Support\Facades\DB;

class MensualidadDao
{
    public function obtenerUltimoNumeroPago($gestion_id)
    {
        $info=DB::table('mensualidades')
            ->select('mensualidades.mensualidad_id','mensualidades.numero_pago')
            ->where('mensualidades.gestion_id','=',$gestion_id)
            ->orderBy('mensualidades.numero_pago','desc')
            ->first();
        return $info;
    }

    public function obtenerMensualidad($mensualidad_id)
    {
        return Mensualidad::find($mensualidad_id);
    }

    public function registrar($mensualidad)
    {
        DB::beginTransaction();
        try {

            $mensualidad->save();
            $data=array(
                'mensaje'=>'Mensualidad creada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function listar($gestion_id)
    {
        DB::beginTransaction();
        try {
            $carreras=DB::table('mensualidades')
                ->select('mensualidades.*')
                ->where('mensualidades.gestion_id','=',$gestion_id)
                ->orderBy('mensualidades.numero_pago','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($carreras)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$carreras,
                'mensaje'=>'Exito al encontrar las mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function actualizar($mensualidad)
    {
        DB::beginTransaction();
        try {

            $mensualidad->save();
            $data=array(
                'mensaje'=>'Mensualidad actualizada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function mensualidadesPagadas($usuario_id,$gestion_id)
    {
        DB::beginTransaction();
        try {
            $pagadas=DB::table('mensualidades_usuarios')
                ->join('usuarios','usuarios.usuario_id','=','mensualidades_usuarios.usuario_id')
                ->join('mensualidades','mensualidades.mensualidad_id','=','mensualidades_usuarios.mensualidad_id')
                ->select('mensualidades.*')
                ->where('mensualidades.gestion_id','=',$gestion_id)
                ->where('usuarios.usuario_id','=',$usuario_id)
                ->orderBy('mensualidades.numero_pago','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        return $pagadas;
    }

    public function mensualidadesNoPagadas($gestion_id,$ids_pagadas)
    {
        DB::beginTransaction();
        try {
            $todas=DB::table('mensualidades')
                ->select('mensualidades.*')
                ->where('mensualidades.gestion_id','=',$gestion_id)
                ->whereNotIn('mensualidades.mensualidad_id',$ids_pagadas)
                ->orderBy('mensualidades.numero_pago','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($todas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$todas,
                'mensaje'=>'Exito al encontrar las mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function todas($gestion_id)
    {
        DB::beginTransaction();
        try {
            $mensualidades=DB::table('mensualidades')
                ->select('mensualidades.*')
                ->where('mensualidades.gestion_id','=',$gestion_id)
                ->orderBy('mensualidades.numero_pago','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        return $mensualidades;
    }

    public function pagadas($usuario_id,$gestion_id)
    {
        DB::beginTransaction();
        try {
            $pagadas=DB::table('mensualidades_usuarios')
                ->join('usuarios','usuarios.usuario_id','=','mensualidades_usuarios.usuario_id')
                ->join('mensualidades','mensualidades.mensualidad_id','=','mensualidades_usuarios.mensualidad_id')
                ->select('mensualidades.*','mensualidades_usuarios.multa_pagada as multa','mensualidades_usuarios.created_at as fecha')
                ->where('mensualidades.gestion_id','=',$gestion_id)
                ->where('usuarios.usuario_id','=',$usuario_id)
                ->orderBy('mensualidades.numero_pago','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($pagadas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$pagadas,
                'mensaje'=>'Exito al encontrar las mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function pagarMensaulidad($mensualidad,$cuenta,$cuenta_universidad,$usuario_id,$multa)
    {
        DB::beginTransaction();
        try {
            $cuenta->save();
            $cuenta_universidad->save();
            $mensualidad->save();
            $mensualidad->usuarios()->attach($usuario_id, ['multa_pagada' => $multa]);
            $data=array(
                'mensaje'=>'Mensualidad pagada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function verificarPago($numero_pago,$mensualidad_id,$usuario_id)
    {
        DB::beginTransaction();
        try {
            $pagada=DB::table('mensualidades_usuarios')
                ->select('mensualidades_usuarios.*')
                ->where('mensualidades_usuarios.mensualidad_id','=',$mensualidad_id)
                ->where('mensualidades_usuarios.usuario_id','=',$usuario_id)
                ->get();
            return $pagada;
        } catch (\Exception $e) {

            DB::rollback();
            return null;
        } catch (Throwable $e) {

            DB::rollback();
            return null;
        }
    }


    public function obtenerMensualidadPorNumeroPago($numero_pago,$gestion_id)
    {

        $user = Mensualidad::where(
            array(
                'gestion_id'=>$gestion_id,
                'numero_pago'=>$numero_pago
            )
        )->first();
        return $user;
    }

    public function pagosMensualidades($mensualidad_id)
    {
        DB::beginTransaction();
        try {
            $pagadas=DB::table('mensualidades_usuarios')
                ->join('mensualidades','mensualidades.mensualidad_id','=','mensualidades_usuarios.mensualidad_id')
                ->join('usuarios','usuarios.usuario_id','=','mensualidades_usuarios.usuario_id')
                ->join('cuentas','cuentas.usuario_id','=','mensualidades_usuarios.usuario_id')
                ->select('mensualidades_usuarios.multa_pagada as multa','usuarios.nombre','usuarios.apellidos','usuarios.carnet','cuentas.numero_cuenta','mensualidades_usuarios.created_at as fecha_del_pago','mensualidades.monto')
                ->where('mensualidades_usuarios.mensualidad_id','=',$mensualidad_id)
                ->orderBy('usuarios.apellidos','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($pagadas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$pagadas,
                'mensaje'=>'Exito al encontrar las mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }

    public function pagosMensualidadesPorGestion($gestion_id)
    {
        DB::beginTransaction();
        try {
            $pagadas=DB::table('mensualidades_usuarios')
                ->join('usuarios','usuarios.usuario_id','=','mensualidades_usuarios.usuario_id')
                ->join('mensualidades','mensualidades.mensualidad_id','=','mensualidades_usuarios.mensualidad_id')
                ->join('cuentas','cuentas.usuario_id','=','usuarios.usuario_id')
                ->select('mensualidades_usuarios.multa_pagada as multa','usuarios.usuario_id','usuarios.nombre as nombre_usuario','usuarios.apellidos','usuarios.carnet','cuentas.numero_cuenta',
                'mensualidades.nombre','mensualidades_usuarios.created_at as fecha_del_pago','mensualidades.monto','mensualidades.mensualidad_id')
                ->where('mensualidades.gestion_id','=',$gestion_id)
                ->orderBy('usuarios.apellidos','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($pagadas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$pagadas,
                'mensaje'=>'Exito al encontrar las mensualidades.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }
    }
}
