<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 31-03-20
 * Time: 05:11 PM
 */

namespace App\Http\Controllers\Dao;


use App\Models\tipo_transaccion;
use Illuminate\Support\Facades\DB;

class TipoTransaccionDao
{
    public function registrar($tipo_transaccion)
    {
        DB::beginTransaction();
        try {

            $tipo_transaccion->save();
            $data=array(
                'mensaje'=>'tipo de transaccion creado con exito',
                'estado'=>'exito',
                'tipo_transaccion_id'=>$tipo_transaccion->tipo_transaccion_id
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function verificarExistenciaNombre($nombre)
    {
        $isset_tipo = tipo_transaccion::where(
            array(
                'nombre' => $nombre
            )
        )->where('estado','=',false)->first();
        if (!is_object($isset_tipo))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function obtenerTipoTransaccion($nombre)
    {
        $tipo = tipo_transaccion::where(
            array(
                'nombre' => $nombre
            )
        )->first();
        return $tipo;
    }


    public function obtenerTipo($tipo_id)
    {
        return tipo_transaccion::find($tipo_id);
    }

    public function listar()
    {
        DB::beginTransaction();
        try {
            $tipos=DB::table('tipo_transacciones')
                ->select('tipo_transacciones.tipo_transaccion_id','tipo_transacciones.nombre','tipo_transacciones.descripcion',
                    'tipo_transacciones.estado')
                //->where('usuarios.estado','=',false)
                ->orderBy('tipo_transacciones.tipo_transaccion_id','asc')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($tipos)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay tipos de transacciones disponibles.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$tipos,
                'mensaje'=>'Exito al encontrar los tipos de transacciones.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }

    }
}
