<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 31-03-20
 * Time: 04:32 PM
 */

namespace App\Http\Controllers\Dao;


use App\Mail\registroEstudiante;
use App\Models\Cuenta;
use App\Models\CuentaNegocio;
use App\Models\transaccion;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CuentasDao
{
    public function obtenerUltimaCuenta()
    {
        $cuenta=DB::table('cuentas')
            ->select('cuentas.*')
            ->orderBy('cuentas.created_at','desc')
            ->take(1)
            ->get();
        return $cuenta;
    }
    public function registrar($cuenta,$pass,$correo,$nombre)
    {

        DB::beginTransaction();
        try {

            $cuenta->save();
            try{
                Mail::to($correo)->send(new registroEstudiante($nombre,$pass,$cuenta->numero_cuenta));
                $data=array(
                    'mensaje'=>'cuenta creado con exito',
                    'estado'=>'exito',
                    'usuario_id'=>$cuenta->cuenta_id
                );
                DB::commit();

            }catch(\Exception $e){
                $data=array(
                    'mensaje'=>'No se pudo registrar la cuenta',
                    'estado'=>'error',
                );
                DB::rollBack();
            }


        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    function verificarExistenciaCuenta($numero_cuenta)
    {
        $isset_cuenta = Cuenta::where(
            array(
                'numero_cuenta' => $numero_cuenta
            )
        )->first();
        if (!is_object($isset_cuenta))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    function verificarSaldo($numero_cuenta)
    {
        $cuenta = Cuenta::where(
            array(
                'numero_cuenta' => $numero_cuenta
            )
        )->first();
        return $cuenta->saldo;
    }

    public function obtenerCuenta($cuenta_id)
    {
        return Cuenta::find($cuenta_id);
    }

    public function actualizar($cuenta)
    {
        DB::beginTransaction();
        try {

            $cuenta->save();
            $data=array(
                'mensaje'=>'cuenta actualizada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function eliminar($cuenta)
    {
        DB::beginTransaction();
        try {

            $cuenta->save();
            $data=array(
                'mensaje'=>'cuenta eliminada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;

    }

    public function listar()
    {
        DB::beginTransaction();
        try {
            $cuentas=DB::table('usuarios')
                ->join('cuentas','usuarios.usuario_id','=','cuentas.usuario_id')
                ->join('carreras','usuarios.carrera_id','=','carreras.carrera_id')
                ->select('usuarios.usuario_id','usuarios.nombre','usuarios.apellidos',
                    'usuarios.carnet','usuarios.carrera_id','usuarios.estado as estado_del_ususario','cuentas.cuenta_id','cuentas.numero_cuenta',
                    'cuentas.saldo','cuentas.estado as estado_de_la_cuenta','carreras.nombre as nombre_carrera')
                //->where('usuarios.estado','=',false)
                ->orderByDesc('usuarios.nombre')
                ->get();
        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        if (sizeof($cuentas)==0)
        {
            $data=array(
                'data'=>null,
                'descripcion'=>'No hay cuentas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }else{
            $data=array(
                'data'=>$cuentas,
                'mensaje'=>'Exito al encontrar las cuentas.',
                'estado'=>'exito'
            );
            return response()->json($data);
        }

    }

    function obtenerCuentaPorNumeroCuenta($numero_cuenta)
    {
        $isset_cuenta = Cuenta::where(
            array(
                'numero_cuenta' => $numero_cuenta
            )
        )->where('estado','=',false)->first();
        return $isset_cuenta;
    }

    public function verificarRolesDeCuenta($numero_cuenta)
    {
        DB::beginTransaction();
        try {
            $roles=DB::table('roles_usuarios')
                ->join('usuarios','roles_usuarios.usuario_id','=','usuarios.usuario_id')
                ->join('cuentas','usuarios.usuario_id','=','cuentas.usuario_id')
                ->select('roles_usuarios.rol_id')
                ->where('cuentas.estado','=',false)
                ->where('cuentas.numero_cuenta','=',$numero_cuenta)
                ->get();

        } catch (\Exception $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        } catch (Throwable $e) {
            $data=array(
                'data'=>null,
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
            return $data;
        }
        return $roles;
    }

    public function depositar($cuenta,$transaccion)
    {
        DB::beginTransaction();
        try {

            $cuenta->save();
            $transaccion->save();
            $data=array(
                'mensaje'=>'transaccion realizada con exito',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }


    public function pagar($cuenta_estudiante,$cuenta_proveedor,$transaccion)
    {
        DB::beginTransaction();
        try {
            //return $cuenta_estudiante;
            $cuenta_estudiante->save();
            $cuenta_proveedor->save();
            $transaccion->save();
            $data=array(
                'mensaje'=>'Pago realizada con exito.',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;

    }

    public function transferir($cuenta_usuario,$cuenta_destino,$transaccion)
    {
        DB::beginTransaction();
        try {
            $cuenta_usuario->save();
            $cuenta_destino->save();
            $transaccion->save();
            $data=array(
                'mensaje'=>'Transferencia realizada con exito.',
                'estado'=>'exito'
            );
            DB::commit();
        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;

    }

    public function verificarRolesDeCuenta2($cuenta_id)
    {
        $cuenta = CuentaNegocio::where(
            array(
                'cuenta_id' => $cuenta_id
            )
        )->first();
        if (!is_object($cuenta))
        {
            //no existe
            return false;
        }else{
            //existe
            return true;
        }
    }

    public function misMovimientosDe($cuenta_id)
    {
        $de_mi=transaccion::where(
            array(
                'cuenta_de'=>$cuenta_id
            )
        )->get('cuenta_para');
        $a_quienes=array();
        for ($i=0;$i<count($de_mi);$i++)
        {
            array_push($a_quienes,$de_mi[$i]->cuenta_para);
        }
        $infos=array();

        for ($i=0;$i<count($a_quienes);$i++)
        {
            $data=$this->obtenerDatosTransaccion1($a_quienes[$i]);
            array_push($infos,$data[0]);

        }
        $infos=array_reverse($infos);
        $de_mi2=$this->obtenerDatosTransaccion2($cuenta_id);
        $transacciones_de_mi=array();
        //return $de_mi2;
        for ($i=0;$i<count($infos);$i++)
        {
            $auxData=array(
                "transaccion_id"=>$de_mi2[$i]->transaccion_id,
                "tipo_transaccion"=>$de_mi2[$i]->nombre, //nombre
                "cuenta_id_de"=>$de_mi2[$i]->de,
                "cuenta_id_para"=>$de_mi2[$i]->para,
                "nombre_para"=>$infos[$i]->nombre,
                "apellidos_para"=>$infos[$i]->apellidos,
                "numero_cuenta_para"=>$infos[$i]->numero_cuenta,
                "monto"=>$de_mi2[$i]->monto,
                "fecha"=>$de_mi2[$i]->fecha
            );
            array_push($transacciones_de_mi,$auxData);
        }
        return $transacciones_de_mi;


    }

    public function obtenerDatosTransaccion1($cuenta_para)
    {
        $info=DB::table('usuarios')
            ->join('cuentas','usuarios.usuario_id','=','cuentas.usuario_id')
            ->select('usuarios.nombre','usuarios.apellidos','cuentas.numero_cuenta')
            ->where('cuentas.cuenta_id','=',$cuenta_para)
            ->get();
        return $info;
    }

    public function obtenerDatosTransaccion2($cuenta_id)
    {

        $info=DB::table('transacciones')
            ->join('cuentas','transacciones.cuenta_de','=','cuentas.cuenta_id')
            ->join('tipo_transacciones','transacciones.tipo_transaccion_id','=','tipo_transacciones.tipo_transaccion_id')
            ->select('transacciones.transaccion_id','tipo_transacciones.nombre','transacciones.cuenta_de as de',
                'transacciones.cuenta_para as para','transacciones.monto','transacciones.created_at as fecha')
            ->where('transacciones.cuenta_de','=',$cuenta_id)
            ->orderBy('fecha','desc')
            ->get();
        return $info;
    }

    public function misMovimientosPara($cuenta_id)
    {
        $para_mi=transaccion::where(
            array(
                'cuenta_para'=>$cuenta_id
            )
        )->get('cuenta_de');

        $a_quienes=array();
        for ($i=0;$i<count($para_mi);$i++)
        {
            array_push($a_quienes,$para_mi[$i]->cuenta_de);
        }
        //return $a_quienes;
        $infos=array();
        for ($i=0;$i<count($a_quienes);$i++)
        {
            $data=$this->obtenerDatosTransaccion3($a_quienes[$i]);
            array_push($infos,$data[0]);

        }
        $infos=array_reverse($infos);
        $de_mi2=$this->obtenerDatosTransaccion4($cuenta_id);
        $transacciones_de_mi=array();
        //return $de_mi2;
        for ($i=0;$i<count($infos);$i++)
        {
            $auxData=array(
                "transaccion_id"=>$de_mi2[$i]->transaccion_id,
                "tipo_transaccion"=>$de_mi2[$i]->nombre, //nombre
                "cuenta_id_de"=>$de_mi2[$i]->de,
                "cuenta_id_para"=>$de_mi2[$i]->para,
                "nombre_de"=>$infos[$i]->nombre,
                "apellidos_de"=>$infos[$i]->apellidos,
                "numero_cuenta_de"=>$infos[$i]->numero_cuenta,
                "monto"=>$de_mi2[$i]->monto,
                "fecha"=>$de_mi2[$i]->fecha
            );
            array_push($transacciones_de_mi,$auxData);
        }
        return $transacciones_de_mi;

    }

    public function obtenerDatosTransaccion3($cuenta_de)
    {
        $info=DB::table('usuarios')
            ->join('cuentas','usuarios.usuario_id','=','cuentas.usuario_id')
            ->select('usuarios.nombre','usuarios.apellidos','cuentas.numero_cuenta')
            ->where('cuentas.cuenta_id','=',$cuenta_de)
            ->get();
        return $info;
    }

    public function obtenerDatosTransaccion4($cuenta_id)
    {

        $info=DB::table('transacciones')
            ->join('cuentas','transacciones.cuenta_de','=','cuentas.cuenta_id')
            ->join('tipo_transacciones','transacciones.tipo_transaccion_id','=','tipo_transacciones.tipo_transaccion_id')
            ->select('transacciones.transaccion_id','tipo_transacciones.nombre','transacciones.cuenta_de as de',
                'transacciones.cuenta_para as para','transacciones.monto','transacciones.created_at as fecha')
            ->where('transacciones.cuenta_para','=',$cuenta_id)
            ->orderBy('fecha','desc')
            ->get();
        return $info;
    }

    public function cuentaUniversidad($cuenta)
    {

        DB::beginTransaction();
        try {

            $cuenta->save();
            $data=array(
                'mensaje'=>'Cuenta de la universidad creada con exito',
                'estado'=>'exito',
                'cuenta_id'=>$cuenta->cuenta_id
            );
            DB::commit();


        } catch (\Exception $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>$e,
                'estado'=>'error'
            );
            DB::rollback();
        } catch (Throwable $e) {
            $data=array(
                'mensaje'=>'Error al realizar la transaccion',
                'descripcion'=>'fallo',
                'estado'=>'error'
            );
            DB::rollback();
        }
        return $data;
    }

    public function obtenerCuentaPorUsuarioId($usuario_id)
    {
        $cuenta = Cuenta::where(
            array(
                'usuario_id' => $usuario_id
            )
        )->first();
        return $cuenta;
    }

}
