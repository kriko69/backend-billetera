<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\MensualidadBl;
use App\Http\Controllers\Dao\MensualidadDao;
use Illuminate\Http\Request;

class MensualidadController extends Controller
{
    public function registrar(Request $request)
    {

        $nombre=$request->json("nombre");
        $fecha_pago=$request->json("fecha_pago");
        $gestion_id=$request->json("gestion_id");
        $monto=$request->json("monto");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            $data=$bl->registrar($payload->roles,$nombre, $fecha_pago, $monto,$gestion_id); //falta imagen

            return response()->json($data,200);
        }


    }

    public function listar(Request $request)
    {

        $gestion_id=$request->json("gestion_id");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            return $bl->listar($payload->roles,$gestion_id);
        }


    }

    public function actualizar(Request $request)
    {

        $nombre=$request->json("nombre");
        $fecha_pago=$request->json("fecha_pago");
        $monto=$request->json("monto");
        $mensualidad_id=$request->json("mensualidad_id");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            $data=$bl->actualizar($payload->roles,$nombre, $fecha_pago, $monto,$mensualidad_id); //falta imagen

            return response()->json($data,200);
        }


    }

    public function listarMensualidadesNoPagadas(Request $request)
    {

        $gestion_id=$request->json("gestion_id");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            return $bl->listarMensualidadesNoPagadas($payload->sub,$payload->roles,$gestion_id);
        }


    }

    public function listarMensualidadesPagadas(Request $request)
    {

        $gestion_id=$request->json("gestion_id");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            return $bl->listarMensualidadesPagadas($payload->sub,$payload->roles,$gestion_id);
        }


    }

    public function pagarMensualidad(Request $request)
    {

        $mensualidad_id=$request->json("mensualidad_id");
        $multa=$request->json("multa");
        $total=$request->json("total");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            /*$mensualidadDao=new MensualidadDao();
            $m=$mensualidadDao->obtenerMensualidad($mensualidad_id);
            $se_puede=$bl->verificar($m,$payload->sub);
            if ($se_puede) {*/
                return $bl->pagarMensaulidad($payload->sub, $payload->roles, $mensualidad_id, $multa, $total);
            /*}
            else{
                $data = array(
                    'mensaje' => 'Tiene un pago pendiente, pague primero lo que debe.',
                    'estado'=>'error'
                );
                return $data;
            }*/
        }


    }

    public function pagosMensualidades(Request $request)
    {

        $mensualidad_id=$request->json("mensualidad_id");
        //$gestion_id=$request->json("gestion_id");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            return $bl->pagosMensualidades($payload->sub, $payload->roles, $mensualidad_id);

        }


    }

    public function pagosMensualidadesPorGestion(Request $request)
    {

        $gestion_id=$request->json("gestion_id");
        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto',
                'estado'=>'error'

            );
            return response()->json($data);

        }else{

            $bl = new MensualidadBl();
            return $bl->pagosMensualidadesPorGestion($payload->sub, $payload->roles,$gestion_id);

        }


    }



}
