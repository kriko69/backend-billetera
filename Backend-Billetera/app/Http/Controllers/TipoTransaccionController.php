<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use App\Http\Controllers\Bl\TipoTransaccionBl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TipoTransaccionController extends Controller
{
    public function registrar(Request $request)
    {

        $rules = [
            'nombre' => 'required',
            'descripcion' => 'required',
        ];
        $customMessages = [
            'required' => ':attribute es requerido.',
            'regex' => 'El atributo :attribute es invalido.',
            'numeric' => 'El atributo :attribute debe ser solo numeros.',
        ];
        $validatorUsuario = Validator::make($request->json()->all(),$rules,$customMessages);
        if ($validatorUsuario->fails()) {
            //valida null, en blanco o no existe
            $data=array(
                'errores'=>$validatorUsuario->errors()
            );
            return response()->json($data);
        }

        $nombre=$request->json("nombre");
        $descripcion=$request->json("descripcion");
        $token=$request->header('Authorization',null);
        if(!is_null($nombre) && !is_null($descripcion)){

            $jwt = new JwtAuth();
            $payload=$jwt->verificarToken($token);
            if(!$payload)
            {
                $data=array(
                    'mensaje'=>'Token incorrecto'
                );
                return response()->json($data);
            }else{
                $bl=new TipoTransaccionBl();
                return $bl->registrar($payload->sub,$payload->roles,$nombre, $descripcion);
            }
        }else{
            $data=array(
                'mensaje'=>'Datos invalidos',
                'descripcion'=>'algun parametro en null'
            );
            return response()->json($data);
        }

    }

    public function listar(Request $request)
    {

        $token=$request->header('Authorization',null);
        $jwt = new JwtAuth();
        $payload=$jwt->verificarToken($token);
        if(!$payload)
        {
            $data=array(
                'mensaje'=>'Token incorrecto'
            );
            return response()->json($data);
        }else{
            $bl=new TipoTransaccionBl();
            return $bl->listar($payload->sub,$payload->roles);
        }
    }

}
