<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-06-20
 * Time: 06:10 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\GestionDao;
use App\Models\Gestion;

class GestionBl
{
    public function listarGestiones($usuario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $gestionDao=new GestionDao();
            return $gestionDao->listarGestiones();
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function registrarGestion($usuario_id,$roles,$semestre,$ano)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $gestionDao=new GestionDao();
            $gestion=new Gestion();
            $gestion->semestre=$semestre;
            $gestion->ano=$ano;
            return $gestionDao->registrarGestion($gestion);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function eliminarGestion($usuario_id,$roles,$gestion_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $gestionDao=new GestionDao();
            $g=$gestionDao->obtenerGestion($gestion_id);
            $g->estado=true;
            return $gestionDao->actualizarGestion($g);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function activarGestion($usuario_id,$roles,$gestion_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $gestionDao=new GestionDao();
            $g=$gestionDao->obtenerGestion($gestion_id);
            $g->estado=false;
            return $gestionDao->actualizarGestion($g);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listarGestionesEstudiante($usuario_id,$roles)
    {
        if(in_array(2,$roles))
        {
            $gestionDao=new GestionDao();
            return $gestionDao->listarGestionesEstudiante();
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
}
