<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 30-03-20
 * Time: 04:58 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\AlumnoUniversidadDao;
use App\Models\Alumno_Universidad;

class AlumnoUniversidadBl
{
    public function registrar($ususario_id,$roles,$carnet,$ciudad,$carrera)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $alumnoDao= new AlumnoUniversidadDao();
            $existeCarnet=$alumnoDao->verificarExistenciaCarnet($carnet);

            if($existeCarnet)
            {
                $data = array(
                    'mensaje' => 'El nombre de la carrera ya existe.',
                    'estado'=>'error'
                );
                return $data;
            }else{
                $alumno=new Alumno_Universidad();
                $alumno->carnet=$carnet;
                $alumno->ciudad=$ciudad;
                $alumno->carrera=$carrera;
                return $alumnoDao->registrar($alumno);
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function actualizar($ususario_id,$roles,$alumno_universidad_id, $ciudad,$carrera)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $alumnoDao= new AlumnoUniversidadDao();
            $alumno=$alumnoDao->obtenerAlumno($alumno_universidad_id);
            $alumno->ciudad=$ciudad;
            $alumno->carrera=$carrera;
            return $alumnoDao->actualizar($alumno);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    /*public function eliminar($ususario_id,$roles,$alumno_universidad_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $alumnoDao= new AlumnoUniversidadDao();
            $alumno=$alumnoDao->obtenerAlumno($alumno_universidad_id);
            $alumno->estado=true;
            return $alumnoDao->eliminar($alumno);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }*/

    public function listar($ususario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $alumnoDao= new AlumnoUniversidadDao();
            return $alumnoDao->listar();
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
}
