<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 31-03-20
 * Time: 05:10 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\TipoTransaccionDao;
use App\Models\tipo_transaccion;

class TipoTransaccionBl
{
    public function registrar($ususario_id,$roles,$nombre,$descripcion)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $dao= new TipoTransaccionDao();
            $nombre=ucfirst($nombre);
            $existeNombre=$dao->verificarExistenciaNombre($nombre);
            if($existeNombre)
            {
                $data = array(
                    'mensaje' => 'El nombre del tipo de transaccion ya existe.',
                    'estado'=>'error'
                );
                return $data;
            }else{
                $tipo_transaccion=new tipo_transaccion();
                $tipo_transaccion->nombre=$nombre;
                $tipo_transaccion->descripcion=$descripcion;
                return $dao->registrar($tipo_transaccion);
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function listar($ususario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $dao= new TipoTransaccionDao();
            return $dao->listar();
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
}
