<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 31-03-20
 * Time: 04:31 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\CuentasDao;
use App\Http\Controllers\Dao\NegocioDao;
use App\Http\Controllers\Dao\UsuarioDao;
use App\Models\Cuenta;
use App\Models\transaccion;
use phpDocumentor\Reflection\Types\Array_;

class CuentasBl
{
    public function esEsudiante($usuario_id)
    {
        $usuarioDao= new UsuarioDao();
        $estudiante=$usuarioDao->mostrarPerfil($usuario_id);
        $roles_usuario=array();
        foreach ($estudiante->roles as $rol)
        {
            array_push ($roles_usuario,$rol->rol_id);
        }
        if (in_array(2,$roles_usuario))
        {
            return true;
        }else{
            return false;
        }
    }
    public function registrarEstudiante($saldo,$dueno_id,$pass)
    {
            $cuentaDao= new CuentasDao();
            $usuarioDao= new UsuarioDao();
            $correo=$usuarioDao->obtenerCorreo($dueno_id);
            $nombre=$correo[0]->nombre.' '.$correo[0]->apellidos;
            $correo=$correo[0]->correo;
            $numeroCuentaInicio=5910000001;
            $existeCuenta=$cuentaDao->verificarExistenciaCuenta($numeroCuentaInicio);
            if($existeCuenta)
            {

                $ultima_cuenta=$cuentaDao->obtenerUltimaCuenta();
                $numero=$ultima_cuenta[0]->numero_cuenta;
                $nuevo_numero_cuenta=$numero+1;
                if ($this->esEsudiante($dueno_id))
                {
                    //es estudiante
                    /*$numero_de_cuentas=$usuarioDao->verificarCantidadDeCuentas($dueno_id);
                    //return count($numero_de_cuentas);
                    if (count($numero_de_cuentas)==0){

                    }else{
                        //no tiene cuentas
                        $data = array(
                            'mensaje' => 'El usuario figura como estudiante y solo puede tener una cuenta.',
                            'estado'=>'error'
                        );
                        return $data;
                    }*/
                    $cuenta=new Cuenta();
                    $cuenta->numero_cuenta=$nuevo_numero_cuenta;
                    $cuenta->saldo=$saldo;
                    $cuenta->usuario_id=$dueno_id;
                    return $cuentaDao->registrar($cuenta,$pass,$correo,$nombre);

                }else{
                    $data = array(
                        'mensaje' => 'El usuario no es un estudiante.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                if ($this->esEsudiante($dueno_id))
                {
                    //es estudiante
                    //es la primera cuenta
                    $cuenta=new Cuenta();
                    $cuenta->numero_cuenta=$numeroCuentaInicio;
                    $cuenta->saldo=$saldo;
                    $cuenta->usuario_id=$dueno_id;
                    return $cuentaDao->registrar($cuenta,$pass,$correo,$nombre);
                }else{
                    $data = array(
                        'mensaje' => 'El usuario no es un estudiante.',
                        'estado'=>'error'
                    );
                    return $data;
                }

            }

    }

    public function registrarProveedor($usuario_id,$roles,$numero_cuenta,$saldo,$dueno_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $cuentaDao= new CuentasDao();
            $usuarioDao= new UsuarioDao();
            $existeCuenta=$cuentaDao->verificarExistenciaCuenta($numero_cuenta);
            if($existeCuenta)
            {
                $data = array(
                    'mensaje' => 'El numero de cuenta ya existe ya existe.',
                    'estado'=>'error'
                );
                return $data;
            }else{
                if (!$this->esEsudiante($dueno_id))
                {
                    //es proveedor
                    $cuenta=new Cuenta();
                    $cuenta->numero_cuenta=$numero_cuenta;
                    $cuenta->saldo=$saldo;
                    $cuenta->usuario_id=$dueno_id;
                    return $cuentaDao->registrar($cuenta);
                }else{
                    $data = array(
                        'mensaje' => 'El usuario no es un proveedor.',
                        'estado'=>'error'
                    );
                    return $data;
                }

            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function actualizar($ususario_id,$roles,$cuenta_id,$saldo)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $cuentaDao= new CuentasDao();
            $cuenta=$cuentaDao->obtenerCuenta($cuenta_id);
            $cuenta->saldo=$saldo;
            return $cuentaDao->actualizar($cuenta);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function eliminar($ususario_id,$roles,$cuenta_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $cuentaDao= new CuentasDao();
            $cuenta=$cuentaDao->obtenerCuenta($cuenta_id);
            $cuenta->estado=true;
            return $cuentaDao->eliminar($cuenta);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function activar($ususario_id,$roles,$cuenta_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $cuentaDao= new CuentasDao();
            $cuenta=$cuentaDao->obtenerCuenta($cuenta_id);
            $cuenta->estado=false;
            return $cuentaDao->eliminar($cuenta);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listar($ususario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $cuentaDao= new CuentasDao();
            return $cuentaDao->listar();
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function depositar($usuario_id,$roles,$numero_cuenta, $monto)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $cuentaDao= new CuentasDao();
            $transaccionBl = new TransaccionBl();
            $existeCuenta= $cuentaDao->verificarExistenciaCuenta($numero_cuenta);
            if ($existeCuenta)
            {

                $cuenta=$cuentaDao->obtenerCuentaPorNumeroCuenta($numero_cuenta);
                $cuenta->saldo=$cuenta->saldo+$monto;
                $transaccion=$transaccionBl->registro($monto,$cuenta->cuenta_id,$cuenta->cuenta_id,'Deposito');
                                                    //monto    cuenta_de           cuenta_para          nombre del tipo de transaccion

                if (is_object($transaccion))
                {
                    //return $cuenta;
                    //return $transaccion;
                    return $cuentaDao->depositar($cuenta,$transaccion); //hacen la misma funcion
                }else{
                    return $transaccion;
                }
            }else{
                $data = array(
                    'mensaje' => 'La cuenta '.$numero_cuenta.' no existe.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerRolesDeCuenta($numero_cuenta)
    {
        $cuentaDao= new CuentasDao();
        $roles= Array();
        $roles_cuenta=$cuentaDao->verificarRolesDeCuenta($numero_cuenta);
        foreach ($roles_cuenta as $rol)
        {
            array_push($roles,$rol->rol_id);
        }
        return $roles;
    }
    public function retirar($usuario_id,$roles,$numero_cuenta)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $cuentaDao= new CuentasDao();
            $transaccionBl = new TransaccionBl();
            $existeCuenta= $cuentaDao->verificarExistenciaCuenta($numero_cuenta);
            if ($existeCuenta)
            {
                $cuenta=$cuentaDao->obtenerCuentaPorNumeroCuenta($numero_cuenta);
                $original_saldo=$cuenta->saldo;
                $cuenta->saldo=0;
                $transaccion=$transaccionBl->registro($original_saldo,$cuenta->cuenta_id,$cuenta->cuenta_id,'Retiro');
                //monto    cuenta_de           cuenta_para          nombre del tipo de transaccion
                if (is_object($transaccion))
                {
                    //return $cuenta;
                    //return $transaccion;
                    return $cuentaDao->depositar($cuenta,$transaccion); //hacen la misma funcion
                }else{
                    return $transaccion;
                }


            }else{
                $data = array(
                    'mensaje' => 'La cuenta '.$numero_cuenta.' no existe.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function misMovimientosMios($usuario_id,$roles,$numero_cuenta)
    {
        $cuentaDao= new CuentasDao();
        $existeCuenta= $cuentaDao->verificarExistenciaCuenta($numero_cuenta);
        if ($existeCuenta)
        {
            $cuenta=$cuentaDao->obtenerCuentaPorNumeroCuenta($numero_cuenta);
            $cuenta_id=$cuenta->cuenta_id;
            $movimientosDeMi=$cuentaDao->misMovimientosDe($cuenta_id);
            //$movimientosParaMi=$cuentaDao->misMovimientosPara($cuenta_id);
            //return $movimientosDeMi; //los movimientos que yo hice
            //return $movimientosParaMi; //los movimientos que me hicieron
            //$misMovimientosGeneral=array_merge($movimientosDeMi,$movimientosParaMi);
            //$misMovimientosGeneral=array_unique($misMovimientosGeneral,SORT_REGULAR); //elimino los repetidos

            if (count($movimientosDeMi)==0)
            {
                $data = array(
                    'data'=>null,
                    'mensaje' => 'La cuenta '.$numero_cuenta.' no tiene movimientos.',
                    'estado'=>'exito'
                );
                return $data;
            }else{
                $data = array(
                    'data'=>$movimientosDeMi,
                    'mensaje' => 'Se encontraron los movimientos.',
                    'estado'=>'exito'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'La cuenta '.$numero_cuenta.' no existe.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function misMovimientosParaMi($usuario_id,$roles,$numero_cuenta)
    {
        $cuentaDao= new CuentasDao();
        $existeCuenta= $cuentaDao->verificarExistenciaCuenta($numero_cuenta);
        if ($existeCuenta)
        {
            $cuenta=$cuentaDao->obtenerCuentaPorNumeroCuenta($numero_cuenta);
            $cuenta_id=$cuenta->cuenta_id;
            //$movimientosDeMi=$cuentaDao->misMovimientosDe($cuenta_id);
            $movimientosParaMi=$cuentaDao->misMovimientosPara($cuenta_id);
            //return $movimientosDeMi; //los movimientos que yo hice
            //return $movimientosParaMi; //los movimientos que me hicieron
            //$misMovimientosGeneral=array_merge($movimientosDeMi,$movimientosParaMi);
            //$misMovimientosGeneral=array_unique($misMovimientosGeneral,SORT_REGULAR); //elimino los repetidos

            if (count($movimientosParaMi)==0)
            {
                $data = array(
                    'data'=>null,
                    'mensaje' => 'La cuenta '.$numero_cuenta.' no tiene movimientos.',
                    'estado'=>'exito'
                );
                return $data;
            }else{
                $data = array(
                    'data'=>$movimientosParaMi,
                    'mensaje' => 'Se encontraron los movimientos.',
                    'estado'=>'exito'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'La cuenta '.$numero_cuenta.' no existe.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function cuentaUniversidad($superadmin_id,$roles)
    {
        if(in_array(4,$roles)) //super admin 4
        {
            $cuenta=new Cuenta();
            $cuentaDao=new CuentasDao();
            $cuenta->numero_cuenta=9990000001;
            $cuenta->saldo=0;
            $cuenta->usuario_id=$superadmin_id;
            return $cuentaDao->cuentaUniversidad($cuenta);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerTotalUniversidad($superadmin_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $negocioDao=new NegocioDao();
            $cuentaDao=new CuentasDao();
            $negocio=$negocioDao->obtenerNegocioUniversidad();
            $negocio_cuenta_id=$negocio->cuentas[0]->cuenta_id;
            $cuenta_universidad=$cuentaDao->obtenerCuenta($negocio_cuenta_id);
            $data = array(
                'data'=>$cuenta_universidad,
                'estado'=>'exito'
            );
            return $data;
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

}
