<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 09-06-20
 * Time: 06:13 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\CuentasDao;
use App\Http\Controllers\Dao\GestionDao;
use App\Http\Controllers\Dao\MensualidadDao;
use App\Http\Controllers\Dao\NegocioDao;
use App\Models\Mensualidad;

class MensualidadBl
{
    public function registrar($roles,$nombre,$fecha_pago,$monto,$gestion_id)
    {

        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $mensualidad=new Mensualidad();
            $gestionDao=new GestionDao();
            $mensualidadDao=new MensualidadDao();
            $mensualidad->nombre=$nombre;
            $mensualidad->fecha_pago=$fecha_pago;
            $mensualidad->monto=$monto;
            $mensualidad->gestion_id=$gestion_id;
            $ges=$gestionDao->obtenerGestion($gestion_id);
            $mensualidades_gestion=count($ges->mensualidades);
            if($mensualidades_gestion>0)
            {
                $ultimo_numero_pago=$mensualidadDao->obtenerUltimoNumeroPago($gestion_id);
                $numero=$ultimo_numero_pago->numero_pago;
                $numero=$numero+1;
                $mensualidad->numero_pago=$numero;
            }else{
                $mensualidad->numero_pago=1;
            }
            return $mensualidadDao->registrar($mensualidad);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listar($roles,$gestion_id)
    {

        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $mensualidadDao=new MensualidadDao();
            return $mensualidadDao->listar($gestion_id);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function actualizar($roles,$nombre,$fecha_pago,$monto,$mensualidad_id)
    {

        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $mensualidadDao=new MensualidadDao();
            $mensualidad=$mensualidadDao->obtenerMensualidad($mensualidad_id);
            $mensualidad->nombre=$nombre;
            $mensualidad->fecha_pago=$fecha_pago;
            $mensualidad->monto=$monto;

            return $mensualidadDao->actualizar($mensualidad);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listarMensualidadesNoPagadas($usuario_id,$roles,$gestion_id)
    {

        if(in_array(2,$roles))
        {
            $mensualidadDao=new MensualidadDao();
            $pagadas=$mensualidadDao->mensualidadesPagadas($usuario_id,$gestion_id);
            $ids_pagadas=array();

            for ($i=0;$i<count($pagadas);$i++)
            {
                array_push($ids_pagadas,$pagadas[$i]->mensualidad_id);
            }

            $numeros_pago=array();
            $todas=$mensualidadDao->todas($gestion_id);
            for ($i=0;$i<count($todas);$i++)
            {
                array_push($numeros_pago,$todas[$i]->numero_pago);
            }

            return $mensualidadDao->mensualidadesNoPagadas($gestion_id,$ids_pagadas);

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listarMensualidadesPagadas($usuario_id,$roles,$gestion_id)
    {

        if(in_array(2,$roles))
        {
            $mensualidadDao=new MensualidadDao();
            return $mensualidadDao->pagadas($usuario_id,$gestion_id);
        }
    }

    public function pagarMensaulidad($usuario_id,$roles,$mensualidad_id,$multa,$total)
    {

        if(in_array(2,$roles))
        {
            $mensualidadDao=new MensualidadDao();
            $cuentaDao=new CuentasDao();
            $negocioDao=new NegocioDao();
            $negocio=$negocioDao->obtenerNegocioUniversidad();
            $negocio_cuenta_id=$negocio->cuentas[0]->cuenta_id;
            $cuenta_universidad=$cuentaDao->obtenerCuenta($negocio_cuenta_id);

            $cuenta=$cuentaDao->obtenerCuentaPorUsuarioId($usuario_id);
            $saldo=$cuenta->saldo;
            if($saldo>=$total)
            {
                $m=$mensualidadDao->obtenerMensualidad($mensualidad_id);

                $nuevo_saldo=$saldo-$total;
                $cuenta->saldo=$nuevo_saldo;

                $saldo_universidad=$cuenta_universidad->saldo;
                $cuenta_universidad->saldo=$saldo_universidad+$total;
                return $mensualidadDao->pagarMensaulidad($m,$cuenta,$cuenta_universidad,$usuario_id,$multa);




            }else{
                $data = array(
                    'mensaje' => 'Usted no tiene saldo suficiente, su saldo actual es: '.$saldo.' Bs.',
                    'estado'=>'error'
                );
                return $data;
            }

        }
    }

    public function verificar($m,$usuario_id)
    {
        $mensualidadDao=new MensualidadDao();
        if($m->numero_pago==1)
        {
            return true;
        }else{
            //verificar
            $aux=$m->numero_pago-1;
            $m_ant=$mensualidadDao->obtenerMensualidadPorNumeroPago($aux,$m->gestion_id);
            $verPago=$mensualidadDao->verificarPago($m->numero_pago,$m_ant->mensualidad_id,$usuario_id);
            if($verPago!=null)
            {
                if(count($verPago)>0)
                {
                    return true;
                }else{
                    return false;

                }

            }else{
                return false;
            }
        }

    }

    public function pagosMensualidades($usuario_id,$roles,$mensualidad_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $mensualidadDao=new MensualidadDao();
            return $mensualidadDao->pagosMensualidades($mensualidad_id);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function pagosMensualidadesPorGestion($usuario_id,$roles,$gestion_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $mensualidadDao=new MensualidadDao();
            return $mensualidadDao->pagosMensualidadesPorGestion($gestion_id);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
}
