<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 30-03-20
 * Time: 04:20 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\CarreraDao;
use App\Models\Carrera;

class CarreraBl
{

    public function registrar($ususario_id,$roles,$nombre,$prefijo)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $carreraDao= new CarreraDao();
            $existeNombre=$carreraDao->verificarExistenciaCarrera($nombre);
            $existePrefijo=$carreraDao->verificarExistenciaPrefijo($prefijo);

            if($existeNombre)
            {
                $data = array(
                    'mensaje' => 'El nombre de la carrera ya existe.',
                    'estado'=>'error'
                );
                return $data;
            }else{
                if($existePrefijo)
                {
                    $data = array(
                        'mensaje' => 'El numero de prefijo de cuenta ya existe.',
                        'estado'=>'error'
                    );
                    return $data;
                }else{
                    $carrera=new Carrera();
                    $carrera->nombre=$nombre;
                    $carrera->prefijo_cuenta=$prefijo;
                    return $carreraDao->registrar($carrera);
                }
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function actualizar($ususario_id,$roles,$carrera_id,$nombre,$prefijo)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $carreraDao= new CarreraDao();
            $carrera=$carreraDao->obtenerCarrera($carrera_id);
            $carrera->nombre=$nombre;
            $carrera->prefijo_cuenta=$prefijo;
            return $carreraDao->actualizar($carrera);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function eliminar($ususario_id,$roles,$carrera_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $carreraDao= new CarreraDao();
            $carrera=$carreraDao->obtenerCarrera($carrera_id);
            $carrera->estado=true;
            return $carreraDao->eliminar($carrera);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listar($ususario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles))
        {

            $carreraDao= new CarreraDao();
            return $carreraDao->listar();
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
}
