<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 18-03-20
 * Time: 05:16 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\CuentasDao;
use App\Http\Controllers\Dao\NegocioDao;
use App\Http\Controllers\Dao\UsuarioDao;
use App\Models\Cuenta;
use App\Models\Negocio;

class NegocioBl
{
    public function registrarNegocio($dueno_id,$roles_token,$nombre,$descripcion,$hora_inicio,$hora_fin)
    {
        $negocio = new Negocio();
        $negocio->nombre=$nombre;
        $negocio->descripcion=$descripcion;
        $negocio->hora_inicio=$hora_inicio;
        $negocio->hora_fin=$hora_fin;
        $negocio->dueno_id=$dueno_id;
        $negocio->estado_afiliacion="Pendiente";
        $negocioDao= new NegocioDao();
        if(in_array(3,$roles_token))
        {
            $existeNegocio=$negocioDao->verificarExistenciaNombreNegocio($nombre);
            if ($existeNegocio)
            {
                $data = array(
                    'mensaje' => 'Ya existe un negocio con ese nombre.',
                    'estado'=>'error'
                );
                return $data;
            }else{
                return $negocioDao->registrarNegocio($negocio);
            }
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function afiliarNegocio($admin_id,$roles_token,$negocio_id)
    {
        if(in_array(1,$roles_token) || in_array(4,$roles_token))
        {
            $negocioDao= new NegocioDao();
            $negocio=$negocioDao->obtenerNegocioPorId($negocio_id);
            $negocio->afiliacion=true;
            return $negocioDao->actualizarNegocio($negocio);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function actualizarNegocio($admin_id,$roles_token,$nombre,$descripcion,$hora_inicio,$hora_fin,$negocio_id)
    {
        if(in_array(1,$roles_token) || in_array(3,$roles_token))
        {
            $negocioDao= new NegocioDao();
            $negocio=$negocioDao->obtenerNegocioPorId($negocio_id);
            $negocio->nombre=$nombre;
            $negocio->descripcion=$descripcion;
            $negocio->hora_inicio=$hora_inicio;
            $negocio->hora_fin=$hora_fin;
            return $negocioDao->actualizarNegocio($negocio);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function eliminarNegocio($admin_id,$roles_token,$negocio_id)
    {
        if(in_array(1,$roles_token) || in_array(4,$roles_token))
        {
            $negocioDao= new NegocioDao();
            $negocio=$negocioDao->obtenerNegocioPorId($negocio_id);
            $negocio->estado=true;
            return $negocioDao->actualizarNegocio($negocio);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function activar($admin_id,$roles_token,$negocio_id)
    {
        if(in_array(1,$roles_token) || in_array(4,$roles_token))
        {
            $negocioDao= new NegocioDao();
            $negocio=$negocioDao->obtenerNegocioPorId($negocio_id);
            $negocio->estado=false;
            return $negocioDao->actualizarNegocio($negocio);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function aceptarAfiliacion($admin_id,$roles_token,$negocio_id)
    {
        if(in_array(1,$roles_token) || in_array(4,$roles_token))
        {
            $negocioDao= new NegocioDao();
            $usuarioDao=new UsuarioDao();
            $negocio=$negocioDao->obtenerNegocioPorId($negocio_id);
            $dueno_id=$negocio->dueno_id;
            $nombre_negocio=$negocio->nombre;
            $nombreDueno=$negocioDao->obtenerNombreDuenoPorId($dueno_id);
            $negocio->afiliacion=true;
            $negocio->estado_afiliacion='Aceptado';
            // negocio listo
            $cuentaDao=new CuentasDao();
            $ultima_cuenta=$cuentaDao->obtenerUltimaCuenta();
            $numero=$ultima_cuenta[0]->numero_cuenta;
            $nuevo_numero_cuenta=$numero+1;
            $cuenta=new Cuenta();
            $cuenta->numero_cuenta=$nuevo_numero_cuenta;
            $cuenta->saldo=0;
            $cuenta->usuario_id=$dueno_id;

            $registroCuenta=$negocioDao->registrarCuentaNegocio($cuenta);
            if(gettype($registroCuenta)=='string')
            {
                $data = array(
                    'mensaje' => $registroCuenta,
                    'estado'=>'error'
                );
                return $data;
            }else{
                $correo=$usuarioDao->obtenerCorreo($dueno_id);
                $correo=$correo[0]->correo;
                return $negocioDao->aceptarAfiliacion($negocio,$registroCuenta,$nombreDueno,$nombre_negocio,$nuevo_numero_cuenta,$correo);
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function rechazarAfiliacion($admin_id,$roles_token,$negocio_id,$usuario_id)
    {
        if(in_array(1,$roles_token) || in_array(4,$roles_token))
        {
            $negocioDao= new NegocioDao();
            $usuarioDao=new UsuarioDao();
            $negocio=$negocioDao->obtenerNegocioPorId($negocio_id);
            $nombre_negocio=$negocio->nombre;
            $negocio->afiliacion=false;
            $negocio->estado_afiliacion='Rechazado';
            $usuario=$usuarioDao->obtenerCorreo($usuario_id);
            $nombre_dueno=$usuario[0]->nombre.' '.$usuario[0]->apellidos;
            $correo_dueno=$usuario[0]->correo;
            // negocio listo
            return $negocioDao->rechazarAfiliacion($negocio,$nombre_negocio,$nombre_dueno,$correo_dueno);

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function verPerfilNegocioParaEstudiante($usuario_id,$negocio_id)
    {
        $negocioDao= new NegocioDao();
        $existeNegocio= $negocioDao->verificarExistenciaNegocioPorId($negocio_id);
        if ($existeNegocio)
        {
            return $negocioDao->verPerfilNegocioParaEstudiante($negocio_id);
        }else{
            $data = array(
                'mensaje' => 'Negocio no encontrado en la billetera movil.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function listarNegociosAfiliadosYNoEliminados($usuario_id)
    {
        $negocioDao= new NegocioDao();
        return $negocioDao->listarNegociosAfiliadosYNoEliminados($usuario_id);
    }

    public function listarTodosLosNegocios($admin_id,$roles_token)
    {
        if(in_array(1,$roles_token) || in_array(4,$roles_token))
        {
            $negocioDao= new NegocioDao();
            return $negocioDao->listarTodosLosNegocios($admin_id);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerDatosParaQR($usuario_id,$roles,$negocio_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles) || in_array(3,$roles))
        {
            $dao = new NegocioDao();
            return $dao->obtenerDatosParaQR($negocio_id);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function misNegocios($usuario_id,$roles)
    {
        if(in_array(3,$roles))
        {
            $dao = new NegocioDao();
            return $dao->misNegocios($usuario_id);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function reintentarAfiliacion($usuario_id,$roles,$negocio_id)
    {
        if(in_array(1,$roles) || in_array(4,$roles) || in_array(3,$roles))
        {
            $dao = new NegocioDao();
            $negocio=$dao->obtenerNegocioPorId($negocio_id);
            $negocio->estado_afiliacion='Pendiente';
            return $dao->actualizarNegocio($negocio);
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function registrarUniversidad($superadmin_id,$roles)
    {
        $negocio = new Negocio();
        $negocio->nombre='Universidad';
        $negocio->descripcion='La universidad que cuenta con una billetera movil.';
        $negocio->hora_inicio='07:30';
        $negocio->hora_fin='20:00';
        $negocio->dueno_id=$superadmin_id;
        $negocio->estado_afiliacion="Aceptado";
        $negocioDao= new NegocioDao();
        $usuarioDao=new UsuarioDao();
        $user=$usuarioDao->mostrarPerfil($superadmin_id);
        $cuenta_id_superadmin=$user->cuentas[0]['cuenta_id'];
        if(in_array(4,$roles))
        {
            $existeNegocio=$negocioDao->verificarExistenciaNombreNegocio($negocio->nombre);
            if ($existeNegocio)
            {
                $data = array(
                    'mensaje' => 'Ya existe un negocio con ese nombre.',
                    'estado'=>'error'
                );
                return $data;
            }else{

                return $negocioDao->negocioUniversidad($negocio,$cuenta_id_superadmin);
            }
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

}
