<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 06-04-20
 * Time: 03:48 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\TipoTransaccionDao;
use App\Http\Controllers\Dao\TransaccionDao;
use App\Models\transaccion;

class TransaccionBl
{
    function registro( $monto, $cuenta_de,$cuenta_para,$tipo_transaccion)
    {
        //cuenta_de es quien la realiza
        //cuenta_para es para quien va dirigida
        //tipo_transaccion puede ser retiro,deposito,pago o transferencia
        $transaccion = new transaccion();
        $transaccion->monto=$monto;
        $transaccion->cuenta_de=$cuenta_de;
        $transaccion->cuenta_para=$cuenta_para;

        $tipoTransaccionDao= new TipoTransaccionDao();
        $transaccionDao= new TransaccionDao();
        $existeTipo= $tipoTransaccionDao->verificarExistenciaNombre($tipo_transaccion);
        if ($existeTipo)
        {
            $tipo=$tipoTransaccionDao->obtenerTipoTransaccion($tipo_transaccion);
            $tipo_id = $tipo->tipo_transaccion_id;
            $transaccion->tipo_transaccion_id=$tipo_id;
            return $transaccion;

        }else{
            $data = array(
                'descripcion' => "Tipo de transaccion '".$tipo_transaccion."' no registrado en la base de datos",
                'estado'=>'error'
            );
            return $data;
        }

    }
}
