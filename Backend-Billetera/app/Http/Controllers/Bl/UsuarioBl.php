<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 16-03-20
 * Time: 03:47 PM
 */

namespace App\Http\Controllers\Bl;


use App\Http\Controllers\Dao\CarreraDao;
use App\Http\Controllers\Dao\CuentasDao;
use App\Http\Controllers\Dao\NegocioDao;
use App\Http\Controllers\Dao\UsuarioDao;
use App\Http\Controllers\UsuarioController;
use App\Mail\ResetPassword;
use App\Models\Cuenta;
use App\Models\Negocio;
use App\Models\Usuario;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Image;

class UsuarioBl
{
    function registro( $nombre, $apellidos, $carnet,$correo, $nacimiento,$carrera, $telefono)
    {
        $usuario = new Usuario();
        $usuario->nombre = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->carnet = $carnet;
        $usuario->correo = $correo;
        $usuario->fecha_nacimiento = $nacimiento;
        $usuario->carrera_id = $carrera;
        $usuario->telefono = $telefono;
        $password=uniqid('',true);
        $default=$password;
        $password = $password . 'billetera'; //la sal
        $usuario->password = hash('sha256', $password);
        $usuario->password;

        /*if(!$imagen){
            $usuario->imagen = 'defaultuser.png';
        }
        else{
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/usuarios/';
            $nombreimagen = $usuario_login.time().'.jpg';
            $subidaimagen->resize(200,200)->save($path.$nombreimagen);
            $usuario->imagen = $nombreimagen;
        }*/

        //$usuario->imagen = $nombreimagen;

        $usuarioDao = new UsuarioDao();
        $cuentaBl= new CuentasBl();
        $existe = $usuarioDao->verificarExistenciaUsuario($usuario->carnet);

        if ($existe) {
            $data = array(
                'mensaje' => 'ya existe un usuario con ese carnet',
                'estado'=>'error'
            );
            return $data;
        } else {

            return $usuarioDao->registrar($usuario,$default);
        }
    }

    function registroVendedor( $nombre, $apellidos, $carnet,$correo, $nacimiento, $telefono,$password)
    {
        $usuario = new Usuario();
        $carreraDao=new CarreraDao();
        $carr=$carreraDao->obtenerCarreraPorNombre('Sin carrera');
        $usuario->nombre = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->carnet = $carnet;
        $usuario->correo = $correo;
        $usuario->carrera_id=$carr->carrera_id;
        $usuario->fecha_nacimiento = $nacimiento;
        $usuario->telefono = $telefono;
        $password = $password . 'billetera'; //la sal
        $usuario->password = hash('sha256', $password);
        $usuario->password_default=false;
        /*if(!$imagen){
            $usuario->imagen = 'defaultuser.png';
        }
        else{
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/usuarios/';
            $nombreimagen = $usuario_login.time().'.jpg';
            $subidaimagen->resize(200,200)->save($path.$nombreimagen);
            $usuario->imagen = $nombreimagen;
        }*/

        //$usuario->imagen = $nombreimagen;

        $usuarioDao = new UsuarioDao();

        $existe = $usuarioDao->verificarExistenciaUsuario($usuario->carnet);

        if ($existe) {
            $data = array(
                'mensaje' => 'el usuario ya existe',
                'descripcion' => 'hay un usuario con ese carnet',
                'estado'=>'error'
            );
            return $data;
        } else {

            return $usuarioDao->registrarVendedor($usuario);
        }
    }

    function registroAdministrador($roles, $nombre, $apellidos, $carnet,$correo, $nacimiento, $telefono)
    {
        if(in_array(4,$roles)) //cambiar a 4
        {
            $usuario = new Usuario();
            $carreraDao=new CarreraDao();
            $carr=$carreraDao->obtenerCarreraPorNombre('Sin carrera');
            $usuario->nombre = $nombre;
            $usuario->apellidos = $apellidos;
            $usuario->carnet = $carnet;
            $usuario->correo = $correo;
            $usuario->fecha_nacimiento = $nacimiento;
            $usuario->telefono = $telefono;
            $usuario->carrera_id=$carr->carrera_id;
            $password=uniqid('',true);
            $default=$password;
            $password = $password . 'billetera'; //la sal
            $usuario->password = hash('sha256', $password);

            $usuarioDao = new UsuarioDao();

            $existe = $usuarioDao->verificarExistenciaUsuario($usuario->carnet);

            if ($existe) {
                $data = array(
                    'mensaje' => 'el usuario ya existe',
                    'descripcion' => 'hay un usuario con ese carnet',
                    'estado'=>'error'
                );
                return $data;
            } else {

                return $usuarioDao->registrarAdministrador($usuario,$default,$nombre,$apellidos,$correo);
            }
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function mostrarUsuario($id)
    {
        if($id!=0)
        {
            $usuarioDao = new UsuarioDao();
            $user=$usuarioDao->mostrarPerfil($id);
            $roles_usuario=[];
            foreach ($user->roles as $rol)
            {
                array_push ($roles_usuario,$rol->nombre);
            }
            $object = (object) [
                "usuario_id"=> $user->usuario_id,
                "nombre"=> $user->nombre,
                "apellidos"=> $user->apellidos,
                "carnet"=> $user->carnet,
                "correo"=> $user->correo,
                "telefono"=> $user->telefono,
                "imagen"=>$user->imagen,
                "fecha_nacimiento"=> $user->fecha_nacimiento,
                "carrera"=>$user->carrera,
                "password_default"=>$user->password_default,
                "rol"=>implode( ", ", $roles_usuario ) //convierto a string y uno los roles con ,

            ];
            $data = array(
                'data'=> $object,
                'descripcion' => 'exito',
                'estado'=>'exito'
            );

        }else{
            $data = array(
                'data'=>null,
                'descripcion' => 'usuario con id igual a 0',
                'estado'=>'error'
            );

        }
        return response()->json($data);
    }

    public function actualizarPerfil($id,$nombre, $apellidos,$correo, $nacimiento, $telefono)
    {
        $usuarioDao=new UsuarioDao();
        $usuario=$usuarioDao->mostrarPerfil($id);
        $usuario->nombre = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->correo = $correo;
        $usuario->fecha_nacimiento = $nacimiento;
        $usuario->telefono = $telefono;
        //$usuario->carrera=$carrera;
        /*if($imagen){
            $image = str_replace('data:image/png;base64,', '', $imagen);
            $image = str_replace('data:image/jpg;base64,', '', $imagen);
            $image = str_replace('data:image/jpeg;base64,', '', $imagen);
            $image = str_replace(' ','+',$image);
            $subidaimagen = Image::make($image);
            $path = public_path().'/imagenes/usuarios/';
            $nombreimagen = $usuario_login.time().'.jpg';
            $subidaimagen->resize(200,200)->save($path.$nombreimagen);
            $usuario->imagen = $nombreimagen;
        }*/

        return $usuarioDao->actualizarPerfil($usuario);
    }

    public function cambiarPasswordDefault($usuario_id,$password)
    {
        $usuarioDao= new UsuarioDao();
        $user = $usuarioDao->mostrarPerfil($usuario_id);
        $password = $password . 'billetera'; //la sal
        $user->password = hash('sha256', $password);
        $user->password_default=false;
        return $usuarioDao->actualizarPerfil($user);
    }

    public function editarUsuarioAdministrador($roles,$usuario_id,$nombre, $apellidos,$correo, $nacimiento, $telefono)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles) || in_array(4,$roles)){
            $usuarioDao=new UsuarioDao();
            $usuario=$usuarioDao->mostrarPerfil($usuario_id);
            $usuario->nombre = $nombre;
            $usuario->apellidos = $apellidos;
            $usuario->correo = $correo;
            $usuario->fecha_nacimiento = $nacimiento;
            $usuario->telefono = $telefono;
            return $usuarioDao->actualizarPerfil($usuario);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function actualizarPassword($usuario_id,$actual_password,$nueva_password)
    {
        $password1 = $actual_password . 'billetera';
        $actual_password_encriptada = hash('sha256', $password1);
        $password2 = $nueva_password . 'billetera';
        $nueva_password_encriptada = hash('sha256', $password2);
        $usuarioDao=new UsuarioDao();
        return $usuarioDao->actualizarPassword($usuario_id,$actual_password_encriptada,$nueva_password_encriptada);
    }

    public function listarUsuarios($usuario_id,$roles_usuarios)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles_usuarios) || in_array(4,$roles_usuarios)){
            return $usuarioDao->listarUsuarios($usuario_id);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function listarUsuariosAdministradores($usuario_id,$roles_usuarios)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(4,$roles_usuarios)){ //cambiar a 4
            return $usuarioDao->listarUsuariosAdministradores();
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }

    }

    public function activarUsuario($roles,$usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles) || in_array(4,$roles)){
            $usuario= $usuarioDao->mostrarPerfil($usuario_id);
            $usuario->estado=false;
            return $usuarioDao->actualizarPerfil($usuario);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
    public function activarUsuarioAdministrador($roles,$usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(4,$roles)){ //cambiar a 4 despues
            $usuario= $usuarioDao->mostrarPerfil($usuario_id);
            $usuario->estado=false;
            return $usuarioDao->actualizarPerfil($usuario);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function eliminarUsuario($roles_usuario,$usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles_usuario) || in_array(4,$roles_usuario)){
            $usuario= $usuarioDao->mostrarPerfil($usuario_id);
            $usuario->estado=true;
            return $usuarioDao->actualizarPerfil($usuario);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
    public function eliminarUsuarioAdministrador($roles_usuario,$usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(4,$roles_usuario)){ //cambiar a 4
            $usuario= $usuarioDao->mostrarPerfil($usuario_id);
            $usuario->estado=true;
            return $usuarioDao->actualizarPerfil($usuario);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerInformacionUsuarioDeposito($usuario_id,$roles,$carnet)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $user=$usuarioDao->obtenerUsuarioConCarnet($carnet);
            if (is_object($user))
            {
                $roles_usuario=array();
                foreach ($user->roles as $rol)
                {
                    array_push ($roles_usuario,$rol->rol_id);
                }
                if (in_array(2,$roles_usuario)) //es estudiante
                {
                    return $usuarioDao->obtenerInformacionUsuarioDeposito($carnet);
                }else{
                    $data = array(
                        'mensaje' => 'El usuario con carnet '.$carnet.' no esta registrado como estudiante.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                $data = array(
                    'mensaje' => 'El usuario con carnet '.$carnet.' no existe.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerInformacionUsuarioRetiro($usuario_id,$roles,$carnet)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $user=$usuarioDao->obtenerUsuarioConCarnet($carnet);
            if (is_object($user))
            {
                $roles_usuario=array();
                foreach ($user->roles as $rol)
                {
                    array_push ($roles_usuario,$rol->rol_id);
                }
                if (in_array(3,$roles_usuario)) //es proveedor
                {
                    return $usuarioDao->obtenerInformacionUsuarioDeposito($carnet);
                }else{
                    $data = array(
                        'mensaje' => 'El usuario con carnet '.$carnet.' no esta registrado como proveedor.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                $data = array(
                    'mensaje' => 'El usuario con carnet '.$carnet.' no existe.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function traspasarCuenta($usuario_id,$roles,$carnet,$nueva_carrera)
    {
        $usuarioDao=new UsuarioDao();
        if(in_array(1,$roles) || in_array(4,$roles))
        {
            $user=$usuarioDao->obtenerUsuarioConCarnet($carnet);
            if (is_object($user))
            {
                $roles_usuario=array();
                foreach ($user->roles as $rol)
                {
                    array_push ($roles_usuario,$rol->rol_id);
                }
                if (in_array(2,$roles_usuario)) //es estudiante
                {
                    /*aca estoy borrando la anterior cuenta y creando otra pero tambien se puede
                    guardar el numero de cuenta en una tabla que sea "cuentas eliminadas" que registre las
                    cuentas que fueron eliminadas para despues ser reutilizadas*/
                    $cuenta=$usuarioDao->obtenerCuentasPorUsuario2($user->usuario_id);
                    $user->carrera=$nueva_carrera;
                    $saldo_anterior_cuenta=$cuenta->saldo;
                    $cuenta->estado=true;
                    $cuentasBl = new CuentasBl();
                    $cuentasBl->registrar($usuario_id,$roles,123123123,$saldo_anterior_cuenta,$user->usuario_id);
                    return $usuarioDao->actualizarPerfil($user);

                }else{
                    $data = array(
                        'mensaje' => 'El usuario con carnet '.$carnet.' no esta registrado como proveedor.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                $data = array(
                    'mensaje' => 'El usuario con carnet '.$carnet.' no existe.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function resetPassword($carnet)
    {
        $usuarioDao = new UsuarioDao();
        $existe=$usuarioDao->verificarExistenciaUsuario($carnet);
        if ($existe)
        {
            $user=$usuarioDao->obtenerUsuarioConCarnet($carnet);
            $fecha=date("Y-m-d");
            $hora="".(date('H')-4).":".date('i').":".date('s');
            $cifrado = md5($user->usuario_id." ".$fecha." ".$hora);
            $key=substr($cifrado,0,7);
            $user->codigo_reset_password=$key;
            $usuarioDao->actualizarPerfil($user);
            try
            {
                Mail::to($user->correo)->send(new ResetPassword($user->nombre." ".$user->apellidos,$key));
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Se envio correo y guardo en la DB el codigo de recuperacion con exito.',
                    "cifrado"=>$key,
                    'estado'=>'exito'
                );
                return $data;
            }catch (\Exception $e){
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No se pudo enviar el correo de recuperacion.',
                    'estado'=>'error'
                );
                return $data;
            }
        }
        else{
            $data=array(
                'data'=>null,
                'mensaje'=>'No se encontro al usuario con carnet de identidad '.$carnet.'.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function verificarResetPassword($carnet,$codigo_reset_password)
    {
        $usuarioDao = new UsuarioDao();
        $existe=$usuarioDao->verificarExistenciaUsuario($carnet);
        if ($existe)
        {
            return $usuarioDao->verificarResetPassword($carnet,$codigo_reset_password);
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'No se encontro al usuario con carnet de identidad '.$carnet.'.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function cambiarPassword($password,$repetir_password,$carnet)
    {
        if (strcmp($password,$repetir_password)==0)
        {
            $usuarioDao = new UsuarioDao();
            $existe=$usuarioDao->verificarExistenciaUsuario($carnet);
            if ($existe)
            {
                $user=$usuarioDao->obtenerUsuarioConCarnet($carnet);
                $password = $password . 'billetera'; //la sal
                $user->password = hash('sha256', $password);
                return $usuarioDao->actualizarPerfil($user);

            }
            else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'No se encontro al usuario con carnet de identidad '.$carnet.'.',
                    'estado'=>'error'
                );
                return $data;
            }
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Las password no son iguales.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerSaldo($usuario_id)
    {
        if (is_integer($usuario_id) && $usuario_id>0)
        {
            $usuarioDao=new UsuarioDao();
            $user=$usuarioDao->mostrarPerfil($usuario_id);
            if (is_object($user))
            {
                return $usuarioDao->obtenerSaldo($usuario_id);
            }else{
                $data=array(
                    'data'=>null,
                    'mensaje'=>'Error al obtener el saldo el usuario no existe.',
                    'estado'=>'error'
                );
                return $data;
            }
        }else{
            $data=array(
                'data'=>null,
                'mensaje'=>'Error en el identificador de usuario.',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerNombresNegociosPago($usuario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles) || in_array(2,$roles) || in_array(3,$roles))
        {
            $dao = new NegocioDao();
            return $dao->obtenerNombresNegociosPago();
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function pagar($usuario_id,$roles,$negocio_id,$dueno_id,$carnet_dueno,$nombre_negocio,$numero_cuenta,$monto)
    {
        if(in_array(1,$roles) || in_array(4,$roles) || in_array(2,$roles)) //estudiante
        {
            $dao = new UsuarioDao();
            $cuentaDao = new CuentasDao();
            $transaccionBl = new TransaccionBl();
            $existeDueno=$dao->verificarDueno($dueno_id,$carnet_dueno);
            if ($existeDueno)
            {
                $existeNegocio=$dao->verificarNegocio($negocio_id,$nombre_negocio);
                if ($existeNegocio)
                {
                    $existeCuenta=$dao->verificarCuenta($dueno_id,$numero_cuenta);
                    if ($existeCuenta)
                    {
                        $saldo = $dao->obtenerSaldoPago($usuario_id);

                        if ($saldo->saldo>=$monto)
                        {
                            //se paga
                            $cuenta_estudiante=$dao->obtenerCuentaEstudiantePago($usuario_id);
                            $cuenta_proveedor=$dao->obtenerCuentaProveedorPago($dueno_id,$numero_cuenta);
                            /*$saldo1=(int)$cuenta_estudiante->saldo;
                            $saldo2=(int)$cuenta_proveedor->saldo;*/
                            $cuenta_estudiante->saldo=$cuenta_estudiante->saldo-$monto;
                            $cuenta_proveedor->saldo=$cuenta_proveedor->saldo+$monto;
                            $transaccion=$transaccionBl->registro($monto,$cuenta_estudiante->cuenta_id,$cuenta_proveedor->cuenta_id,'Pago');
                            //monto    cuenta_de           cuenta_para          nombre del tipo de transaccion
                            if (is_object($transaccion))
                            {
                                //return $cuenta;
                                //return $transaccion;
                                return $cuentaDao->pagar($cuenta_estudiante,$cuenta_proveedor,$transaccion);
                            }else{
                                return $transaccion;
                            }


                        }else{
                            $data = array(
                                'data'=>null,
                                'mensaje' => 'Error saldo insuficiente.',
                                'estado'=>'error'
                            );
                            return $data;
                        }
                    }else{
                        $data = array(
                            'data'=>null,
                            'mensaje' => 'La cuenta no se encuentra en la base de datos.',
                            'estado'=>'error'
                        );
                        return $data;
                    }
                }else{
                    $data = array(
                        'data'=>null,
                        'mensaje' => 'El negocio no se encuentra en la base de datos.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                $data = array(
                    'data'=>null,
                    'mensaje' => 'El dueno del negocio no se encuentra en la base de datos.',
                    'estado'=>'error'
                );
                return $data;
            }
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function obtenerDatosTransferir($usuario_id,$roles,$numero_cuenta)
    {
        if(in_array(1,$roles) || in_array(4,$roles) || in_array(2,$roles))
        {
            $cuentaDao=new CuentasDao();
            $usuarioDao= new UsuarioDao();
            $cuenta_usuario=$usuarioDao->obtenerSaldoPago($usuario_id);
            if ($cuenta_usuario->numero_cuenta!=$numero_cuenta)
            {
                $existeCuenta=$cuentaDao->verificarExistenciaCuenta($numero_cuenta);
                if ($existeCuenta)
                {
                    $roles_cuenta=$cuentaDao->verificarRolesDeCuenta($numero_cuenta);
                    $r=Array();
                    foreach ($roles_cuenta as $rol)
                    {
                        array_push($r,$rol->rol_id);
                    }
                    if (in_array(2,$r))
                    {
                        return $usuarioDao->obtenerDatosTransferir($numero_cuenta);
                    }else{
                        $data = array(
                            'mensaje' => 'El numero de cuenta no pertenece a un estudiante.',
                            'estado'=>'error'
                        );
                        return $data;
                    }

                }else{
                    $data = array(
                        'mensaje' => 'El numero de cuenta no se encuentra en la base de datos.',
                        'estado'=>'error'
                    );
                    return $data;
                }
            }else{
                $data = array(
                    'mensaje' => 'Es su propia cuenta.',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }



    public function transferir($usuario_id,$roles,$numero_cuenta,$monto)
    {
        if(in_array(2,$roles)) //estudiante
        {

            $usuarioDao= new UsuarioDao();
            $cuentaDao= new CuentasDao();
            $transaccionBl = new TransaccionBl();
            $cuenta_usuario=$usuarioDao->obtenerSaldoPago($usuario_id);
            if ($cuenta_usuario->numero_cuenta!=$numero_cuenta)
            {
                $c_id=$cuentaDao->obtenerCuentaPorNumeroCuenta($numero_cuenta); //obtengo el objeto de la nuero de cuenta
                $ii=$c_id->cuenta_id; //saco la cuenta_id del objeto
                $existe=$cuentaDao->verificarRolesDeCuenta2($ii); //verifico si esta en la tabla cuentas_negocios
                //return response()->json($existe);
                //si existe es true es que es un proveedor sino un estudiante
                if (!$existe) //no es proveedor
                {
                    if ($cuenta_usuario->saldo>=$monto)
                    {
                        //transferir
                        $cuenta_destino=$cuentaDao->obtenerCuentaPorNumeroCuenta($numero_cuenta);
                        $cuenta_destino->saldo=$cuenta_destino->saldo+$monto;
                        $cuenta_destino->saldo=number_format((float)$cuenta_destino->saldo, 2, '.', '');
                        $cuenta_usuario->saldo=$cuenta_usuario->saldo-$monto;
                        $cuenta_usuario->saldo=number_format((float)$cuenta_usuario->saldo, 2, '.', '');
                        $transaccion=$transaccionBl->registro($monto,$cuenta_usuario->cuenta_id,$cuenta_destino->cuenta_id,'Transferencia');
                        //monto    cuenta_de           cuenta_para          nombre del tipo de transaccion
                        if (is_object($transaccion))
                        {
                            //return $cuenta_usuario;
                            //return $cuenta_destino;
                            //return $transaccion;
                            return $cuentaDao->transferir($cuenta_usuario,$cuenta_destino,$transaccion);
                        }else{
                            return $transaccion;
                        }


                    }else{
                        $data = array(
                            'mensaje' => 'No cuenta con saldo suficiente para traspaso. Saldo disponible '.$cuenta_usuario->saldo.' Bs.',
                            'estado'=>'error'
                        );
                        return $data;
                    }
                }else{
                    $data = array(
                        'mensaje' => 'El numero de cuenta no pertenece a un estudiante.',
                        'estado'=>'error'
                    );
                    return $data;
                }

            }else{
                $data = array(
                    'mensaje' => 'No puede realizar una transferencia a usted mismo',
                    'estado'=>'error'
                );
                return $data;
            }

        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function generarCodigoTransferencia($usuario_id,$roles)
    {
        if(in_array(1,$roles) || in_array(4,$roles) || in_array(2,$roles))
        {
            try{
                $codigo="";
                for ($i=0;$i<5;$i++)
                {
                    $d=mt_rand(0,9);
                    $codigo=$codigo.$d;
                }
                $data = array(
                    'data'=>$codigo,
                    'mensaje' => 'Se genero el codigo de transferencia correctamente',
                    'estado'=>'exito'
                );
                return $data;
            }
            catch (\Exception $e)
            {
                $data = array(
                    'mensaje' => 'Ocurrio un error en la generacion del codigo '.$e,
                    'estado'=>'error'
                );
                return $data;
            }
        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }


    public function obtenerNumeroCuenta($usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        return $usuarioDao->obtenerNumeroCuenta($usuario_id);
    }

    public function retirarDineroEstudianteFinCarrera($roles,$usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        $cuentaDao=new CuentasDao();
        if(in_array(1,$roles) || in_array(4,$roles)){ //cambiar a 4 despues
            $cuenta=$usuarioDao->obtenerCuentaPorUsuario($usuario_id);
            $cuenta[0]->saldo=0;
            return $cuentaDao->actualizar($cuenta[0]);
        }else{
            $data = array(
                'mensaje' => 'el usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }

    public function crearSuperAdministradorGenesis($nombre, $apellidos, $carnet,$correo, $nacimiento, $telefono,$password)
    {
        $usuario = new Usuario();
        $carreraDao=new CarreraDao();
        $carr=$carreraDao->obtenerCarreraPorNombre('Sin carrera');
        $usuario->nombre = $nombre;
        $usuario->apellidos = $apellidos;
        $usuario->carnet = $carnet;
        $usuario->correo = $correo;
        $usuario->fecha_nacimiento = $nacimiento;
        $usuario->carrera_id = $carr->carrera_id;
        $usuario->telefono = $telefono;
        $password = $password . 'billetera'; //la sal
        $usuario->password = hash('sha256', $password);

        $usuarioDao = new UsuarioDao();
        $cuentaBl= new CuentasBl();
        $existe = $usuarioDao->verificarExistenciaUsuario($usuario->carnet);

        if ($existe) {
            $data = array(
                'mensaje' => 'ya existe un usuario con ese carnet',
                'estado'=>'error'
            );
            return $data;
        } else {
            return $usuarioDao->crearSuperAdministradorGenesis($usuario);
        }
    }

    public function actualizarFotoPerfil($usuario_id,$imagen,$rol)
    {

        $usuarioDao=new UsuarioDao();
        $user=$usuarioDao->mostrarPerfil($usuario_id);
        $image = str_replace('data:image/png;base64,', '', $imagen);
        $image = str_replace('data:image/jpg;base64,', '', $imagen);
        $image = str_replace('data:image/jpeg;base64,', '', $imagen);
        $image = str_replace(' ','+',$image);

        $subida=\Intervention\Image\Facades\Image::make($image);
        $directorio='';


        if($rol=="estudiante")
        {
            $path=public_path().'/imagenes/usuarios/estudiantes/';
            $directorio='/imagenes/usuarios/estudiantes/';
        }else{
            if($rol=="proveedor")
            {
                $path=public_path().'/imagenes/usuarios/proveedores/';
                $directorio='/imagenes/usuarios/proveedores/';
            }
            else{
                if($rol=="administrador")
                {
                    $path=public_path().'/imagenes/usuarios/administradores/';
                    $directorio='/imagenes/usuarios/administradores/';
                }
                else{
                    if($rol=="superadmin")
                    {
                        $path=public_path().'/imagenes/usuarios/superadmin/';
                        $directorio='/imagenes/usuarios/superadmin/';
                    }
                }
            }
        }
        $nombreImagenDB=$user->imagen; //nombre de la foto en la DB
        if($nombreImagenDB!='defaultuser.png') //si ees diferente a la por defecto
        {
            try{
                //elimino la foto
                File::delete($path.$nombreImagenDB);

            }catch (\Exception $e)
            {
                $data = array(
                    'mensaje' => 'No se pudo cargar la nueva imagen, intente nuevamente.',
                    'estado'=>'error'
                );
                return $data;
            }
        }

        $nombreimagen = $user->carnet.time().'.png';
        //$subida->resize(200,200)->save($path.$nombreimagen);
        $subida->resize(200,200)->encode('png');
        $disk=Storage::disk('billetera');
        $disk->put($directorio.$nombreimagen,$subida);

        //compress image with tinypng
        /*try {
            \Tinify\setKey("T6dqZzGhwQmyb820Btk21Ly1CgPCyhcJ"); // Alternatively, you can store your key in .env file.
            $source = \Tinify\fromFile($directorio.$nombreimagen);
            $source->toFile($directorio.$nombreimagen);
        } catch(\Tinify\AccountException $e) {
            // Verify your API key and account limit.
            //$e->getMessage()
        } catch(\Tinify\ClientException $e) {
            // Check your source image and request options.
            //$e->getMessage()
        } catch(\Tinify\ServerException $e) {
            // Temporary issue with the Tinify API.
            //$e->getMessage()
        } catch(\Tinify\ConnectionException $e) {
            // A network connection error occurred.
            //$e->getMessage()
        } catch(Exception $e) {
            // Something else went wrong, unrelated to the Tinify API.
            //$e->getMessage()
        }*/


        $user->imagen = $nombreimagen;
        return $usuarioDao->actualizarPerfil($user);

    }

    public function eliminarFotoPerfil($usuario_id)
    {

        $usuarioDao=new UsuarioDao();
        $user=$usuarioDao->mostrarPerfil($usuario_id);
        $user->imagen = "defaultuser.png";
        return $usuarioDao->actualizarPerfil($user);
    }

    public function guardarOneSignalId($usuario_id,$onesignal_id)
    {
        $usuarioDao=new UsuarioDao();
        $user=$usuarioDao->mostrarPerfil($usuario_id);
        $user->onesignal_id=$onesignal_id;
        return $usuarioDao->actualizarPerfil($user);
    }

    public function resetOneSignalId($usuario_id)
    {
        $usuarioDao=new UsuarioDao();
        $user=$usuarioDao->mostrarPerfil($usuario_id);
        $user->onesignal_id='s/n';
        return $usuarioDao->actualizarPerfil($user);
    }

    public function obtenerOneSignalId($usuario_id,$roles,$usuario2_id)
    {
        if(in_array(2,$roles))
        {
            $usuarioDao=new UsuarioDao();
            return $usuarioDao->mostrarPerfil($usuario2_id);


        }else{
            $data = array(
                'mensaje' => 'El usuario no tiene privilegios',
                'estado'=>'error'
            );
            return $data;
        }
    }
}
