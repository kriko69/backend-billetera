create database billeteramovil;

select * from usuarios;
select * from roles;
select * from roles_usuarios;
select * from negocios;
select * from alumnos_universidad;
select * from carreras;
select * from cuentas;
select * from tipo_transacciones;
select * from transacciones;
select * from cuentas_negocios;
SHOW TABLES LIKE 'tip%';

select c.numero_cuenta from cuentas c, negocios n, cuentas_negocios cn
where c.cuenta_id=cn.cuenta_id
and n.negocio_id=cn.negocio_id
and n.negocio_id=1;

alter table negocios add estado_afiliacion varchar(100);

select a.carnet,a.usuario_id,b.nombre,b.negocio_id,c.numero_cuenta
from usuarios a,negocios b,cuentas c,cuentas_negocios d
where b.dueno_id=a.usuario_id
and a.usuario_id=c.usuario_id
and d.negocio_id=b.negocio_id
and d.cuenta_id=c.cuenta_id
and a.estado=false
and c.estado=false
and b.negocio_id=1;


select t.transaccion_id,tt.nombre,t.cuenta_de as de,t.cuenta_para as para, t.monto,t.created_at 
from tipo_transacciones tt, transacciones t, cuentas c
where c.cuenta_id=t.cuenta_de
and t.tipo_transaccion_id=tt.tipo_transaccion_id
and t.cuenta_de=4
or t.cuenta_para=4
and c.cuenta_id=t.cuenta_de
and t.tipo_transaccion_id=tt.tipo_transaccion_id
order by t.created_at ASC;

select u.nombre,u.apellidos,c.numero_cuenta
from usuarios u,cuentas c
where c.usuario_id=u.usuario_id
and c.cuenta_id=4;

insert into carreras(nombre,prefijo_cuenta) values ("Sistemas",10);

update usuarios set estado=false where usuario_id=3;

update cuentas set saldo=10.5 where cuenta_id=1;


update negocios set estado=false where negocio_id=1;
#drop table usuarios;
#drop table roles;
#drop table roles_usuarios;
#drop table negocios;
#drop table alumnos_universidad;
#drop table carreras;
#drop table cuentas;
#drop table transacciones;
#drop table tipo_trasacciones;
insert into roles(nombre,descripcion,estado) values ("administrador","administrador de la billetera movil",false);
insert into roles(nombre,descripcion,estado) values ("estudiante","estudiantes de la universidad",false);
insert into roles(nombre,descripcion,estado) values ("proveedor","vendedor de servicios o productos",false);
insert into roles(nombre,descripcion,estado) values ("superadmin","tiene el control de toda la billetera movil",false);


insert into roles_usuarios(usuario_id,rol_id) values (2,1);

select a.* from usuarios a,roles_usuarios b
where a.usuario_id=b.usuario_id
and b.rol_id=3
and a.estado=false;

alter table usuarios add column codigo_reset_password varchar(100);

select a.saldo from cuentas a, usuarios b 
where a.usuario_id = b.usuario_id
and a.estado=false
and b.usuario_id=2
limit 1;

select a.rol_id from roles_usuarios a, usuarios b, cuentas c
where c.usuario_id=b.usuario_id
and b.usuario_id=a.usuario_id
and c.estado=false
and c.numero_cuenta=123456789;

select a.* from usuarios a, cuentas b, roles_usuarios c
where b.usuario_id=a.usuario_id
and a.usuario_id=c.usuario_id
and c.rol_id=2
and b.estado=false
and b.numero_cuenta=123123127;